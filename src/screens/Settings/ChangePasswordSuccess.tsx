import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationProp, useRoute } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { BackHandler, Button, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import SvgSuccess from '../../assets/svg/SvgSuccess'
import { COLORS, ROUTES } from '../../constants'
import { useDataContextNavigation } from '../../navigations/ContextNavigation'


interface Props {
    navigation: NavigationProp<any, any>
}

const ChangePasswordSuccess: React.FC<Props> = (props) => {
    const { navigation } = props
    const { updateContextData } = useDataContextNavigation()
    const route = useRoute<any>();
    const { TYPE } = route.params

    useEffect(() => {
        const backAction = () => {
            return true;
        };

        const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

        return () => {
            backHandler.remove();
        };
    }, []);

    const onPressLearnMore = async () => {
        await AsyncStorage.removeItem('@userInfoData')
        await AsyncStorage.removeItem('@userInfoDataDetail')
        updateContextData(false)

        navigation.reset({
            index: 0,
            routes: [{ name: ROUTES.LOGIN }]
        })
    }

    return (
        <SafeAreaView style={styles.bgColor}>
            <View style={styles.comingSoon}>
                <View style={styles.logo}>
                    <SvgSuccess />
                </View>
                {TYPE === "FORGOT_PASSWORD"
                    ? <Text style={styles.textComingSoon}>Bạn đã đặt lại mật khẩu tài khoản thành công.</Text>
                    : <>
                        <Text style={styles.textComingSoon}>Bạn đã đổi mật khẩu  thành công.</Text>
                        <Text style={styles.textComingSoon}>Vui lòng đăng nhập lại ứng dụng</Text>
                    </>}

                <TouchableOpacity onPress={onPressLearnMore} style={styles.bgButton}>
                    <Text style={styles.back}>Đăng nhập ngay</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>

    )
}

export default ChangePasswordSuccess

const styles = StyleSheet.create({
    bgColor: {
        flex: 1,
        backgroundColor: COLORS.bgColor,

    },
    logo: {
        alignSelf: "center",
        marginBottom: 20
    },
    comingSoon: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "center",
    },
    textComingSoon: {
        "fontFamily": "Quicksand",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: "center"
    },
    bgButton: {
        backgroundColor: COLORS.white,
        borderRadius: 15,
        alignSelf: 'center',
        marginTop: 28
    },
    back: {
        alignSelf: "center",
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": "#034685",
        "paddingTop": 12,
        "paddingRight": 49,
        "paddingBottom": 12,
        "paddingLeft": 49,
    }
})