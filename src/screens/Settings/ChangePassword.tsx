import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationProp } from '@react-navigation/native';
import axios from 'axios';
import { get, isEmpty } from 'lodash';
import { Button, Input } from 'native-base';
import React, { useState } from 'react';
import { KeyboardAvoidingView, Platform, Pressable, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Toast } from 'react-native-toast-message/lib/src/Toast';
import SvgEye from '../../assets/svg/SvgEye';
import SvgEye1 from '../../assets/svg/SvgEye1';
import { ROUTES } from '../../constants';
import { URL_AUTH_ASC } from '../../network/UrlNetWork';
import { useDataContextNavigation } from '../../navigations/ContextNavigation';

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const ChangePassword: React.FC<RouterProps> = (props) => {
    const { navigation } = props

    const [oldPassword, setOldPassword] = useState<string | undefined>(undefined)
    const [password, setPassword] = useState<string | undefined>(undefined)
    const [confirmPassword, setConfirmPassword] = useState<string | undefined>(undefined)

    const { updateContextData } = useDataContextNavigation();

    const [loadingRegister, setLoadingRegister] = useState<boolean>(false)

    const [show, setShow] = React.useState(false);
    const [showConfirm, setShowConfirm] = React.useState(false);
    const [showOld, setShowOld] = React.useState(false);

    const onSubmitForm = async () => {

        if (isEmpty(oldPassword)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập mật khẩu cũ',
                topOffset: 60,
            })
        }

        if (oldPassword && oldPassword.length < 6) {
            return Toast.show({
                type: 'error',
                text1: 'Mật khẩu cũ phải có ít nhất 6 kí tự.',
                topOffset: 60,

            })
        }

        if (isEmpty(password)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập mật khẩu mới',
                topOffset: 60,

            })
        }

        if (password && password.length < 6) {
            return Toast.show({
                type: 'error',
                text1: 'Mật khẩu phải có ít nhất 6 kí tự.',
                topOffset: 60,

            })
        }

        if (password !== confirmPassword) {
            return Toast.show({
                type: 'error',
                text1: 'Mật khẩu không khớp nhau',
                topOffset: 60
            })
        }

        setLoadingRegister(true)
        const TokenCode: string | null = await AsyncStorage.getItem('@TokenCode');
        const userInfoData: any = await AsyncStorage.getItem('@userInfoData');

        try {
            const res = await axios.post(`${URL_AUTH_ASC}/api/AcsUser/ChangePassword`,
                {
                    "ApiData": {
                        "LOGIN_NAME": get(JSON.parse(userInfoData), 'User.LoginName'),
                        "PASSWORD__OLD": oldPassword,
                        "PASSWORD__NEW": password
                    }
                },
                {
                    headers: {
                        "Content-Type": "application/json",
                        TokenCode: TokenCode,
                        ApplicationCode: "TTV",
                    }
                })

            if (res && res.data && res.data.Success) {
                setTimeout(() => {
                    setLoadingRegister(false)
                }, 500)
                setPassword(undefined)
                setOldPassword(undefined)
                setConfirmPassword(undefined)

                await AsyncStorage.removeItem('@userInfoData')
                await AsyncStorage.removeItem('@userInfoDataDetail')
                updateContextData(false);

                navigation.navigate(ROUTES.CHANGE_PASSWORD_SUCCESS, { TYPE: "FORGOT_PASSWORD" })

                return Toast.show({
                    type: 'success',
                    text1: "Đổi mật khẩu thành công",
                    topOffset: 60,
                })
            } else {
                setTimeout(() => {
                    setLoadingRegister(false)
                }, 500)
                return Toast.show({
                    type: 'error',
                    text1: get(res, 'data.Param.Messages[0]'),
                    topOffset: 60,
                })
            }
        } catch (error) {
            setTimeout(() => {
                setLoadingRegister(false)
            }, 500)
        }
    }

    const onChangePassword = (text: string) => setPassword(text)
    const onChangeConfirmPassword = (text: string) => setConfirmPassword(text)
    const onChangeOldPassword = (text: string) => setOldPassword(text)

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: "#F2F2F4" }}>
                <View style={styles.formLogin}>
                    <KeyboardAvoidingView
                        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                        style={{
                            flex: 1
                        }}
                    >
                        <ScrollView
                            style={{
                                flex: 1,
                                padding: 15,
                                paddingTop: 30
                            }}
                            keyboardShouldPersistTaps="handled"
                        >
                            <>
                                <View>
                                    <Text style={styles.lableForm}>Mật khẩu cũ</Text>
                                </View>
                                <View style={styles.inputView}>
                                    <Input
                                        type={showOld ? "text" : "password"}
                                        InputRightElement={<Pressable onPress={() => setShowOld(!showOld)}>
                                            {showOld ? <SvgEye1 /> : <SvgEye />}
                                        </Pressable>}
                                        style={styles.inputPassword}
                                        placeholderTextColor="#443B3B"
                                        placeholder="Nhập mật khẩu cũ"
                                        onChangeText={(onChangeOldPassword)}
                                        value={oldPassword}
                                        variant="unstyled"

                                    />
                                </View>
                            </>

                            <>
                                <View>
                                    <Text style={styles.lableForm}>Mật khẩu mới</Text>
                                </View>
                                <View style={styles.inputView}>
                                    <Input
                                        type={show ? "text" : "password"}
                                        InputRightElement={<Pressable onPress={() => setShow(!show)}>
                                            {show ? <SvgEye1 /> : <SvgEye />}
                                        </Pressable>}
                                        style={styles.inputPassword}
                                        placeholderTextColor="#443B3B"
                                        placeholder="Nhập mật khẩu mới"
                                        onChangeText={(onChangePassword)}
                                        value={password}
                                        variant="unstyled"

                                    />
                                </View>
                            </>

                            <>
                                <View>
                                    <Text style={styles.lableForm}>Nhập lại mật khẩu mới</Text>
                                </View>
                                <View style={styles.inputView}>
                                    <Input
                                        type={showConfirm ? "text" : "password"}
                                        InputRightElement={<Pressable onPress={() => setShowConfirm(!showConfirm)}>
                                            {showConfirm ? <SvgEye1 /> : <SvgEye />}
                                        </Pressable>}
                                        style={styles.inputPassword}
                                        placeholderTextColor="#443B3B"
                                        placeholder="Nhập lại mật khẩu mới"
                                        onChangeText={(onChangeConfirmPassword)}
                                        value={confirmPassword}
                                        variant="unstyled"

                                    />
                                </View>
                            </>

                            <Button onPress={onSubmitForm} isLoadingText='Đổi mật khẩu'
                                isLoading={loadingRegister} style={styles.borderBotton}>
                                <Text style={styles.textButton}>Đổi mật khẩu</Text>
                            </Button>

                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default ChangePassword

const styles = StyleSheet.create({
    formLogin: {
        flex: 3,
    },
    lableForm: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#034685",
        marginBottom: 5
    },
    borderBotton: {
        "backgroundColor": "#034685",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignSelf: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 9,
        paddingBottom: 9,
        marginTop: 20,
        marginBottom: 20
    },
    textButton: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#FFFFFF",
    },
    inputView: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignContent: "center",
        justifyContent: "center",
        marginBottom: 10
    },
    inputPassword: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        height: 42,
    },
    input: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        height: 45,
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        padding: 10,
        marginBottom: 10
    },
})