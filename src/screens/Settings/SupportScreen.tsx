import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import { Image, Linking, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { COLORS } from '../../constants';
import imgs from '../../constants/imgs'

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const SupportScreen: React.FC<RouterProps> = (props) => {
    const { navigation } = props

    const onClickPhone = (phone: string) => {
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }

        Linking.openURL(phoneNumber);
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: "#F2F2F4" }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignSelf: "center", marginTop: 40, marginBottom: 35 }}>
                        <Image source={imgs.LOGO} style={{ width: 90, height: 90 }} />
                    </View>

                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                        <Text style={styles.title}>TRUNG TÂM THÔNG TIN {'\n'}THỂ DỤC THỂ THAO</Text>
                        <Text style={[styles.content, { color: COLORS.black }]}>
                            <Text style={styles.lable}>Địa chỉ:</Text> Nhà C, 36 Trần Phú - Ba Đình - Hà Nội.
                        </Text>
                        <TouchableOpacity onPress={() => onClickPhone("02437472958")}>
                            <Text style={styles.content}>
                                <Text style={styles.lable}>Số điện thoại:</Text> 0243.747.2958.
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => onClickPhone("19006888")}>
                            <Text style={styles.content}>
                                <Text style={styles.lable}>Hotline: </Text> 1900 6888.
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => Linking.openURL('mailto:support@tdtt.gov.vn')}>
                            <Text style={styles.content}>
                                <Text style={styles.lable}>Email: </Text> support@tdtt.gov.vn.
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>

        </SafeAreaView>
    )
}

export default SupportScreen

const styles = StyleSheet.create({
    title: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#000000",
        alignSelf: "center",
        marginBottom: 20,
        textAlign: "center"
    },
    content: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#034685",
        paddingBottom: 10
    },
    lable: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        fontWeight: "700",
        color: COLORS.black
    }
})