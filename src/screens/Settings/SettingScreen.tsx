import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationProp } from '@react-navigation/native';
import { Button, Modal } from 'native-base';
import React, { useState } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Toast } from 'react-native-toast-message/lib/src/Toast';
import SvgIconSetting from '../../assets/svg/SvgIconSetting';
import { ROUTES } from '../../constants';
import imgs from '../../constants/imgs';
import { useDataContextNavigation } from '../../navigations/ContextNavigation';

interface RouterProps {
  navigation: NavigationProp<any, any>;
}

const SettingScreen: React.FC<RouterProps> = props => {
  const { navigation } = props;
  const { isLogin, updateContextData } = useDataContextNavigation();
  const [showModal, setShowModal] = useState<boolean>(false);

  const onLogOut = async () => {
    setShowModal(false);
    try {
      await AsyncStorage.removeItem('@userInfoData');
      await AsyncStorage.removeItem('@userInfoDataDetail');
      updateContextData(false);
      Toast.show({
        type: 'success',
        text1: 'Đăng xuất thành công',
        topOffset: 60,
      });
    } catch (e) { }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: '#F2F2F4' }}>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ alignSelf: 'center', marginTop: 40, marginBottom: 35 }}>
            <Image source={imgs.LOGO} style={{ width: 90, height: 90 }} />
          </View>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate(ROUTES.INTRODUCE);
            }}>
            <View style={styles.boxSetting}>
              <View style={{ flex: 1 }}>
                <Image
                  source={imgs.IMG_SETTING_1}
                  style={{ width: 20, height: 20 }}
                />
              </View>
              <View style={styles.boxRight}>
                <Text style={styles.titleSetting}>Giới thiệu về ứng dụng</Text>
                <View>
                  <SvgIconSetting />
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate(ROUTES.COMING_SOON);
            }}>
            <View style={styles.boxSetting}>
              <View style={{ flex: 1 }}>
                <Image
                  source={imgs.IMG_SETTING_2}
                  style={{ width: 20, height: 20 }}
                />
              </View>
              <View style={styles.boxRight}>
                <Text style={styles.titleSetting}>Đánh giá ứng dụng</Text>
                <View>
                  <SvgIconSetting />
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <View style={styles.boxSetting}>
            <View style={{ flex: 1 }}>
              <Image
                source={imgs.IMG_SETTING_3}
                style={{ width: 20, height: 20 }}
              />
            </View>
            <View style={styles.boxRight}>
              <Text style={styles.titleSetting}>Phiên bản: 11.0</Text>
            </View>
          </View>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate(ROUTES.SUPPORT);
            }}>
            <View style={styles.boxSetting}>
              <View style={{ flex: 1 }}>
                <Image
                  source={imgs.IMG_SETTING_4}
                  style={{ width: 20, height: 20 }}
                />
              </View>
              <View style={styles.boxRight}>
                <Text style={styles.titleSetting}>Hỗ trợ</Text>
                <View>
                  <SvgIconSetting />
                </View>
              </View>
            </View>
          </TouchableOpacity>

          {isLogin ? (
            <>
              <TouchableOpacity
                onPress={() => navigation.navigate(ROUTES.CHANGE_PASSWORD)}>
                <View style={styles.boxSetting}>
                  <View style={{ flex: 1 }}>
                    <Image
                      source={imgs.IMG_SETTING_5}
                      style={{ width: 20, height: 20 }}
                    />
                  </View>
                  <View style={styles.boxRight}>
                    <Text style={styles.titleSetting}>Đổi mật khẩu</Text>
                    <View>
                      <SvgIconSetting />
                    </View>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => setShowModal(true)}>
                <View style={styles.boxSetting}>
                  <View style={{ flex: 1 }}>
                    <Image
                      source={imgs.IMG_SETTING_6}
                      style={{ width: 20, height: 20 }}
                    />
                  </View>
                  <View style={styles.boxRight}>
                    <Text style={styles.titleSetting}>Đăng xuất</Text>
                    <View>
                      <SvgIconSetting />
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </>
          ) : (
            ''
          )}
        </ScrollView>
      </View>

      <Modal
        isOpen={showModal}
        onClose={() => {
          setShowModal(false);
        }}>
        <Modal.Content>
          <Modal.Body>
            <View>
              <Text style={styles.titleModal}>Thông báo</Text>
              <Text style={styles.contentModal}>
                Bạn có chắc muốn đăng xuất?
              </Text>
            </View>
            <View style={styles.buttonModalNotification}>
              <Button
                onPress={() => {
                  setShowModal(false);
                }}
                style={[
                  styles.buttonModal,
                  { backgroundColor: '#ED1C24', flex: 0.5 },
                ]}>
                <Text style={styles.textModal}>Bỏ qua</Text>
              </Button>

              <Button
                onPress={onLogOut}
                style={[
                  styles.buttonModal,
                  { backgroundColor: '#034685', flex: 0.5 },
                ]}>
                <Text style={styles.textModal}>Đồng ý</Text>
              </Button>
            </View>
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </SafeAreaView>
  );
};

export default SettingScreen;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 15,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  titleSetting: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 15,
    color: '#2D2F34',
  },
  boxSetting: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
    backgroundColor: '#FFFFFF',
    marginBottom: 10,
    alignItems: 'center',
  },
  boxRight: {
    flex: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleModal: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 15,
    textAlign: 'center',
    color: '#2D2F34',
  },
  contentModal: {
    fontFamily: 'Quicksand-Medium',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 15,

    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#000000',
    paddingTop: 14,
  },
  buttonModalNotification: {
    flex: 1,
    flexDirection: 'row',
    gap: 10,
    justifyContent: 'center',
    marginTop: 24,
  },
  buttonModal: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    width: 136,
  },
  textModal: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 15,

    textAlign: 'center',
    color: '#FFFFFF',
  },
  tileFooter: {
    fontFamily: 'Quicksand-SemiBold',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 15,

    color: '#034685',
  },
});
