import { NavigationProp } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { BackHandler, Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import imgs from '../../constants/imgs'
import { ROUTES } from '../../constants';

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const IntroduceScreen: React.FC<RouterProps> = (props) => {
    const { navigation } = props

    return (
        <SafeAreaView style={{ flex: 1 }}>

            <View style={{ flex: 1, backgroundColor: "#F2F2F4" }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignSelf: "center", marginTop: 40, marginBottom: 20 }}>
                        <Image source={imgs.LOGO} style={{ width: 120, height: 120 }} />
                    </View>

                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                        <Text style={styles.title}>THỂ THAO VIỆT NAM</Text>
                        <Text style={styles.content}>
                            Ứng dụng THỂ THAO VIỆT NAM là ứng dụng chính thức của Tổng Cục Thể Dục Thể Thao. Ứng dụng được xây dựng với mục tiêu cung cấp các thông tin, tiện ích liên quan đến thể dục - thể thao, tiêu biểu như:
                        </Text>
                        <View style={styles.dot}>
                            <Text style={styles.content}>
                                {'\u2022'} Tin tức thể thao: Cung cấp các thông tin nóng về tình hình thể dục - thể thao trong và ngoài nước
                            </Text>
                            <Text style={styles.content}>
                                {'\u2022'} Lịch thi đấu: Cập nhật, theo dõi các lịch thi đấu nhanh chóng, dễ dàng
                            </Text>
                            <Text style={styles.content}>
                                {'\u2022'} Cung cấp thông tin: Cung cấp các thông tin về môn thi đấu, liên đoàn hiệp hội
                            </Text>
                            <Text style={styles.content}>
                                {'\u2022'} Liên thông ứng dụng: liên thông với các ứng dụng trong nền tảng số của Tổng Cục Du Lịch
                            </Text>
                        </View>
                        <Text style={styles.content}>Và rất nhiều tiện ích khác......</Text>
                    </View>
                </ScrollView>
            </View>

        </SafeAreaView>
    )
}

export default IntroduceScreen

const styles = StyleSheet.create({
    title: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#000000",
        alignSelf: "center",
        marginBottom: 20,
        paddingTop: 5
    },
    content: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,
        "color": "#000000",
        paddingBottom: 5,
        paddingTop: 5
    },
    dot: {
        paddingLeft: 5
    }
})