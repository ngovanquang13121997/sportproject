import { NavigationProp } from '@react-navigation/native';
import axios from 'axios';
import { get } from 'lodash';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Dimensions, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';
import SvgComingSoon from '../../assets/svg/SvgComingSoon';
import { COLORS } from '../../constants';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

interface Props {
    navigation: NavigationProp<any, any>
}

const FlightScreen: React.FC<Props> = (props) => {
    const { navigation } = props
    const [loading, setLoading] = useState<boolean>(false)
    const [sourceUri, setSourceUri] = useState<string | undefined>(undefined)

    useEffect(() => {
        onGetDataLayoutHotel()
    }, [])

    const onGetDataLayoutHotel = async () => {
        setLoading(true)
        try {
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'X-HTTP-Method-Override': "get_webview_url"
                }
            };

            const res = await axios.get(`https://biin-api.tourism.com.vn/api/dulich/v1/gotadis/get_webview_url?product=flight&layout=single`, config)
            if (res && res.status === 200) {
                setSourceUri(get(res, 'data.result.RedirectUrl'))
                setLoading(false)
            } else {
                setSourceUri(undefined)
                setLoading(false)
            }

        } catch (error) {
            setLoading(false)
            setSourceUri(undefined)
        }
    }

    return (
        <View style={{ flex: 1 }}>
            {loading
                ? <ActivityIndicator style={{ marginTop: 10 }} />
                : sourceUri ? <WebView
                    style={styles.webview}
                    source={{ uri: sourceUri }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    startInLoadingState={false}
                    scalesPageToFit={true}
                />
                    : <View style={styles.bgColor}>
                        <View style={styles.comingSoon}>
                            <View style={styles.logo}>
                                <SvgComingSoon />
                            </View>
                            <Text style={styles.textComingSoon}>Tính năng sẽ được cập nhật trong phiên{"\n"} bản tiếp theo.</Text>
                            <TouchableOpacity onPress={() => {
                                navigation.goBack()
                            }} style={styles.bgButton}>
                                <Text style={styles.back}>Trở lại</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

            }
        </View>
    )
}

export default FlightScreen

const styles = StyleSheet.create({
    webview: {
        flex: 1,
        width: deviceWidth,
        height: deviceHeight
    },
    logo: {
        alignSelf: "center",
        marginBottom: 20
    },
    bgColor: {
        flex: 1,
        backgroundColor: COLORS.bgColor,
    },
    comingSoon: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "center",
    },
    textComingSoon: {
        "fontFamily": "Quicksand",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: "center"
    },
    bgButton: {
        backgroundColor: COLORS.white,
        borderRadius: 15,
        alignSelf: 'center',
        marginTop: 28
    },
    back: {
        alignSelf: "center",
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": "#034685",
        "paddingTop": 12,
        "paddingRight": 49,
        "paddingBottom": 12,
        "paddingLeft": 49,
    }
})