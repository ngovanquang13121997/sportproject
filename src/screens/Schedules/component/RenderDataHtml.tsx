import React from 'react';
import { ScrollView, StyleSheet, useWindowDimensions } from 'react-native';
import RenderHTML from 'react-native-render-html';
import WebView from 'react-native-webview';
import TableRenderer, { tableModel } from '@native-html/table-plugin';

const RenderDataHtml = React.memo(function WebDisplay({ html }: any) {
    const { width: contentWidth } = useWindowDimensions();

    const htmlProps = {
        renderers: {
            table: TableRenderer
        },
        renderersProps: {
            table: {

            }
        },
        customHTMLElementModels: {
            table: tableModel
        }
    };


    return (
        <RenderHTML
            contentWidth={contentWidth}
            source={{ html }}
            {...htmlProps}
            WebView={WebView}
            defaultWebViewProps={{
                scrollEnabled: false
            }}
            baseStyle={{
                "fontFamily": "Quicksand-Regular",
                "fontStyle": "normal",
                "fontWeight": "400",
                "fontSize": 15,
                padding: 10,
                textAlign: "left"
            }}
        />
    );
});

export default RenderDataHtml

const styles = StyleSheet.create({})