import { NavigationProp, useRoute } from '@react-navigation/native'
import { decode } from 'html-entities'
import { isEmpty } from 'lodash'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Image, Linking, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { COLORS, IMGS, ROUTES } from '../../../constants'
import ApiHelper from '../../../network/ApiClient'
import HeaderDetail from '../../../views/components/HeaderDetail'
import TextComponent from '../../../views/components/TextComponent'
import ShareComponent from '../Share'
import RenderDataHtml from './RenderDataHtml'
export interface IDataDetail {
    "gameId": number,
    "sportId": number,
    "sportName": string,
    "gameName": string,
    "startDate": any,
    "endDate": any,
    "venue": string,
    "address"?: string,
    "city"?: string,
    "schedule"?: any,
    iconSport?: string
}

interface IOtherData {
    [anyProps: string]: any
}
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const ViewSchedules: React.FC<RouterProps> = (props) => {
    const [loading, setLoading] = useState<boolean>(false)
    const [dataDetail, setDataDetail] = useState<IDataDetail | undefined>(undefined)
    const [otherData, setOtherData] = useState<IOtherData[]>([])

    const route = useRoute<any>();
    const { gameId, gameName, iconSport } = route.params

    useEffect(() => {
        if (gameId) {
            onGetDataDetail(gameId)
        }
    }, [gameId])

    const onGetDataDetail = async (gameId: number) => {
        setLoading(true)
        try {
            const res = await ApiHelper.fetch(`/game/${gameId}`)
            if (res && res.data) {
                setDataDetail(res.data)
                setLoading(false)
                await onGetOtherData(res?.data?.sportId)
            } else {
                setDataDetail(undefined)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
        }
    }

    const onGetOtherData = async (sportId?: string) => {
        if (sportId) {
            try {
                const res = await ApiHelper.fetch(`/game/sport/${sportId}`, {
                    start: 0,
                    limit: 5
                })
                if (res && res.data) {
                    setOtherData(res.data)
                } else {
                    setOtherData([])
                }
            } catch (error) {
                setOtherData([])
            }
        }
    }

    return (
        <SafeAreaView style={styles.bgWhite}>
            <ScrollView>
                <View style={styles.marginTop}>
                    <HeaderDetail pathSvg={iconSport} image={IMGS.SCHEDULES_DETAIL} title={gameName} />
                </View>

                {loading
                    ? <ActivityIndicator style={{ marginTop: 10 }} />
                    : <>
                        <ShareComponent dataDetail={dataDetail} navigation={props.navigation} />

                        <View style={styles.listSchedule}>
                            <Text style={styles.title}>THỜI GIAN:</Text>
                            <Text style={styles.content}>Từ ngày {moment(dataDetail?.startDate).format('DD/MM')} đến ngày  {moment(dataDetail?.endDate).format('DD/MM/YYYY')}</Text>
                        </View>

                        <View style={styles.listSchedule}>
                            <Text style={styles.title}>ĐỊA ĐIỂM:</Text>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.content}>{dataDetail?.venue}</Text>
                                <TouchableOpacity style={styles.imageMap}
                                    onPress={() =>
                                        Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${dataDetail?.venue}`)
                                    }
                                >
                                    <Image source={IMGS.ICON_MAP} style={{ width: "100%", height: "100%" }} />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={styles.listSchedule}>
                            <Text style={styles.title}>ĐỊA CHỈ:</Text>
                            <TextComponent styleProps={styles.content} title={dataDetail?.address} />
                        </View>

                        <View style={styles.listSchedule}>
                            <Text style={styles.title}>THÀNH PHỐ:</Text>
                            <TextComponent styleProps={styles.content} title={dataDetail?.city} />
                        </View>

                        <View style={styles.listScheduleHtml}>
                            <Text style={styles.titleHtml}>LỊCH THI ĐẤU DỰ KIẾN:</Text>
                        </View>
                        {dataDetail?.schedule
                            ? <View >
                                {dataDetail?.schedule === "<div>Đang cập nhật</div>"
                                    ? <TextComponent styleProps={[styles.textSportName, { paddingLeft: 10 }]} />
                                    : <RenderDataHtml html={decode(dataDetail?.schedule)} />}
                            </View>
                            : ""}
                        <View style={styles.listScheduleHtml}>
                            <Text style={styles.titleHtml}>MÔN THI ĐẤU:</Text>
                            <TouchableOpacity onPress={() => {
                                props.navigation.navigate(ROUTES.SPORTS_DETAIL, { sportId: dataDetail?.sportId })
                            }}>
                                <TextComponent styleProps={styles.textSportName} title={dataDetail?.sportName} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.listScheduleHtml}>
                            <Text style={styles.titleHtml}>CÁC GIẢI ĐẤU KHÁC:</Text>
                            {!isEmpty(otherData)
                                ? otherData.map((d) => {
                                    return (
                                        <TouchableOpacity
                                            key={d?.gameId}
                                            onPress={() => {
                                                props.navigation.navigate(ROUTES.SCHEDULES_DETAIL, {
                                                    gameId: d?.gameId,
                                                    gameName: d?.gameName,
                                                    iconSport: d?.iconSport
                                                })
                                            }}>
                                            <Text style={styles.textSportName}>{d?.gameName}</Text>
                                        </TouchableOpacity>
                                    )
                                })
                                : ""}
                        </View>
                    </>}

            </ScrollView>
        </SafeAreaView>
    )
}

export default ViewSchedules

const styles = StyleSheet.create({
    bgWhite: {
        backgroundColor: COLORS.white,
        flex: 1
    },
    marginTop: {
        ...Platform.select({
            android: {
                marginTop: 40
            },
        }),
    },
    listSchedule: {
        flexDirection: "row",
        flex: 1,
        marginBottom: 10,
        paddingLeft: 10,
        gap: 5
    },
    title: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        flex: 0.5,
        "color": COLORS.black,
        width: 100
    },
    listScheduleHtml: {
        marginBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },
    titleHtml: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "color": COLORS.black,
        marginTop: 5,
    },
    content: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        flex: 1,
        "color": "#011AFF",
        marginBottom: 20
    },
    boxSchedule: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 10
    },
    titleContentSchedule: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "color": COLORS.black,
        marginBottom: 8
    },
    contentSchedule: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#011AFF",
        marginBottom: 12
    },
    imageMap: {
        width: 30,
        height: 30
    },
    textSportName: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#011AFF",
        marginTop: 8,
        marginBottom: 10
    }
})