import { NavigationProp } from '@react-navigation/native'
import moment from 'moment'
import React, { useState } from 'react'
import { ActivityIndicator, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useInfiniteQuery } from 'react-query'
import SvgComingSoon from '../../assets/svg/SvgComingSoon'
import { COLORS, ROUTES } from '../../constants'
import { newsApi } from '../../network/services'
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const ScheduleScreen = ({ navigation }: RouterProps) => {
    const [limit, setLimit] = useState<number>(0)
    const { data, isLoading, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery(
        'schedule',
        ({ pageParam = 0 }) => newsApi.getDataListGames(pageParam),
        {
            getNextPageParam: (lastPage, allPages) => {
                if (lastPage.length === 0 || lastPage === undefined) return false
                return limit
            },
            staleTime: 120000,
            cacheTime: 120000,
            refetchOnWindowFocus: false
        }
    )

    const fetchMoreData = async () => {
        if (hasNextPage && !isFetchingNextPage) {
            await setLimit(limit + 10)
            await fetchNextPage()
        }
    }

    return (
        <SafeAreaView style={styles.bgColor}>
            {isLoading
                ? <ActivityIndicator style={{ marginTop: 10 }} />
                : <FlatList
                    data={data?.pages.map(page => page).flat()}
                    numColumns={1}
                    contentContainerStyle={{
                        padding: 15
                    }}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={styles.itemCard} onPress={() => {
                                navigation.navigate(ROUTES.SCHEDULES_DETAIL, {
                                    gameId: item.gameId,
                                    gameName: item.gameName,
                                    iconSport: item?.iconSport
                                })
                            }}>
                                <View style={styles.viewIageSchedules}>
                                    {item?.iconSport
                                        ? <SvgUri
                                            width="60"
                                            height="60"
                                            fill={COLORS.white}
                                            uri={item.iconSport}
                                        />
                                        : <SvgComingSoon width={60} height={60} />
                                    }
                                </View>
                                <View style={styles.itemContent}>
                                    <Text numberOfLines={2} style={styles.titleContent}>{item.gameName}</Text>
                                    <Text numberOfLines={1} style={styles.titleTime}>
                                        Từ ngày {moment(item.startDate).format('DD/MM')} đến ngày  {moment(item.endDate).format('DD/MM/YYYY')}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item, key) => String(key + 1)}
                    onEndReachedThreshold={0.7}
                    onEndReached={fetchMoreData}
                    ListFooterComponent={isFetchingNextPage ? <ActivityIndicator /> : null}
                />
            }

        </SafeAreaView>
    )
}

export default ScheduleScreen

const styles = StyleSheet.create({
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1,
    },
    itemCard: {
        flex: 1,
        flexDirection: "row",
        marginBottom: 20,
    },
    viewIageSchedules: {
        flex: 1,
    },
    itemContent: {
        flex: 4
    },
    titleContent: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "color": "#FFE601",
        marginBottom: 10
    },
    titleTime: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "color": COLORS.white
    }
})