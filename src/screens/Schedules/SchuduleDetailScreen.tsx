import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import ViewSchedules from './component/ViewSchedules'
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const SchuduleDetailScreen: React.FC<RouterProps> = (props) => {
    return <ViewSchedules navigation={props.navigation} />
}

export default SchuduleDetailScreen