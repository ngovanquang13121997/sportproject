import AsyncStorage from '@react-native-async-storage/async-storage'
import { isEmpty, isEqual, uniq } from 'lodash'
import moment from 'moment'
import { Button, Modal } from 'native-base'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import RNCalendarEvents from "react-native-calendar-events"
import SvgLike from '../../assets/svg/SvgLike'
import SvgLikeActive from '../../assets/svg/SvgLikeActive'
import { COLORS } from '../../constants'
import { listCalendars } from '../../views/container/LocalCalendarService'

import Share from 'react-native-share'
import SvgShare from '../../assets/svg/SvgShare'
import { IDataDetail } from './component/ViewSchedules'
interface Props {
    dataDetail: IDataDetail | undefined
    navigation?: any
}

const ShareComponent: React.FC<Props> = (props) => {
    const { dataDetail, navigation } = props
    const [modalVisible, setModalVisible] = useState(false);

    const [isSave, setIsSave] = useState<boolean>(false)

    useEffect(() => {
        if (dataDetail) {
            onCheckSave()
        }
    }, [dataDetail])

    const onCheckSave = async () => {
        const arrGameIds: string | null = await AsyncStorage.getItem('@arrGameIds');
        if (arrGameIds) {
            const myArray: string[] = JSON.parse(arrGameIds);
            setIsSave(myArray.includes(String(dataDetail?.gameId!)))
        }
    }

    const onSaveData = async () => {
        setModalVisible(false)
        if (isSave) {
            AsyncStorage.getItem('@arrGameIds')
                .then((myArrayString: string | null) => {
                    if (myArrayString) {
                        // Chuyển đổi chuỗi JSON thành mảng
                        const myArray: string[] = JSON.parse(myArrayString);

                        // Xoá phần tử "name" khỏi mảng
                        const index = myArray.indexOf(String(dataDetail?.gameId!));
                        if (index > -1) {
                            myArray.splice(index, 1);
                        }

                        // Chuyển đổi lại mảng thành chuỗi JSON
                        const arrGameIds = JSON.stringify(myArray);

                        // Lưu trữ lại giá trị mới của mảng vào AsyncStorage
                        AsyncStorage.setItem('@arrGameIds', arrGameIds);
                    }
                })
        } else {
            const arrGameIds: string | null = await AsyncStorage.getItem('@arrGameIds');
            if (arrGameIds) {
                const myArray: string[] = JSON.parse(arrGameIds);
                myArray.push(String(dataDetail?.gameId))

                const myArrayGameIds = JSON.stringify(uniq(myArray));
                await AsyncStorage.setItem('@arrGameIds', myArrayGameIds);
            } else {
                const myArray: string[] = [String(dataDetail?.gameId)];
                const arrGameId: string = JSON.stringify(myArray);
                await AsyncStorage.setItem('@arrGameIds', arrGameId);
            }
        }

        if (!isSave) {
            // Check quyền app Lịch
            const calendarsTmp = await listCalendars();
            if (!isEmpty(calendarsTmp)) {
                if (dataDetail) {
                    const dateStart = moment(dataDetail?.startDate).toISOString()
                    const dateEnd = moment(dataDetail?.endDate).toISOString()

                    // Gắn Events vào dữ liệu App lịch
                    RNCalendarEvents.saveEvent(dataDetail?.gameName, {
                        startDate: dateStart,
                        endDate: dateEnd,
                        location: dataDetail?.venue,
                        notes: `Từ ngày ${moment(dataDetail?.startDate).format('DD/MM')} đến ngày  ${moment(dataDetail?.endDate).format('DD/MM/YYYY')}`,
                        allDay: true,
                        calendarId: "123",
                    })
                }
            }
        } else {
            if (dataDetail) {
                const dateStart = moment(dataDetail?.startDate).toISOString()
                const dateEnd = moment(dataDetail?.endDate).toISOString()

                const oldData = {
                    title: dataDetail?.gameName,
                    location: dataDetail?.venue,
                }

                RNCalendarEvents.fetchAllEvents(dateStart, dateEnd).then((events) => {
                    for (let event of events) {
                        const newData = {
                            title: event?.title,
                            location: event?.location,
                        }

                        if (isEqual(oldData, newData)) {
                            RNCalendarEvents.removeEvent(event.id)
                        }
                    }
                })
            }
        }

        setIsSave(!isSave)
    }

    const onShareFacebook = async () => {
        if (dataDetail) {
            const content = `${dataDetail?.gameName}\n\n Thời gian diễn ra: ${moment(dataDetail?.startDate).format('DD-MM-YYYY')} - ${moment(dataDetail?.endDate).format('DD-MM-YYYY')}\n\n Nhà thi đấu: ${dataDetail?.venue}\n\n Địa chỉ: ${dataDetail?.address}\n\n Thành phố: ${dataDetail?.city} \n\n#thethaovietnam`

            const shareOptions = {
                title: "Thể thao Việt Nam",
                message: content,
                url: 'https://tdtt.gov.vn/'
            };

            try {
                await Share.open(shareOptions);
            } catch (error) {

            }
        }
    }

    return (
        <>
            <View style={styles.cardShare}>
                <View style={styles.leftShare}>
                    <Text style={styles.titleShare}>CHIA SẺ:</Text>
                    <TouchableOpacity onPress={onShareFacebook}>
                        <SvgShare />
                    </TouchableOpacity>
                </View>
                <View style={styles.rightShare}>
                    <Text style={[styles.titleShare]}>{isSave ? 'BỎ THEO DÕI:' : 'THEO DÕI:'}</Text>
                    <TouchableOpacity onPress={() => {
                        setModalVisible(true)
                    }}>
                        {isSave ? <SvgLikeActive /> : <SvgLike />}
                    </TouchableOpacity>
                </View>
            </View >

            <View style={{ flex: 1 }}>
                <Modal
                    isOpen={modalVisible}
                    onClose={() => {
                        setModalVisible(false)
                    }}
                    size={"md"}>
                    <Modal.Content maxH="212">
                        <Modal.Body>
                            <View>
                                <Text style={styles.title}>Thông báo</Text>
                                <Text style={styles.content}>
                                    {isSave
                                        ? 'Bạn có muốn hủy theo dõi lịch thi đấu này. Bạn sẽ không nhận được các thông báo về lịch thi đấu này.'
                                        : 'Bạn có muốn theo dõi lịch thi đấu này. Bạn sẽ nhận được các thông báo về lịch thi đấu này.'}
                                </Text>
                            </View>
                            <View style={styles.buttonModalNotification}>
                                {isSave
                                    ? <Button onPress={() => {
                                        navigation.goBack()
                                    }} style={[styles.buttonModal, { backgroundColor: "#ED1C24", flex: 0.5 }]}>
                                        <Text style={styles.textModal}>Bỏ qua</Text>
                                    </Button>
                                    : <Button onPress={() => {
                                        setModalVisible(false)
                                    }} style={[styles.buttonModal, { backgroundColor: "#ED1C24", flex: 0.5 }]}>
                                        <Text style={styles.textModal}>Bỏ qua</Text>
                                    </Button>
                                }

                                <Button onPress={onSaveData} style={[styles.buttonModal, { backgroundColor: "#034685", flex: 0.5 }]}>
                                    <Text style={styles.textModal}>Đồng ý</Text>
                                </Button>
                            </View>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
            </View>
        </>
    )
}

export default ShareComponent

const styles = StyleSheet.create({
    cardShare: {
        flex: 1,
        flexDirection: "row",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 14,
        backgroundColor: "rgba(217, 217, 217, 0.6)",
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10
    },
    leftShare: {
        flex: 0.5,
        flexDirection: "row",
        gap: 16,
        alignItems: "center"
    },
    rightShare: {
        flex: 0.5,
        flexDirection: "row",
        gap: 20,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    titleShare: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": COLORS.black
    },
    title: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#2D2F34"
    },
    content: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#000000",
        paddingTop: 14,
    },
    buttonModalNotification: {
        flex: 1,
        flexDirection: "row",
        gap: 10,
        justifyContent: "center",
        marginTop: 24
    },
    buttonModal: {
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        width: 136,
    },
    textModal: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#FFFFFF"
    }
})