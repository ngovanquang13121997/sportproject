import { useRoute } from '@react-navigation/native';
import { decode } from 'html-entities';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { COLORS } from '../../constants';
import ApiHelper from '../../network/ApiClient';
import RenderDataHtml from './RenderDataHtml';
interface IDataDetail {
    articleID: number,
    title: string,
    summary?: any,
    cleanArticleData?: any
    publishDate?: any
    numberOfViews: number
    articleContent?: any
}

const DetailScreen = () => {
    const route = useRoute<any>();
    const { articleID, articleImage, title } = route.params
    const [loading, setLoading] = useState<boolean>(false)
    const [dataDetail, setDataDetail] = useState<IDataDetail | undefined>(undefined)

    useEffect(() => {
        if (articleID) {
            onGetDataDetail(articleID)
        }
    }, [articleID])

    const onGetDataDetail = async (articleID: number) => {
        setLoading(true)
        try {
            const res = await ApiHelper.fetch(`/article/${articleID}`)

            if (res && res.data) {
                setDataDetail(res.data)
                setLoading(false)
            } else {
                setDataDetail(undefined)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
        }
    }

    return (
        <ScrollView>
            <View style={styles.detailHeader}>
                <ImageBackground
                    source={{
                        uri: articleImage
                    }}
                    style={styles.imageDetail}

                >
                    <LinearGradient
                        colors={['#00000000', '#000000']}
                        style={{ height: '100%', width: '100%' }} />
                </ImageBackground>
                <Text style={styles.titleContentt}>{title}</Text>
            </View>

            {loading
                ? <ActivityIndicator style={{ marginTop: 10 }} />
                : <>
                    <View style={styles.flexRow}>
                        <Text style={styles.date}>Ngày đăng: {dataDetail?.publishDate ? moment(dataDetail?.publishDate).format('DD-MM-YYYY') : ""}</Text>
                        <Text style={styles.view}>Lượt xem: {dataDetail?.numberOfViews || 0}</Text>
                    </View>
                    <View style={styles.border} />
                    <View>
                        <RenderDataHtml html={decode(dataDetail?.articleContent)} />
                    </View>
                </>
            }
        </ScrollView>
    )
}

export default DetailScreen


const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
    },
    detailHeader: {
        height: 380,
        position: "relative",
    },
    imageDetail: {
        width: "100%",
        height: "100%",
        resizeMode: "contain",
    },
    titleContentt: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        position: "absolute",
        bottom: 30,
        paddingLeft: 22,
        paddingRight: 22
    },
    flexRow: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 8
    },
    date: {
        "fontFamily": "Quicksand-Light",
        "fontStyle": "normal",
        "fontWeight": "300",
        "fontSize": 15,
        "color": "#000000"
    },
    view: {
        "fontFamily": "Quicksand-Light",
        "fontStyle": "normal",
        "fontWeight": "300",
        "fontSize": 15,
        "color": "#000000"
    },
    border: {
        borderTopWidth: 1,
        "borderColor": "#847676",
        "borderStyle": "solid",
        marginLeft: 16,
        marginRight: 16,
        marginTop: 10
    }
})