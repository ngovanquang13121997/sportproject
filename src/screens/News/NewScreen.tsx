import { NavigationProp } from '@react-navigation/native';
import { uniqBy } from 'lodash';
import React, { useState } from 'react';
import { ActivityIndicator, Alert, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useInfiniteQuery } from 'react-query';
import { COLORS, ROUTES } from '../../constants';
import { newsApi } from '../../network/services';
import Header from '../../views/components/Header';
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const NewScreen = ({ navigation }: RouterProps) => {
    const [limit, setLimit] = useState<number>(0)

    const { data, isLoading, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery(
        'news',
        ({ pageParam = limit }) => newsApi.getDataListNews(pageParam),
        {
            getNextPageParam: (lastPage) => {
                if (lastPage.length === 0 || lastPage === undefined) return false
                return limit
            },
            staleTime: 120000,
            cacheTime: 120000,
            refetchOnWindowFocus: false,
        }
    )

    const fetchMoreData = async () => {
        if (hasNextPage && !isFetchingNextPage) {
            await setLimit(limit + 10)
            await fetchNextPage()
        }
    }

    return (
        <SafeAreaView style={styles.bgColor}>
            <View style={{ flex: 1 }}>
                {isLoading
                    ? <ActivityIndicator />
                    : <FlatList
                        data={data?.pages.map(page => page).flat()}
                        numColumns={1}
                        contentContainerStyle={{
                            paddingLeft: 15,
                            paddingRight: 15,
                            paddingTop: 15
                        }}

                        renderItem={({ item }) => {
                            return (
                                <TouchableOpacity style={styles.itemCard} onPress={() => {
                                    navigation.navigate(ROUTES.NEWS_DETAIL, item)
                                }}>
                                    <Image source={{
                                        uri: item.articleImage
                                    }} style={styles.imageNew} />
                                    <Text style={styles.titleContentCrad}>{item.title}</Text>
                                </TouchableOpacity>
                            )
                        }}
                        keyExtractor={(item, key) => String(key + 1)}
                        onEndReachedThreshold={0.7}
                        onEndReached={fetchMoreData}
                        ListFooterComponent={isFetchingNextPage ? <ActivityIndicator /> : null}
                    />
                }
            </View>
        </SafeAreaView>
    )
}

export default NewScreen

const styles = StyleSheet.create({
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1,
    },
    itemCard: {
        flexDirection: "row",
        flex: 1,
        gap: 10,
        marginBottom: 15
    },
    imageNew: {
        flex: 2,
        borderRadius: 8,
        resizeMode: "cover",
        height: 90
    },
    titleContentCrad: {
        "fontFamily": "Quicksand-Bold",
        flex: 3,
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: 15,
        color: COLORS.white
    }
})