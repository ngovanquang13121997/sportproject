import React from 'react';
import { StyleSheet, useWindowDimensions } from 'react-native';
import RenderHTML from 'react-native-render-html';
import { COLORS } from '../../constants';
import IframeRenderer, { iframeModel } from '@native-html/iframe-plugin';
import WebView from 'react-native-webview';

const RenderDataHtml = React.memo(function WebDisplay({ html }: any) {
    const { width: contentWidth } = useWindowDimensions();
    const renderers = {
        iframe: IframeRenderer
    };

    const customHTMLElementModels = {
        iframe: iframeModel
    };

    return (
        <RenderHTML
            renderers={renderers}
            WebView={WebView}
            contentWidth={contentWidth}
            source={{ html }}
            customHTMLElementModels={customHTMLElementModels}
            ignoredStyles={['width']}
            baseStyle={{
                "fontFamily": "Quicksand-Regular",
                "fontStyle": "normal",
                "fontWeight": "400",
                "fontSize": 15,
                paddingTop: 10,
                paddingBottom: 16,

                color: COLORS.black,
                flexShrink: 1,
                lineHeight: 20,
                padding: 20
            }}
            defaultWebViewProps={{
                androidHardwareAccelerationDisabled: false,
                androidLayerType: 'hardware',
                mixedContentMode: 'always'
            }}

            renderersProps={{
                iframe: {
                    scalesPageToFit: true,
                    webViewProps: {
                        allowsFullScreen: true,
                    },
                }
            }}
            tagsStyles={{
                iframe: {
                    left: -20
                },
            }}
        />
    );
});

export default RenderDataHtml

const styles = StyleSheet.create({})