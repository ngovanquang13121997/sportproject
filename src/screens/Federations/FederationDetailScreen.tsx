import { StyleSheet, Text, SafeAreaView, ScrollView, View, Image, ActivityIndicator, TouchableOpacity, Platform, Linking } from 'react-native'
import React, { useEffect, useState } from 'react'
import { COLORS, IMGS } from '../../constants'
import HeaderDetail from '../../views/components/HeaderDetail'
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs'
import { useRoute } from '@react-navigation/native'
import ApiHelper from '../../network/ApiClient'
import TextComponent from '../../views/components/TextComponent'
import { get } from 'lodash'
import moment from 'moment'

const DATA_DETAIL = [
    { lable: "Viết tắt", content: "abbreviationName" },
    { lable: "Thành lập", content: "foundingDate", type: "date" },
    { lable: "Chủ tịch", content: "president" },
    { lable: "Tổng thư ký", content: "generalSecretary" },
    { lable: "Telephone", content: "phoneNumber", typeLinking: "PHONE" },
    { lable: "Fax", content: "fax", typeLinking: "PHONE" },
    { lable: "Email", content: "email", typeLinking: "EMAIL" },
    { lable: "Website", content: "website", type: "link", typeLinking: "WEBSITE" },
    { lable: "Địa chỉ", content: "address" },
]

interface IDataDetail {
    "federationId": number,
    "sportId": number,
    "sportName": string,
    "fullNameEnglish": string,
    "fullName": string,
    "abbreviationName": string,
    "president": string,
    "generalSecretary": string,
    "phoneNumber": string,
    "fax": string,
    "email": string,
    "website": string,
    "address": string,
    "iconPath": string,
    "isDomestic": boolean,
    foundingDate?: any
}

const FederationDetailScreen = () => {

    const [loading, setLoading] = useState<boolean>(false)
    const [dataDetail, setDataDetail] = useState<IDataDetail | undefined>(undefined)

    const route = useRoute<any>();
    const { federationId, fullName, iconPath } = route.params

    useEffect(() => {
        if (federationId) {
            onGetDataDetail(federationId)
        }
    }, [federationId])

    const onGetDataDetail = async (federationId: number) => {
        setLoading(true)
        try {
            const res = await ApiHelper.fetch(`/federation/${federationId}`)

            if (res && res.data) {
                setDataDetail(res.data)
                setLoading(false)
            } else {
                setDataDetail(undefined)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
        }
    }

    const renderText = (d: { content: string, type?: string }, dataInfo?: IDataDetail) => {
        let showText = undefined
        if (dataInfo) {
            showText = get(dataInfo, d.content)
            if (d.type === "date") {
                showText = get(dataInfo, "foundingDate") ? moment(get(dataInfo, "foundingDate")).format('DD/MM/YYYY') : undefined
            }
        }
        return showText
    }

    const onClickLinking = async (typeLinking: string, value: string) => {
        if (typeLinking === "PHONE") {
            let phoneNumber = value;
            if (Platform.OS !== 'android') {
                phoneNumber = `telprompt:${value.replace(/[^\d]/g, "")}`;
            }
            else {
                phoneNumber = `tel:${value.replace(/[^\d]/g, "")}`;
            }

            Linking.openURL(phoneNumber);
        } else if (typeLinking === "EMAIL") {
            Linking.openURL(`mailto:${value}`)
        } else {
            await Linking.openURL(value);
        }
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1 }}>
                <HeaderDetail urlImage={iconPath} title={fullName} />

                {loading
                    ? <ActivityIndicator style={{ marginTop: 10 }} />
                    : <View style={styles.detailFederation}>
                        <View>
                            <Text style={styles.title}>Tên Liên đoàn - Hiệp hội</Text>
                            <TextComponent title={dataDetail?.fullName} styleProps={styles.titleBlue} />
                            <TextComponent title={dataDetail?.fullNameEnglish} styleProps={styles.titleBlue} />
                        </View>
                        <View style={{ marginTop: 16 }}>
                            {DATA_DETAIL.map((d, key) => {
                                return (
                                    d.typeLinking
                                        ? <TouchableOpacity onPress={() => onClickLinking(d.typeLinking, renderText(d, dataDetail))}>
                                            <View key={key} style={styles.cardDetail}>
                                                <Text numberOfLines={1} style={styles.titleCardDetail}>{d.lable}</Text>
                                                <TextComponent title={renderText(d, dataDetail)} styleProps={styles.titleRight} />
                                            </View>
                                        </TouchableOpacity>
                                        : <View key={key} style={styles.cardDetail}>
                                            <Text numberOfLines={1} style={styles.titleCardDetail}>{d.lable}</Text>
                                            <TextComponent title={renderText(d, dataDetail)} styleProps={styles.titleRight} />
                                        </View>
                                )
                            })}
                        </View>
                    </View>
                }

            </ScrollView>
        </SafeAreaView>
    )
}

export default FederationDetailScreen

const styles = StyleSheet.create({
    detailFederation: {
        padding: 25
    },
    cardDetail: {
        flex: 1,
        flexDirection: "row",
        marginBottom: 20
    },
    titleCardDetail: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        "color": COLORS.black,
        flex: 2,
    },
    titleRight: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#011AFF",
        flex: 4
    },
    title: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": COLORS.black
    },
    titleBlue: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#011AFF",
        paddingTop: 10
    }
})