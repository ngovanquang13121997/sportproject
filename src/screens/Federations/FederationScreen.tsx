import { NavigationProp } from '@react-navigation/native';
import React, { useState } from 'react';
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useInfiniteQuery } from 'react-query';
import { COLORS, IMGS, ROUTES } from '../../constants';
import { newsApi } from '../../network/services';
interface RouterProps {
    navigation: NavigationProp<any, any>;
}
interface IDataFederations {
    federationId: number
    fullName: string
    abbreviationName: string
}

const FederationScreen = ({ navigation }: RouterProps) => {
    const [limit, setLimit] = useState<number>(0)

    const { data, isLoading, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery(
        'federations',
        ({ pageParam = limit }) => newsApi.getDataListFederations(pageParam),
        {
            getNextPageParam: (lastPage) => {
                if (lastPage.length === 0 || lastPage === undefined) return false
                return limit
            },
            staleTime: 120000,
            cacheTime: 120000,
            refetchOnWindowFocus: false
        }
    )

    const fetchMoreData = async () => {
        if (hasNextPage && !isFetchingNextPage) {
            await setLimit(limit + 10)
            await fetchNextPage()
        }
    }

    return (
        <SafeAreaView style={styles.bgColor}>
            {isLoading
                ? <ActivityIndicator />
                : <FlatList
                    data={data?.pages.map(page => page).flat()}
                    numColumns={1}
                    contentContainerStyle={{
                        padding: 15
                    }}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={styles.itemCard}
                                onPress={() => {
                                    navigation.navigate(ROUTES.FEDERATIONS_DETAIL, {
                                        federationId: item.federationId,
                                        fullName: item.fullName,
                                        iconPath: item?.iconPath
                                    })
                                }}>
                                <View style={styles.viewImage}>
                                    {item?.iconPath
                                        ? <Image source={{
                                            uri: item?.iconPath
                                        }} style={styles.imageNew} />
                                        : <Image source={IMGS.LOGO} style={styles.imageNew} />
                                    }

                                </View>
                                <View style={styles.boxContent}>
                                    <Text style={styles.titleContentCrad}>{item.fullName}</Text>
                                    <Text style={styles.titleContentCrad}>{item.abbreviationName}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item, index) => String(index + 1)}
                    onEndReachedThreshold={0.7}
                    onEndReached={fetchMoreData}
                    ListFooterComponent={isFetchingNextPage ? <ActivityIndicator /> : null}
                />
            }

        </SafeAreaView>
    )
}

export default FederationScreen

const styles = StyleSheet.create({
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1
    },
    itemCard: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 5,
        borderWidth: 1,
        borderColor: COLORS.white,
        paddingLeft: 17,
        paddingRight: 17,
        paddingTop: 5,
        paddingBottom: 5,
        marginBottom: 18,
        gap: 15
    },
    viewImage: {
        flex: 1,
    },
    imageNew: {
        width: 60,
        height: 80,
        resizeMode: "contain"
    },
    boxContent: {
        flex: 4
    },
    titleContentCrad: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "alignItems": "center",
        "color": COLORS.white,
        paddingBottom: 5
    }
})