import { decode } from 'html-entities';
import { first, isEmpty, last, map, uniq } from 'lodash';
import moment from 'moment';
import { Modal } from 'native-base';
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Accordion from 'react-native-collapsible/Accordion';
import { useQuery } from 'react-query';
import SvgLeft from '../../assets/svg/SvgLeft';
import SvgRightDate from '../../assets/svg/SvgRightDate';
import { COLORS } from '../../constants';
import imgs from '../../constants/imgs';
import ApiHelper from '../../network/ApiClient';
import HeaderSeaGame from '../../views/components/HeaderSeaGame';
import RenderDataHtml from './RenderDataHtml';

const width = Dimensions.get('window').width - 20;

LocaleConfig.locales['vi'] = {
  monthNames: [
    'Tháng 1',
    'Tháng 2',
    'Tháng 3',
    'Tháng 4',
    'Tháng 5',
    'Tháng 6',
    'Tháng 7',
    'Tháng 8',
    'Tháng 9',
    'Tháng 10',
    'Tháng 11',
    'Tháng 12',
  ],
  monthNamesShort: [
    'Thg 1',
    'Thg 2',
    'Thg 3',
    'Thg 4',
    'Thg 5',
    'Thg 6',
    'Thg 7',
    'Thg 8',
    'Thg 9',
    'Thg 10',
    'Thg 11',
    'Thg 12',
  ],
  dayNames: ['CN', 'Th 2', 'Th 3', 'Th 4', 'Th 5', 'Th 6', 'Th 7'],
  dayNamesShort: ['CN', 'Th 2', 'Th 3', 'Th 4', 'Th 5', 'Th 6', 'Th 7'],
  today: 'Hôm nay',
};

LocaleConfig.defaultLocale = 'vi';

interface dataSchedule {
  competitionDate?: string;
  eventDetails?: string;
  eventName?: string;
  id: number | string;
}

const ScheduleSeaGameScreen = () => {
  const [selected, setSelected] = useState<string>(
    moment().format('YYYY-MM-DD[T]00:00:00'),
  );

  const [activeSections, setActiveSections] = useState<number[]>([]);
  const [modalVisible, setModalVisible] = useState(false);

  const [markedDates, setMarkedDates] = useState<any>(undefined);
  const [dates, setDates] = useState<string[]>([]);

  const { data, isLoading, refetch, isFetching } = useQuery({
    queryKey: 'schedule_asian',
    queryFn: () =>
      ApiHelper.fetch('/schedule', { competitionDate: selected }).then(res => {
        return res.data;
      }),
  });

  const { data: medalAll } = useQuery({
    queryKey: 'medalAll',
    queryFn: () =>
      ApiHelper.fetch('/schedules').then(res => {
        const dataAll = res.data;
        if (!isEmpty(dataAll)) {
          setActiveSections([0]);
          const today = moment().format('YYYY-MM-DD[T]00:00:00');

          const dateData = uniq(map(dataAll, 'competitionDate'));
          setDates(dateData);

          if (!isEmpty(dateData)) {
            let result: any = {};
            dateData.forEach(dateStr => {
              const date = moment(dateStr).format('YYYY-MM-DD');
              result[date] = {
                disabled: false,
                disableTouchEvent: false,
              };
            });
            if (!dateData.includes(today)) {
              const mergeDate = {
                [moment().format('YYYY-MM-DD')]: { disabled: true, marked: true },
              };
              result = { ...result, ...mergeDate };
            }
            setMarkedDates(result);
          }
        }
        return res.data;
      }),
  });

  useEffect(() => {
    if (selected) {
      refetch();
      setActiveSections([0]);
    }
  }, [selected]);

  const renderHeader = (section: any, _: any, isActive: any) => {
    return (
      <View style={styles.accordHeader}>
        {section && section?.eventName ? (
          <RenderDataHtml html={decode(section?.eventName)} />
        ) : (
          ''
        )}
        {isActive ? (
          <Image source={imgs.IMG_ARROW_UP} />
        ) : (
          <Image source={imgs.IMG_ARROW_DOWN} />
        )}
      </View>
    );
  };

  const renderContent = (section: any, _: any, isActive: any) => {
    return (
      <View style={styles.accordContent}>
        {section && section?.eventDetails ? (
          <RenderDataHtml html={decode(section?.eventDetails)} />
        ) : (
          <Text style={styles.title}>Đang cập nhật...</Text>
        )}
      </View>
    );
  };

  const checkDisableLeft: any = () => {
    if (isEmpty(data)) { return true }
    if (!isEmpty(selected)) {
      const dataDate = moment(selected).format('YYYY-MM-DD');
      const yesterday = moment(dataDate, 'YYYY-MM-DD')
        .subtract(1, 'day')
        .format('YYYY-MM-DD[T]00:00:00');

      if (!isEmpty(first(dates))) {
        const firstDate = moment(first(dates));
        return firstDate.isAfter(yesterday);
      } else {
        return false;
      }
    } else {
      return true;
    }
  };

  const checkDisableRight: any = () => {
    if (isEmpty(data)) { return true }
    if (!isEmpty(selected)) {
      const dataDate = moment(selected).format('YYYY-MM-DD');
      const tomorrow = moment(dataDate, 'YYYY-MM-DD')
        .add('1', 'day')
        .format('YYYY-MM-DD[T]00:00:00');

      if (!isEmpty(last(dates))) {
        const lastDate = moment(last(dates));
        return moment(tomorrow).isAfter(lastDate);
      } else {
        return false;
      }
    } else {
      return true;
    }
  };

  return (
    <SafeAreaView style={styles.bgColor}>
      <View style={{ flex: 1 }}>
        <HeaderSeaGame title="LỊCH THI ĐẤU ĐOÀN THỂ THAO VIỆT NAM" />
        {isLoading
          ? <ActivityIndicator style={{ paddingTop: 10 }} />
          : <>
            <View style={styles.headerDate}>
              <Pressable
                style={checkDisableLeft() ? { opacity: 0.7 } : undefined}
                onPress={() => {
                  if (!checkDisableLeft()) {
                    const dataDate = moment(selected).format('YYYY-MM-DD');
                    const yesterday = moment(dataDate, 'YYYY-MM-DD')
                      .subtract(1, 'day')
                      .format('YYYY-MM-DD[T]00:00:00');
                    setSelected(yesterday);
                  }
                }}>
                <View style={styles.left}>
                  <SvgLeft />
                  <Text style={styles.titleDate}>Trước</Text>
                </View>
              </Pressable>

              <TouchableOpacity
                onPress={() => {
                  setModalVisible(true);
                }}>
                <View>
                  <Text style={styles.titleDate}>
                    {selected
                      ? `Ngày ${moment(selected).format('DD/MM/YYYY')}`
                      : 'Chọn ngày'}
                  </Text>
                </View>
              </TouchableOpacity>

              <Pressable
                style={checkDisableRight() ? { opacity: 0.7 } : undefined}
                onPress={() => {
                  if (!checkDisableRight()) {
                    const dataDate = moment(selected).format('YYYY-MM-DD');
                    const tomorrow = moment(dataDate, 'YYYY-MM-DD')
                      .add(1, 'day')
                      .format('YYYY-MM-DD[T]00:00:00');
                    setSelected(tomorrow);
                  }
                }}>
                <View style={styles.right}>
                  <Text style={styles.titleDate}>Sau</Text>
                  <SvgRightDate />
                </View>
              </Pressable>
            </View>

            <ScrollView
              contentContainerStyle={{
                paddingBottom: 40,
              }}>
              {!isEmpty(data) ? (
                isFetching ? (
                  <ActivityIndicator style={{ paddingTop: 10 }} />
                ) : (
                  <>
                    <Accordion
                      align="bottom"
                      sections={data}
                      activeSections={activeSections}
                      renderHeader={renderHeader}
                      renderContent={renderContent}
                      onChange={(sections: number[]) => {
                        setActiveSections(sections);
                      }}
                      sectionContainerStyle={styles.accordContainer}
                      expandMultiple={true}
                      underlayColor="rgba(217, 217, 217, 0.2)"
                    />
                    <Text style={styles.note}>
                      * <Text style={styles.textNote}>Chú ý</Text>: Lịch thi đấu
                      có thể thay đổi theo quyết định của Ban tổ chức.
                    </Text>
                  </>
                )
              ) : (
                <Text style={styles.titleUpdate}>Lịch thi đấu đang cập nhật…</Text>
              )}
            </ScrollView>
          </>}

      </View>
      <Modal
        isOpen={modalVisible}
        onClose={() => setModalVisible(false)}
        animationPreset="fade">
        <Modal.Content style={{ width }}>
          <Modal.CloseButton />
          <Modal.Header>Chọn ngày</Modal.Header>

          <Calendar
            onDayPress={day => {
              const dateDate = moment(day.dateString, 'YYYY-MM-DD').format(
                'YYYY-MM-DDTHH:mm:ss',
              );
              setSelected(dateDate);
              setModalVisible(false);
            }}
            markedDates={{
              ...markedDates,
            }}
            disabledByDefault={true}
            disableAllTouchEventsForDisabledDays={true}
            theme={{
              textDayFontFamily: 'Quicksand-SemiBold',
              textMonthFontFamily: 'Quicksand-SemiBold',
              textDayHeaderFontFamily: 'Quicksand-SemiBold',
              textDayFontWeight: '600',
              textMonthFontWeight: '600',
              textDayHeaderFontWeight: '600',
            }}
          />
        </Modal.Content>
      </Modal>
    </SafeAreaView>
  );
};

export default ScheduleSeaGameScreen;

const styles = StyleSheet.create({
  bgColor: {
    backgroundColor: COLORS.bgColor,
    flex: 1,
  },
  accordContainer: {
    paddingBottom: 5,
  },
  accordHeader: {
    backgroundColor: 'rgba(217, 217, 217, 0.2)',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  accordContent: {
    padding: 10,
  },
  title: {
    fontFamily: 'Quicksand-SemiBold',
    fontStyle: 'normal',
    fontWeight: '600',
    color: COLORS.white,
    fontSize: 15,
  },
  titleUpdate: {
    fontFamily: 'Quicksand-SemiBold',
    fontStyle: 'normal',
    fontWeight: '600',
    color: COLORS.white,
    fontSize: 15,
    padding: 10,
  },
  titleDate: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    color: COLORS.white,
    fontSize: 15,
  },
  headerDate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    paddingBottom: 20,
    alignContent: 'center',
    alignItems: 'center',
  },
  left: {
    flexDirection: 'row',
    gap: 20,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderStyle: 'solid',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
    padding: 5,
    width: 100,
  },
  right: {
    flexDirection: 'row',
    gap: 20,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderStyle: 'solid',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
    width: 100,
    padding: 5,
    justifyContent: 'flex-end',
  },
  overlay: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    backgroundColor: 'rgba(100, 100, 100, 0.6)',
  },
  note: {
    padding: 10,
    fontStyle: 'italic',
    fontWeight: '500',
    fontSize: 15,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    paddingTop: 14,
    color: '#FFF200',
  },
  textNote: {
    fontWeight: '700',
  },
});
