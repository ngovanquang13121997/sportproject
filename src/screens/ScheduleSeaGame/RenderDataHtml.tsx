import React from 'react';
import { StyleSheet, useWindowDimensions } from 'react-native';
import RenderHTML from 'react-native-render-html';
import { COLORS } from '../../constants';

const RenderDataHtml = React.memo(function WebDisplay({ html }: any) {
    const { width: contentWidth } = useWindowDimensions();

    return (
        <RenderHTML
            contentWidth={contentWidth}
            source={{ html }}
            baseStyle={{
                "fontFamily": "Quicksand-SemiBold",
                "fontStyle": "normal",
                "fontWeight": "600",
                color: COLORS.white,
                lineHeight: 20
            }}
        />
    );
});

export default RenderDataHtml

const styles = StyleSheet.create({})