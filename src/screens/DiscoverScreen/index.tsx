import { NavigationProp } from '@react-navigation/native';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { COLORS } from '../../constants';

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const DiscoverScreen = ({ navigation }: RouterProps) => {
    return (
        <SafeAreaView style={styles.bgColor}>
            <View style={styles.listView}>

            </View>
        </SafeAreaView>
    )
}

export default DiscoverScreen

const styles = StyleSheet.create({
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1
    },
    listView: {
        flex: 1,
    },
})