import { get, isEmpty } from 'lodash'
import React, { useState } from 'react'
import { KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'
import SvgLogo from '../../../assets/svg/SvgLogo'
// @ts-ignore
import { NavigationProp } from '@react-navigation/native'
import axios from 'axios'
import { Button } from 'native-base'
import { Toast } from 'react-native-toast-message/lib/src/Toast'
import { COLORS, ROUTES } from '../../../constants'
import CustomHeaderLeft from '../../../views/components/CustomHeader'
// @ts-ignore
import CryptoJS from 'react-native-crypto-js'
import { useQuery } from 'react-query'
import { getTokenUser } from '../../../network/services'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { URL_AUTH_ASC, URL_AUTH_COS } from '../../../network/UrlNetWork'

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const isValidPhoneNumber = (phoneNumber: string) => {
    const pattern = /^(\+?(84|0))?\d{9}$/;
    return pattern.test(phoneNumber);
};

const ForgotPassword: React.FC<RouterProps> = (props) => {
    const { navigation } = props
    const { data, isLoading, refetch } = useQuery('tokenUser', getTokenUser)

    const [phone, setPhone] = useState<string | undefined>(undefined)

    const [loadingRegister, setLoadingRegister] = useState<boolean>(false)

    const onSubmitForm = async () => {
        if (isEmpty(phone)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập số điện thoại',
                topOffset: 60
            })
        }

        if (!isValidPhoneNumber(phone!)) {
            return Toast.show({
                type: 'error',
                text1: 'Số điện thoại không đúng định dạng',
                topOffset: 60
            })
        }

        const cleanedPhoneNumber = phone!.replace(/\D/g, "")
        const formattedPhoneNumber = "84" + cleanedPhoneNumber.slice(-9)

        const param_searchphone = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(`{"ApiData":{"PHONE__EXACT":"` +
            formattedPhoneNumber + `"}}`));
        setLoadingRegister(true)

        try {
            const res = await axios.get(`${URL_AUTH_COS}/api/CosPeople/GetByBasicInfoFilter?param=${param_searchphone}`, {
                headers: {
                    'content-type': 'application/json',
                    'TokenCode': data,
                }
            })
            if (res && res.data && res.data.Success) {
                const PeopleCode = get(res, 'data.Data.PeopleCode')

                try {
                    setTimeout(() => {
                        setLoadingRegister(false)
                    }, 500)
                    const resOtp = await axios.post(`${URL_AUTH_ASC}/api/AcsOtp/Required`,
                        {
                            "ApiData": {
                                "LoginName": PeopleCode,
                                "Mobile": formattedPhoneNumber,
                                "ApplicationCode": "TTV"
                            }
                        },
                        {
                            headers: {
                                'content-type': 'application/json',
                            }
                        })
                    if (resOtp && resOtp.data?.Data) {
                        navigation.navigate(ROUTES.FormForgotPassword, {
                            "LoginName": PeopleCode,
                            "Mobile": formattedPhoneNumber,
                        })
                    } else {
                        setTimeout(() => {
                            setLoadingRegister(false)
                        }, 500)
                        return Toast.show({
                            type: 'error',
                            text1: get(resOtp, 'data.Param.Messages[0]') || "Thất bại",
                            topOffset: 60
                        })
                    }
                } catch (error) {
                    setTimeout(() => {
                        setLoadingRegister(false)
                    }, 500)
                }
            } else {
                setTimeout(() => {
                    setLoadingRegister(false)
                }, 500)
                return Toast.show({
                    type: 'error',
                    text1: 'Số điện thoại của bạn chưa đăng ký tài khoản.',
                    topOffset: 60
                })
            }
        } catch (error) {
            setTimeout(() => {
                setLoadingRegister(false)
            }, 500)
        }
    }

    const onChangePhone = (text: string) => setPhone(text)

    return (
        <SafeAreaView style={styles.ForgotPassword}>
            <View style={styles.logo}>
                <View style={styles.svgLogo}><SvgLogo /></View>
            </View>

            <View style={styles.formLogin}>
                <View style={{
                    flexDirection: "row",
                    borderBottomWidth: 5,
                    borderBottomColor: COLORS.white,
                    padding: 15,
                    "backgroundColor": "#EFEDED",
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                    position: "relative",
                    top: -10
                }}>
                    <View style={{ flex: 1 }}>
                        <CustomHeaderLeft color='#034685' />
                    </View>
                    <View style={{ flex: 2.2 }}>
                        <Text style={styles.title}>Quên mật khẩu</Text>
                    </View>
                </View>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    style={styles.container}
                >
                    <ScrollView
                        style={{
                            flex: 1,
                            padding: 15
                        }}
                        keyboardShouldPersistTaps="handled"
                    >
                        <>
                            <View>
                                <Text style={styles.lableForm}>Số điện thoại</Text>
                            </View>
                            <View>
                                <TextInput
                                    style={styles.input}
                                    placeholderTextColor="#443B3B"
                                    placeholder="Nhập số điện thoại"
                                    keyboardType={"numeric"}
                                    textContentType="password"
                                    onChangeText={(onChangePhone)}
                                    value={phone}
                                    maxLength={10}
                                />
                            </View>
                        </>

                        <Button onPress={onSubmitForm} isLoadingText='Tiếp tục'
                            isLoading={loadingRegister} style={styles.borderBotton}>
                            <Text style={styles.textButton}>Tiếp tục</Text>
                        </Button>

                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        </SafeAreaView>
    )
}

export default ForgotPassword

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    ForgotPassword: {
        flex: 1,
    },
    logo: {
        flex: 1.3,
        backgroundColor: "#034685",
        justifyContent: "center",
    },
    svgLogo: {
        alignSelf: "center",
        alignItems: "center",

    },
    formLogin: {
        flex: 3,
    },
    contact: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#0D0D0D"
    },
    footerContact: {
        bottom: 40,
        left: 0,
        right: 0,
        alignItems: "center",
        position: "absolute"
    },
    lableForm: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#034685",
        marginBottom: 5
    },
    borderBotton: {
        "backgroundColor": "#034685",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignSelf: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 9,
        paddingBottom: 9,
        marginTop: 20,
        marginBottom: 20
    },
    textButton: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#FFFFFF",
    },
    inputView: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignContent: "center",
        justifyContent: "center",
        marginBottom: 10
    },
    input: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        height: 45,
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        padding: 10,
        marginBottom: 10
    },
    title: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "color": "#000000",
        fontSize: 15
    }
})