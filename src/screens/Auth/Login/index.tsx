import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationProp, useFocusEffect, useIsFocused } from '@react-navigation/native'
import axios from 'axios'
import { get, isEmpty } from 'lodash'
import { Button, Input, Modal, Pressable } from 'native-base'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Alert, BackHandler, Keyboard, KeyboardAvoidingView, Linking, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Toast from 'react-native-toast-message'
import SvgLogo from '../../../assets/svg/SvgLogo'
// @ts-ignore
import CryptoJS from 'react-native-crypto-js'
import { useQuery } from 'react-query'
import SvgEye from '../../../assets/svg/SvgEye'
import SvgEye1 from '../../../assets/svg/SvgEye1'
import { COLORS, ROUTES } from '../../../constants'
import { useDataContextNavigation } from '../../../navigations/ContextNavigation'
import { getTokenUser } from '../../../network/services'
import { URL_AUTH_ASC, URL_AUTH_COS } from '../../../network/UrlNetWork'
import CustomHeaderLeft from '../../../views/components/CustomHeader'
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

export const isValidPhoneNumber = (phoneNumber: string) => {
    const pattern = /^(\+?(84|0))?\d{9}$/;
    return pattern.test(phoneNumber);
};

const isVietnamesePhoneNumber = (phoneNumber: string) => {
    const regex = /^(0[0-9]{2}|84[0-9]{2})[0-9]{7}$/;
    return regex.test(phoneNumber);
}

const LoginScreen: React.FC<RouterProps> = (props) => {
    const { navigation } = props

    const { updateContextData } = useDataContextNavigation()

    const [peopleCode, setPeopleCode] = useState<string>()
    const [showModal, setShowModal] = useState<boolean>(false)
    const [showPassword, setShowPassword] = useState<boolean>(false)

    const [password, setPassword] = useState<string>()

    const [phone, setPhone] = useState<string | undefined>(undefined)
    const isFocused = useIsFocused();

    const { data, isLoading, refetch } = useQuery('tokenUser', getTokenUser)

    const [loadingLogin, setLoadingLogin] = useState<boolean>(false)

    const [keyboardStatus, setKeyboardStatus] = useState(false);

    const [show, setShow] = React.useState(false);

    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                onGoBackData()
                return true;
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => {
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
            };
        }, [showPassword]),
    );

    const onGoBackData = () => {
        if (showPassword) {
            setShowPassword(false)
            setPhone(undefined)
        } else {
            navigation.navigate(ROUTES.HOME)
        }
    }

    useEffect(() => {
        const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    useEffect(() => {
        if (isFocused) {
            refetch()
        }
    }, [isFocused]);

    const onSubmitForm = async () => {
        if (isEmpty(phone)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập số điện thoại',
                topOffset: 60
            })
        }

        if (isVietnamesePhoneNumber(phone!)) {
            const cleanedPhoneNumber = phone!.replace(/\D/g, "")
            const formattedPhoneNumber = "84" + cleanedPhoneNumber.slice(-9)

            const param_searchphone = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(`{"ApiData":{"PHONE__EXACT":"` +
                formattedPhoneNumber + `"}}`));
            setLoadingLogin(true)
            try {
                const res = await axios.get(`${URL_AUTH_COS}/api/CosPeople/GetByBasicInfoFilter?param=${param_searchphone}`, {
                    headers: {
                        'content-type': 'application/json',
                        'TokenCode': data,
                    }
                })

                if (res && res.data && res.data.Success) {
                    setShowPassword(true)
                    setPeopleCode(get(res, 'data.Data.PeopleCode'))
                    await AsyncStorage.setItem('@userInfoDataDetail', JSON.stringify(get(res, 'data.Data')));
                } else {
                    setShowModal(true)
                }
                setTimeout(() => {
                    setLoadingLogin(false)
                }, 500)

            } catch (error: any) {
                setTimeout(() => {
                    setLoadingLogin(false)
                }, 500)
            }

        } else {
            return Toast.show({
                type: 'error',
                text1: 'Số điện thoại không đúng định dạng',
                topOffset: 60
            })
        }
    }

    const onChangePhone = (text: string) => setPhone(text)
    const onChangePassword = (text: string) => setPassword(text)

    const onSubmitFormLogin = async () => {
        if (isEmpty(password)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập mật khẩu',
                topOffset: 60
            })
        }

        const plainText = `TTV:${peopleCode}:${password}`
        const authbasic_user = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(plainText));
        setLoadingLogin(true)
        try {
            const res = await axios.get(`${URL_AUTH_ASC}/api/Token/Login`, {
                headers: {
                    'content-type': 'application/json',
                    'Authorization': `Basic ${authbasic_user}`
                }
            })
            if (res && res.data && res.data.Success) {
                await AsyncStorage.setItem('@userInfoData', JSON.stringify(get(res, 'data.Data')));

                await updateContextData(true)
                navigation.navigate(ROUTES.HOME)
                setTimeout(() => {
                    setLoadingLogin(false)
                }, 500)
                return Toast.show({
                    type: 'success',
                    text1: "Đăng nhập thành công",
                    topOffset: 60
                })
            } else {
                setTimeout(() => {
                    setLoadingLogin(false)
                }, 500)
                return Toast.show({
                    type: 'error',
                    text1: get(res, 'data.Param.Messages[0]') || "Tên đăng nhập hoặc mật khẩu không chính xác",
                    topOffset: 60
                })
            }
        } catch (error) {
            setLoadingLogin(false)
        }

    }

    const onClickPhone = (phone: string) => {
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }

        Linking.openURL(phoneNumber);
    }

    return (
        <SafeAreaView style={styles.loginScreen}>
            <View style={styles.logo}>
                <View style={styles.svgLogo}><SvgLogo /></View>
            </View>

            <View style={styles.formLogin}>
                <View style={{
                    flexDirection: "row",
                    borderBottomWidth: 5,
                    borderBottomColor: COLORS.white,
                    padding: 15,
                    "backgroundColor": "#EFEDED",
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                    position: "relative",
                    top: -10
                }}>
                    <View style={{ flex: 1 }}>
                        <CustomHeaderLeft color='#034685' onGoBackData={onGoBackData} />
                    </View>
                    <View style={{ flex: 2.2 }}>
                        <Text style={styles.titleHeader}>Đăng nhập tài khoản</Text>
                    </View>
                </View>

                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    style={styles.container}>
                    <ScrollView style={{
                        flex: 1,
                        padding: 15
                    }}
                        keyboardShouldPersistTaps="handled"
                    >
                        {isLoading
                            ? <ActivityIndicator />
                            : <View>
                                <>
                                    <View>
                                        <Text style={styles.lableForm}>Số điện thoại</Text>
                                    </View>
                                    <View>
                                        {showPassword
                                            ? <TextInput
                                                style={[styles.input, { backgroundColor: "#E7E7E7" }]}
                                                placeholderTextColor="#443B3B"
                                                placeholder="Nhập số điện thoại"
                                                keyboardType={"numeric"}
                                                onChangeText={(onChangePhone)}
                                                value={phone}
                                                editable={false}
                                                maxLength={10}
                                            />
                                            : <TextInput
                                                style={styles.input}
                                                placeholderTextColor="#443B3B"
                                                placeholder="Nhập số điện thoại"
                                                keyboardType={"numeric"}
                                                onChangeText={(onChangePhone)}
                                                value={phone}
                                                editable={true}
                                                maxLength={10}
                                            />}

                                    </View>
                                </>
                                {showPassword ?
                                    <>
                                        <View>
                                            <Text style={styles.lableForm}>Mật khẩu</Text>
                                        </View>
                                        <View style={styles.inputView}>
                                            <Input
                                                type={show ? "text" : "password"}
                                                InputRightElement={<Pressable onPress={() => setShow(!show)}>
                                                    {show ? <SvgEye1 /> : <SvgEye />}
                                                </Pressable>}
                                                style={styles.inputPassword}
                                                placeholderTextColor="#443B3B"
                                                placeholder="Nhập mật khẩu"
                                                onChangeText={(onChangePassword)}
                                                value={password}
                                                variant="unstyled"
                                            />
                                        </View>

                                        <View style={{ flex: 1, justifyContent: "space-between", flexDirection: "row" }}>
                                            <View>
                                                <TouchableOpacity onPress={() => {
                                                    navigation.navigate(ROUTES.ForgotPassword)
                                                }}>
                                                    <Text style={[styles.tileFooter, { "textDecorationLine": "underline" }]}>Quên mật khẩu?</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setShowPassword(false)
                                                        setPassword(undefined)
                                                    }}>
                                                    <Text style={styles.tileFooter}>Đăng nhập tài khoản khác</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <Button onPress={onSubmitFormLogin}
                                            isLoadingText='Đăng nhập'
                                            isLoading={loadingLogin} style={styles.borderBotton}>
                                            <Text style={styles.textButton}>Đăng nhập</Text>
                                        </Button>
                                    </>
                                    : <Button onPress={onSubmitForm} isLoadingText='Đăng nhập' isLoading={loadingLogin} style={styles.borderBotton}>
                                        <Text style={styles.textButton}>Đăng nhập</Text>
                                    </Button>
                                }
                            </View>
                        }
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>

            {!keyboardStatus
                ? <View style={styles.footerContact}>
                    <TouchableOpacity onPress={() => onClickPhone("19006888")}>
                        <Text style={styles.contact}>Hỗ trợ
                            <Text style={{
                                "textDecorationLine": "underline",
                                "color": "#034685"
                            }} > 1900 6888</Text>
                        </Text>
                    </TouchableOpacity>
                </View>
                : ""}

            <Modal
                isOpen={showModal}
                onClose={() => {
                    setShowModal(false)
                }}
            >
                <Modal.Content>
                    <Modal.Body>
                        <View>
                            <Text style={styles.title}>Thông báo</Text>
                            <Text style={styles.content}>
                                Số điện thoại của bạn chưa đăng ký tài khoản. Vui lòng đăng ký tài khoản.
                            </Text>
                        </View>
                        <View style={styles.buttonModalNotification}>
                            <Button
                                onPress={() => {
                                    navigation.navigate(ROUTES.REGISTER, {
                                        phoneNumber: phone
                                    })
                                    setShowModal(false)
                                    setPhone(undefined)
                                }}
                                style={[styles.buttonModal, { backgroundColor: "#034685" }]}>
                                <Text style={styles.textModal}>Đăng ký</Text>
                            </Button>
                        </View>
                    </Modal.Body>
                </Modal.Content>
            </Modal>
        </SafeAreaView>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loginScreen: {
        flex: 1
    },
    logo: {
        flex: 1.3,
        backgroundColor: "#034685",
        justifyContent: "center",
    },
    svgLogo: {
        alignSelf: "center",
        alignItems: "center",

    },
    formLogin: {
        flex: 3,
    },
    titleHeader: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "color": "#000000",
        fontSize: 15
    },
    contact: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#0D0D0D"
    },
    footerContact: {
        alignItems: "center",
        position: "absolute",
        bottom: 40,
        left: 0,
        right: 0,
    },
    lableForm: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#034685",
        marginBottom: 5
    },
    borderBotton: {
        "backgroundColor": "#034685",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignSelf: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 9,
        paddingBottom: 9,
        marginTop: 20
    },
    textButton: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#FFFFFF",
        alignSelf: "center"
    },
    inputView: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignContent: "center",
        justifyContent: "center",
        marginBottom: 10
    },
    inputPassword: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        height: 42,
    },
    input: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        height: 45,
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        padding: 10,
        marginBottom: 10
    },
    title: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#2D2F34"
    },
    content: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#000000",
        paddingTop: 14,
    },
    buttonModalNotification: {
        flex: 1,
        flexDirection: "row",
        gap: 10,
        justifyContent: "center",
        marginTop: 24
    },
    buttonModal: {
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        width: 136,
    },
    textModal: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#FFFFFF"
    },
    tileFooter: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#034685"
    }
})