import { get, isEmpty } from 'lodash'
import React, { useEffect, useState } from 'react'
import { Keyboard, KeyboardAvoidingView, Linking, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import SvgLogo from '../../../assets/svg/SvgLogo'
// @ts-ignore
import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationProp, useRoute } from '@react-navigation/native'
import axios from 'axios'
import { Button, Input, Modal, NativeBaseProvider, Pressable } from 'native-base'
import { Toast } from 'react-native-toast-message/lib/src/Toast'
import { COLORS, ROUTES } from '../../../constants'
import CustomHeaderLeft from '../../../views/components/CustomHeader'
import OTPScreen from './OTPScreen'
import SvgEye from '../../../assets/svg/SvgEye'
import SvgEye1 from '../../../assets/svg/SvgEye1'
import { URL_AUTH_ASC, URL_AUTH_COS } from '../../../network/UrlNetWork'

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const isValidPhoneNumber = (phoneNumber: string) => {
    const pattern = /^(\+?(84|0))?\d{9}$/;
    return pattern.test(phoneNumber);
};

const RegisterScreen: React.FC<RouterProps> = (props) => {
    const { navigation } = props
    const [timeRemaining, setTimeRemaining] = useState(120);

    const [phone, setPhone] = useState<string | undefined>(undefined)
    const [password, setPassword] = useState<string | undefined>(undefined)
    const [confirmPassword, setConfirmPassword] = useState<string | undefined>(undefined)

    const [otp, setOtp] = useState<string>('')

    const [showModal, setShowModal] = useState<boolean>(false)

    const [loadingRegister, setLoadingRegister] = useState<boolean>(false)
    const [loadingOTP, setLoadingOTP] = useState<boolean>(false)

    const [keyboardStatus, setKeyboardStatus] = useState(false);

    const [show, setShow] = React.useState(false);
    const [showConfirm, setShowConfirm] = React.useState(false);

    const route = useRoute<any>();
    const { phoneNumber } = route.params

    useEffect(() => {
        if (phoneNumber) {
            setPhone(phoneNumber)
        }
    }, [phoneNumber])

    useEffect(() => {
        const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    useEffect(() => {
        let timer: any | null = null;
        if (timeRemaining > 0) {
            timer = setTimeout(() => {
                setTimeRemaining(prev => prev - 1);
            }, 1000);
        }
        return () => {
            if (timer) {
                clearTimeout(timer);
            }
        };
    }, [timeRemaining]);

    const handleResendOTP = async () => {
        const cleanedPhoneNumber = phone!.replace(/\D/g, "")
        const formattedPhoneNumber = "84" + cleanedPhoneNumber.slice(-9)

        await onSendOtp(formattedPhoneNumber)
    };

    const onSubmitForm = async () => {
        if (isEmpty(phone)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập số điện thoại',
                topOffset: 60
            })
        }

        if (!isValidPhoneNumber(phone!)) {
            return Toast.show({
                type: 'error',
                text1: 'Số điện thoại không đúng định dạng',
                topOffset: 60
            })
        }

        if (isEmpty(password)) {
            return Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập mật khẩu',
                topOffset: 60
            })
        }

        if (password && password.length < 6) {
            return Toast.show({
                type: 'error',
                text1: 'Mật khẩu phải có ít nhất 6 kí tự.',
                topOffset: 60,

            })
        }

        if (password !== confirmPassword) {
            return Toast.show({
                type: 'error',
                text1: 'Mật khẩu không khớp nhau',
                topOffset: 60
            })
        }

        const cleanedPhoneNumber = phone!.replace(/\D/g, "")
        const formattedPhoneNumber = "84" + cleanedPhoneNumber.slice(-9)

        setLoadingRegister(true)
        try {
            const res = await axios.post(`${URL_AUTH_COS}/api/CosPeople/CheckExists`,
                { "ApiData": { "CmndNumber": "", "PhoneNumber": formattedPhoneNumber } },
                {
                    headers: {
                        "Content-Type": "application/json"
                    }
                })

            if (res && res.data && res.data.Success) {
                // Gửi mã OTP
                setTimeout(() => {
                    setLoadingRegister(false)
                }, 500)

                await onSendOtp(formattedPhoneNumber)
            } else {
                setTimeout(() => {
                    setLoadingRegister(false)
                }, 500)
                return Toast.show({
                    type: 'error',
                    text1: get(res, 'data.Param.Messages[0]'),
                    topOffset: 60,
                })
            }
        } catch (error) {
            setTimeout(() => {
                setLoadingRegister(false)
            }, 500)
        }
    }

    const onSendOtp = async (phoneNumber: string) => {
        try {
            await axios.post(`${URL_AUTH_ASC}/api/AcsOtp/RequiredOnly`, {
                "ApiData": {
                    "MOBILE": phoneNumber,
                    "APPLICATIONCODE": "TTV"
                }
            }, {
                headers: {
                    "Content-Type": "application/json"
                }
            })
            setShowModal(true)
            setTimeRemaining(120)
            setOtp('')

        } catch (error) {

        }
    }

    const onChangePhone = (text: string) => setPhone(text)

    const onChangePassword = (text: string) => setPassword(text)

    const onChangeConfirmPassword = (text: string) => setConfirmPassword(text)

    const onConfirmOTP = async () => {
        if (otp.length === 4) {
            const cleanedPhoneNumber = phone!.replace(/\D/g, "")
            const formattedPhoneNumber = "84" + cleanedPhoneNumber.slice(-9)

            const TokenCode: string | null = await AsyncStorage.getItem('@TokenCode');

            if (TokenCode) {
                setLoadingOTP(true)
                try {
                    const res = await axios.post(`${URL_AUTH_COS}/api/CosPeople/VstRegisterUser`, {
                        "ApiData": {
                            "MobileNumber": formattedPhoneNumber,
                            "Password": password,
                            "Otp": otp,
                            "ReferralCode": ""
                        }
                    }, {
                        headers: {
                            ApplicationCode: "TTV",
                            TokenCode: JSON.parse(TokenCode)
                        }
                    })

                    if (res && res.data && res.data.Success) {
                        setTimeout(() => {
                            setLoadingOTP(false)
                        }, 500)
                        navigation.navigate(ROUTES.REGISTER_SUCCESS)
                    } else {
                        setTimeout(() => {
                            setLoadingOTP(false)
                        }, 500)
                        return Toast.show({
                            type: 'error',
                            text1: "Đăng ký tài khoản thất bại",
                            topOffset: 60,
                        })
                    }
                } catch (error) {
                    setTimeout(() => {
                        setLoadingOTP(false)
                    }, 500)
                }
            }
        } else {
            return Toast.show({
                type: 'error',
                text1: "Vui lòng nhập mã OTP",
                topOffset: 60,
            })
        }
    }

    const onClickPhone = (phone: string) => {
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }

        Linking.openURL(phoneNumber);
    }

    return (
        <>
            <SafeAreaView style={styles.RegisterScreen}>
                <View style={styles.logo}>
                    <View style={styles.svgLogo}><SvgLogo /></View>
                </View>

                <View style={styles.formLogin}>
                    <View style={{
                        flexDirection: "row",
                        borderBottomWidth: 5,
                        borderBottomColor: COLORS.white,
                        padding: 15,
                        "backgroundColor": "#EFEDED",
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        position: "relative",
                        top: -10
                    }}>
                        <View style={{ flex: 1 }}>
                            <CustomHeaderLeft color='#034685' />
                        </View>
                        <View style={{ flex: 2.2 }}>
                            <Text style={styles.title}>Đăng ký tài khoản</Text>
                        </View>
                    </View>
                    <KeyboardAvoidingView
                        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                        style={styles.container}
                    >
                        <ScrollView
                            style={{
                                flex: 1,
                                padding: 15
                            }}
                            keyboardShouldPersistTaps="handled"
                        >
                            <>
                                <View>
                                    <Text style={styles.lableForm}>Số điện thoại</Text>
                                </View>
                                <View>
                                    <TextInput
                                        style={styles.input}
                                        placeholderTextColor="#443B3B"
                                        placeholder="Nhập số điện thoại"
                                        keyboardType={"numeric"}
                                        textContentType="password"
                                        onChangeText={(onChangePhone)}
                                        value={phone}
                                        editable={Boolean(!showModal)}
                                        maxLength={10}
                                    />
                                </View>
                            </>

                            <>
                                <View>
                                    <Text style={styles.lableForm}>Mật khẩu</Text>
                                </View>
                                <View style={styles.inputView}>
                                    <Input
                                        type={show ? "text" : "password"}
                                        InputRightElement={<Pressable onPress={() => setShow(!show)}>
                                            {show ? <SvgEye1 /> : <SvgEye />}
                                        </Pressable>}
                                        style={styles.inputPassword}
                                        placeholderTextColor="#443B3B"
                                        placeholder="Nhập mật khẩu"
                                        onChangeText={(onChangePassword)}
                                        value={password}
                                        variant="unstyled"
                                        editable={Boolean(!showModal)}
                                    />
                                </View>
                            </>

                            <>
                                <View>
                                    <Text style={styles.lableForm}>Nhập lại mật khẩu</Text>
                                </View>
                                <View style={styles.inputView}>
                                    <Input
                                        type={showConfirm ? "text" : "password"}
                                        InputRightElement={<Pressable onPress={() => setShowConfirm(!showConfirm)}>
                                            {showConfirm ? <SvgEye1 /> : <SvgEye />}
                                        </Pressable>}
                                        style={styles.inputPassword}
                                        placeholderTextColor="#443B3B"
                                        placeholder="Nhập lại mật khẩu"
                                        onChangeText={(onChangeConfirmPassword)}
                                        value={confirmPassword}
                                        variant="unstyled"
                                        editable={Boolean(!showModal)}
                                    />
                                </View>
                            </>

                            <Button onPress={onSubmitForm} isLoadingText='Đăng ký' isLoading={loadingRegister} style={styles.borderBotton}>
                                <Text style={styles.textButton}>Đăng ký</Text>
                            </Button>

                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>

                {!keyboardStatus
                    ? <View style={styles.footerContact}>
                        <TouchableOpacity onPress={() => onClickPhone("19006888")}>
                            <Text style={styles.contact}>Hỗ trợ
                                <Text style={{
                                    "textDecorationLine": "underline",
                                    "color": "#034685"
                                }} > 1900 6888</Text>
                            </Text>
                        </TouchableOpacity>
                    </View>
                    : ""}

            </SafeAreaView>

            <Modal
                isOpen={showModal}
                onClose={() => {
                    setShowModal(false)
                    setTimeRemaining(120)
                }}
            >
                <Modal.Content>
                    <Modal.Body>
                        <View>
                            <Text style={styles.titleModal}>Thông báo</Text>
                            <Text style={styles.content}>
                                Vui lòng kiểm tra tin nhắn và nhập mã OTP đã gửi về số điện thoại của bạn
                            </Text>
                        </View>

                        <View>
                            {showModal ? <OTPScreen otp={otp} setOtp={setOtp} /> : ""}
                        </View>
                        <View>
                            {!timeRemaining
                                ? <TouchableOpacity onPress={handleResendOTP}><Text style={[styles.note, { marginBottom: 10 }]}>Gửi lại</Text></TouchableOpacity>
                                : <Text style={[styles.note, { marginBottom: 10 }]}>Thời gian hiệu lực: {Math.floor(timeRemaining / 60)}:{timeRemaining % 60}</Text>
                            }
                        </View>

                        <View>
                            <Text style={styles.note} >Lưu ý: Nếu không nhận được mã OTP hãy liên hệ tổng đài
                                <Text
                                    onPress={() => onClickPhone("19006888")}
                                    style={{
                                        "textDecorationLine": "underline",
                                        "color": "#034685"
                                    }} > 1900 6888 </Text>
                                để được hỗ trợ</Text>
                        </View>
                        <View style={styles.buttonModalNotification}>
                            <Button
                                onPress={onConfirmOTP}
                                isLoading={loadingOTP}
                                isLoadingText="Xác nhận"
                                style={[styles.buttonModal, { backgroundColor: "#034685" }]}>
                                <Text style={styles.textModal}>Xác nhận</Text>
                            </Button>
                        </View>
                    </Modal.Body>
                </Modal.Content>
            </Modal>
        </>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    RegisterScreen: {
        flex: 1,
    },
    logo: {
        flex: 1.3,
        backgroundColor: "#034685",
        justifyContent: "center",
    },
    svgLogo: {
        alignSelf: "center",
        alignItems: "center",

    },
    formLogin: {
        flex: 3,
    },
    contact: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#0D0D0D"
    },
    footerContact: {
        bottom: 40,
        left: 0,
        right: 0,
        alignItems: "center",
        position: "absolute"
    },
    lableForm: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#034685",
        marginBottom: 5
    },
    borderBotton: {
        "backgroundColor": "#034685",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignSelf: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 9,
        paddingBottom: 9,
        marginTop: 20,
        marginBottom: 20
    },
    textButton: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#FFFFFF",
    },
    inputView: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        alignContent: "center",
        justifyContent: "center",
        marginBottom: 10
    },
    inputPassword: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        height: 42,
    },
    input: {
        "backgroundColor": "#FFFFFF",
        "borderWidth": 1.5,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        height: 45,
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,

        "color": "#443B3B",
        padding: 10,
        marginBottom: 10
    },
    title: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "color": "#000000",
        fontSize: 15
    },
    titleModal: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#2D2F34"
    },
    content: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#000000",
        paddingTop: 14,
    },
    buttonModalNotification: {
        flex: 1,
        flexDirection: "row",
        gap: 10,
        justifyContent: "center",
        marginTop: 24
    },
    buttonModal: {
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        width: 136,
    },
    textModal: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "textAlign": "center",
        "color": "#FFFFFF"
    },
    note: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "color": "#000000",
        textAlign: "center"
    }
})