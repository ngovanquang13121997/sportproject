import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';

interface Props {
    otp?: any,
    setOtp?: any
}

const OTPScreen: React.FC<Props> = (props) => {
    const otp1Ref = useRef<TextInput>(null);
    const otp2Ref = useRef<TextInput>(null);
    const otp3Ref = useRef<TextInput>(null);
    const otp4Ref = useRef<TextInput>(null);

    const { otp, setOtp } = props

    const handleOnChangeText = (value: string, index: number) => {
        setOtp((prev: any) => {
            const otpArr = [...prev];
            otpArr[index] = value;
            return otpArr.join('');
        });

        if (value.length === 1 && index < 3) {
            if (index === 0) {
                otp2Ref.current?.focus();
            } else if (index === 1) {
                otp3Ref.current?.focus();
            } else if (index === 2) {
                otp4Ref.current?.focus();
            }
        } else if (value.length === 0 && index > 0) {
            if (index === 1) {
                otp1Ref.current?.focus();
            } else if (index === 2) {
                otp2Ref.current?.focus();
            } else if (index === 3) {
                otp3Ref.current?.focus();
            }
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.otpContainer}>
                <TextInput
                    ref={otp1Ref}
                    style={styles.otpInput}
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={value => handleOnChangeText(value, 0)}
                    value={otp[0]}
                />
                <TextInput
                    ref={otp2Ref}
                    style={styles.otpInput}
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={value => handleOnChangeText(value, 1)}
                    value={otp[1]}
                />
                <TextInput
                    ref={otp3Ref}
                    style={styles.otpInput}
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={value => handleOnChangeText(value, 2)}
                    value={otp[2]}
                />
                <TextInput
                    ref={otp4Ref}
                    style={styles.otpInput}
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={value => handleOnChangeText(value, 3)}
                    value={otp[3]}
                />
            </View>
        </View>
    );
};

export default OTPScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpInput: {
        borderRadius: 4,
        paddingHorizontal: 16,
        paddingVertical: 8,
        marginHorizontal: 8,
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        width: 51,
        "borderWidth": 2,
        "borderColor": "#034685",
        "borderStyle": "solid",
        "borderTopLeftRadius": 14,
        "borderTopRightRadius": 14,
        "borderBottomRightRadius": 14,
        "borderBottomLeftRadius": 14,
        marginTop: 15,
        marginBottom: 15
    },
})

