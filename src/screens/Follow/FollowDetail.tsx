import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ViewSchedules from '../Schedules/component/ViewSchedules'
import { NavigationProp } from '@react-navigation/native';

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const FollowDetail: React.FC<RouterProps> = (props) => {
    return <ViewSchedules navigation={props.navigation} />
}

export default FollowDetail

const styles = StyleSheet.create({})