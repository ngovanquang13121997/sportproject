import { NavigationProp, useIsFocused } from '@react-navigation/native'
import { isEmpty } from 'lodash'
import moment from 'moment'
import React, { useEffect } from 'react'
import { ActivityIndicator, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useInfiniteQuery } from 'react-query'
import SvgComingSoon from '../../assets/svg/SvgComingSoon'
import { COLORS, ROUTES } from '../../constants'
import { newsApi } from '../../network/services'
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const FollowScreen = ({ navigation }: RouterProps) => {
    const isFocused = useIsFocused();

    const { data, isLoading, refetch } = useInfiniteQuery(
        'follow',
        ({ pageParam = 0 }) => newsApi.getDataListGames(pageParam, 500),
        {
            enabled: false,
            refetchOnWindowFocus: false
        }
    )

    useEffect(() => {
        if (isFocused) {
            refetch()
        }
    }, [isFocused]);

    return (
        <SafeAreaView style={styles.bgColor}>
            {isLoading
                ? <View style={{ paddingTop: 15 }}><ActivityIndicator /></View>
                : !isEmpty(data?.pages.map(page => page).flat()) ?
                    <FlatList
                        data={data?.pages.map(page => page).flat()}
                        numColumns={1}
                        contentContainerStyle={{
                            padding: 15
                        }}
                        renderItem={({ item }) => {
                            return (
                                <TouchableOpacity style={styles.itemCard} onPress={() => {
                                    navigation.navigate(ROUTES.FollowDetail, {
                                        gameId: item.gameId,
                                        gameName: item.gameName,
                                        iconSport: item?.iconSport
                                    })
                                }}>
                                    <View style={styles.viewIageSchedules}>
                                        {item?.iconSport
                                            ? <SvgUri
                                                width="60"
                                                height="60"
                                                fill={COLORS.white}
                                                uri={item.iconSport}
                                            />
                                            : <SvgComingSoon width={60} height={60} />
                                        }
                                    </View>
                                    <View style={styles.itemContent}>
                                        <Text numberOfLines={2} style={styles.titleContent}>{item.gameName}</Text>
                                        <Text numberOfLines={1} style={styles.titleTime}>
                                            Từ ngày {moment(item.startDate).format('DD/MM')} đến ngày  {moment(item.endDate).format('DD/MM/YYYY')}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                        keyExtractor={(item, key) => String(key + 1)}
                        onEndReachedThreshold={0.7}
                    />
                    : <Text style={styles.textEmpty}>Bạn đang không theo dõi lịch thi đấu nào</Text>

            }

        </SafeAreaView>
    )
}

export default FollowScreen

const styles = StyleSheet.create({
    textEmpty: {
        "fontFamily": "Quicksand",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: "center",
        paddingTop: 15
    },
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1,
    },
    itemCard: {
        flex: 1,
        flexDirection: "row",
        marginBottom: 20
    },
    viewIageSchedules: {
        flex: 1,
    },
    imageSchedules: {
        width: 58,
        height: 58
    },
    itemContent: {
        flex: 4
    },
    titleContent: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "color": "#FFE601",
        marginBottom: 10
    },
    titleTime: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "color": COLORS.white
    }
})