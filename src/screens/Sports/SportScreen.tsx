import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import { ActivityIndicator, Dimensions, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useQuery } from 'react-query'
import { COLORS, ROUTES } from '../../constants'
import imgs from '../../constants/imgs'
import { newsApi } from '../../network/services'
interface RouterProps {
    navigation: NavigationProp<any, any>;
}

const SportScreen = ({ navigation }: RouterProps) => {
    const { data, isLoading } = useQuery('sports', newsApi.getDataListSports)

    return (
        <SafeAreaView style={styles.bgColor}>
            <View style={styles.listView}>
                {isLoading
                    ? <ActivityIndicator />
                    : <FlatList
                        data={data}
                        numColumns={3}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        contentContainerStyle={{
                            padding: 5
                        }}
                        renderItem={({ item }) => {
                            return (
                                <TouchableOpacity style={[styles.itemCard]}
                                    onPress={() => {
                                        navigation.navigate(ROUTES.SPORTS_DETAIL, { sportId: item.sportId })
                                    }}>
                                    <View style={styles.viewImage} >
                                        {item.name === "Wushu" ?
                                            <Image source={imgs.IMG_SPORT} style={{ width: 90, height: 90 }} />
                                            : item?.iconPath.includes('.svg')
                                                ? <SvgUri
                                                    width="90"
                                                    height="90"
                                                    fill={COLORS.white}
                                                    uri={item.iconPath}
                                                />
                                                : <Image
                                                    source={{
                                                        uri: item.iconPath
                                                    }}
                                                    style={{ width: 90, height: 90, borderRadius: 45 }}
                                                />
                                        }
                                    </View>
                                    <View style={styles.containerContent}>
                                        <Text style={styles.titleContent}>{item.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                        keyExtractor={(item, index) => String(index + 1)}
                    />
                }
            </View>
        </SafeAreaView>
    )
}

export default SportScreen

const styles = StyleSheet.create({
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1
    },
    listView: {
        flex: 1,
    },
    itemCard: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5,
        marginVertical: 4,
        marginHorizontal: 4,
    },
    viewImage: {
        alignSelf: "center",
        marginBottom: 5,
        marginTop: 5,
        width: 90,
        height: 90,
    },
    containerContent: {
        flexDirection: 'row',
        flexGrow: 1,
    },
    titleContent: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": COLORS.white,
        textAlignVertical: 'center',
        textAlign: "center",
        flex: 1,
    },
})