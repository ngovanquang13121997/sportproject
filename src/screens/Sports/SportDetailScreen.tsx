import { useRoute } from '@react-navigation/native'
import { decode } from 'html-entities'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import { COLORS, IMGS } from '../../constants'
import ApiHelper from '../../network/ApiClient'
import HeaderDetail from '../../views/components/HeaderDetail'
import OpenURLButton from '../../views/container/OpenURLButton'
import RenderDataHtml from '../News/RenderDataHtml'
interface IDataDetail {
    iconPath: string,
    introduce?: any,
    name?: string
    rules?: string
    gameId?: string
}

const SportDetailScreen = () => {
    const [loading, setLoading] = useState<boolean>(false)
    const [dataDetail, setDataDetail] = useState<IDataDetail | undefined>(undefined)

    const route = useRoute<any>();
    const { sportId } = route.params

    useEffect(() => {
        if (sportId) {
            onGetDataDetail(sportId)
        }
    }, [sportId])

    const onGetDataDetail = async (sportId: number) => {
        setLoading(true)
        try {
            const res = await ApiHelper.fetch(`/sport/${sportId}`)
            if (res && res.data) {
                setDataDetail(res.data)
                setLoading(false)
            } else {
                setDataDetail(undefined)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
        }
    }

    return (
        <SafeAreaView style={styles.bgWhite}>
            <ScrollView style={{ flex: 1 }}>
                {loading
                    ? <ActivityIndicator style={{ marginTop: 10 }} />
                    : <>
                        {dataDetail?.iconPath.includes('.svg')
                            ? dataDetail?.name === "Wushu" ?
                                <Image source={IMGS.IMG_WUSHU} style={styles.imgLogo} />
                                : <HeaderDetail image={IMGS.SCHEDULES_DETAIL} title={dataDetail?.name!} pathSvg={dataDetail?.iconPath} />
                            : <HeaderDetail image={IMGS.SCHEDULES_DETAIL} title={dataDetail?.name!} urlImage={dataDetail?.iconPath} />}
                        <RenderDataHtml html={decode(dataDetail?.introduce)} />
                        <View style={styles.bgBox}>
                            <View style={styles.listBox}>
                                <Text style={styles.titleBox}>Luật Thi Đấu</Text>
                                <View style={{ marginBottom: 15 }}>
                                    <OpenURLButton url={dataDetail?.rules!}>
                                        <Text style={styles.linkBox}>{dataDetail?.rules}</Text>
                                    </OpenURLButton>
                                </View>
                            </View>
                        </View>
                    </>
                }
            </ScrollView>
        </SafeAreaView>
    )
}

export default SportDetailScreen

const styles = StyleSheet.create({
    bgWhite: {
        backgroundColor: COLORS.white,
        flex: 1
    },
    contentSport: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        "color": COLORS.black,
        paddingLeft: 26,
        paddingRight: 26,
        paddingTop: 15,
        paddingBottom: 10
    },
    bgBox: {
        backgroundColor: "rgba(217, 217, 217, 0.6)",
        padding: 26
    },
    listBox: {
        flex: 1
    },
    titleBox: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "alignItems": "center",
        "color": COLORS.black,
        paddingBottom: 15
    },
    linkBox: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#011AFF",
    },
    imgLogo: {
        width: 160,
        height: 160,
        resizeMode: "contain",
        alignSelf: "center",
        marginBottom: 10,
        marginTop: 5,
    }
})