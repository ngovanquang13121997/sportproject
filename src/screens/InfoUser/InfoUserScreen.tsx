import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationProp } from '@react-navigation/native'
import { get, isEmpty } from 'lodash'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Image, Linking, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import SvgAvatarDefault from '../../assets/svg/SvgAvatarDefault'
import SvgIconSetting from '../../assets/svg/SvgIconSetting'
import { COLORS, ROUTES } from '../../constants'
import { URL_IMAGE } from '../../network/UrlNetWork'
import TextComponent from '../../views/components/TextComponent'

interface RouterProps {
    navigation: NavigationProp<any, any>;
}

interface UserInfo {
    AvatarUrl?: string
    Phone?: string
    FullName?: string
    Cards?: {
        BankCardCode?: string
        BankCode?: string
        CardCode: string
        Id: number
        IsActive: number
        LinkCode?: string
    }[]
    GenderName?: string
    Dob?: any
    Address?: string
    ProvinceName?: string
    DistrictName?: string
    CommuneName?: string
}

const InfoUserScreen: React.FC<RouterProps> = (props) => {
    const { navigation } = props
    const [userInfo, setUserInfo] = useState<UserInfo | undefined>(undefined)
    const [loading, setLoading] = useState<boolean>(false)

    useEffect(() => {
        onGetUserInfo()
    }, [])

    const onGetUserInfo = async () => {
        setLoading(true)
        const userInfoData: any = await AsyncStorage.getItem('@userInfoDataDetail');
        const data = JSON.parse(userInfoData)
        if (data) {
            setLoading(false)
            setUserInfo(data)
        } else {
            setLoading(false)
        }
    }

    const onClickPhone = (phone?: string) => {
        if (phone) {
            let phoneNumber = phone;
            if (Platform.OS !== 'android') {
                phoneNumber = `telprompt:${phone}`;
            }
            else {
                phoneNumber = `tel:${phone}`;
            }

            Linking.openURL(phoneNumber);
        }
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: "#F2F2F4" }}>
                <ScrollView style={{ flex: 1 }}>
                    {loading
                        ? <ActivityIndicator style={{ paddingTop: 10 }} />
                        : !isEmpty(userInfo)
                            ? <>
                                <View style={styles.boxInfo}>
                                    <Text style={styles.titleInfo}>Ảnh đại diện</Text>
                                    {userInfo?.AvatarUrl
                                        ? <Image
                                            style={{ width: 55, height: 55 }}
                                            source={{
                                                uri: `${URL_IMAGE}/${userInfo?.AvatarUrl}`
                                            }} />
                                        : <SvgAvatarDefault />}

                                </View>

                                {!isEmpty(userInfo?.Cards)
                                    ? <View style={styles.boxInfo}>
                                        <Text style={styles.titleInfo}>Họ tên</Text>
                                        <Text style={styles.font16}>{userInfo?.FullName}</Text>
                                    </View>
                                    : ""
                                }

                                <TouchableOpacity onPress={() => onClickPhone(userInfo?.Phone)}>
                                    <View style={styles.boxInfo}>
                                        <Text style={styles.titleInfo}>Số điện thoại</Text>
                                        <Text style={styles.font16}>{userInfo?.Phone?.replace('84', '0')}</Text>
                                    </View>
                                </TouchableOpacity>

                                {!isEmpty(userInfo?.Cards)
                                    ? <>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Số thẻ</Text>
                                            <Text style={styles.font16}>{get(userInfo, 'Cards[0].CardCode')}</Text>
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Giới tính</Text>
                                            <Text style={styles.colorGray}>{userInfo?.GenderName}</Text>
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Ngày sinh</Text>
                                            <Text style={styles.colorGray}>{moment(userInfo?.Dob, "YYYYMMDDHHmmss").format("DD-MM-YYYY")}</Text>
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Thông tin địa chỉ</Text>
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Địa chỉ</Text>
                                            <Text style={styles.font16} numberOfLines={1}>{userInfo?.Address}</Text>
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Tỉnh/Thành Phố</Text>
                                            <TextComponent styleProps={styles.font16} title={userInfo?.ProvinceName} />
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Quận/Huyện</Text>
                                            <TextComponent styleProps={styles.font16} title={userInfo?.DistrictName} />
                                        </View>
                                        <View style={styles.boxInfo}>
                                            <Text style={styles.titleInfo}>Phường/Xã </Text>
                                            <TextComponent styleProps={styles.font16} title={userInfo?.CommuneName} />
                                        </View>
                                    </>
                                    : <View style={styles.boxInfo}>
                                        <Text style={styles.titleInfo}>Thẻ Du lịch - Thể thao</Text>
                                        <TouchableOpacity
                                            onPress={() => {
                                                navigation.navigate(ROUTES.COMING_SOON)
                                            }}
                                            style={{
                                                flexDirection: "row",
                                                alignSelf: "center", alignItems: "center", gap: 5
                                            }}>
                                            <Text style={styles.textLink}>Mở thẻ ngay</Text>
                                            <View ><SvgIconSetting /></View>
                                        </TouchableOpacity>
                                    </View>
                                }
                            </>
                            : <ActivityIndicator style={{ paddingTop: 10 }} />}
                </ScrollView>
            </View>

        </SafeAreaView>
    )
}

export default InfoUserScreen

const styles = StyleSheet.create({
    title: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#FFFFFF"
    },
    titleInfo: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "color": "#2D2F34"
    },
    textLink: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,

        "textDecorationLine": "underline",
        "color": "#034685"
    },
    boxInfo: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomWidth: 1,
        "borderColor": "#D3D3D3",
        "borderStyle": "solid",
        paddingBottom: 14,
        paddingTop: 19
    },
    font16: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "color": "#2D2F34"
    },
    colorGray: {
        fontWeight: "700",
        color: "#8C8D8E",
        fontSize: 15
    },
    titleUpdate: {
        fontFamily: 'Quicksand-SemiBold',
        fontStyle: 'normal',
        fontWeight: '600',
        color: COLORS.white,
        fontSize: 15,
        padding: 10,
    },
})
