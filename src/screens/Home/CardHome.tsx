import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import { Dimensions, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { COLORS } from '../../constants'
interface Props {
    title: String,
    data: {
        id: string,
        title: string,
        icon: React.ReactNode,
        navigation?: string
    }[],
    navigation: NavigationProp<any, any>,
    numColumns?: number
    type?: string
}

const spacing = 8;
const width = (Dimensions.get('window').width - 4 * 15) / 2;

const CardHome: React.FC<Props> = (props) => {
    const { title, data, navigation, numColumns, type } = props

    return (
        <View style={styles.cardHome}>
            <Text style={styles.titleCrad}>{title}</Text>
            <View style={[styles.listCard]}>
                <FlatList
                    data={data}
                    scrollEnabled={false}
                    numColumns={numColumns || 2}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={[styles.itemCard, type ? { padding: 5 } : { justifyContent: "center" }]} onPress={() => {
                                if (item.navigation) {
                                    navigation.navigate(item.navigation)
                                }
                            }}>
                                {item.icon}
                                <Text style={styles.titleContentCrad}>{item.title}</Text>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item, index) => String(index + 1)}
                />
            </View>
        </View>
    )
}

export default CardHome

const styles = StyleSheet.create({
    cardHome: {
        flex: 1,
        marginBottom: 28
    },
    listCard: {
        alignSelf: "center",
    },
    titleCrad: {
        "fontFamily": "Quicksand-Bold",
        fontStyle: "normal",
        fontWeight: "700",
        fontSize: 15,
        alignItems: "center",
        letterSpacing: 1,
        color: COLORS.white,
        paddingLeft: 14,
        marginBottom: 10,
        paddingTop: 5
    },
    itemCard: {
        borderWidth: 2,
        borderColor: COLORS.white,
        borderRadius: 14,
        width: width,
        margin: spacing,
        height: width + 15,
        alignItems: "center",
    },
    titleContentCrad: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 13,
        "textAlign": "center",
        "textTransform": "uppercase",
        "color": COLORS.white,
        position: "absolute",
        bottom: 10,
        paddingTop: 5
    },
})