import moment from 'moment';
import React from 'react';
import {
    Dimensions, StyleSheet,
    Text,
    View
} from 'react-native';
import { SvgUri } from 'react-native-svg';
import SvgComingSoon from '../../assets/svg/SvgComingSoon';
import { COLORS } from '../../constants';

const { width } = Dimensions.get('screen');

interface Props {
    item?: any
}

const SlideItem: React.FC<Props> = (props) => {
    const { item } = props

    return (
        <View style={styles.container}>
            <View style={styles.logoSlider}>
                {item?.iconSport
                    ? <SvgUri
                        width="75"
                        height="75"
                        fill={COLORS.white}
                        uri={item.iconSport}
                    />
                    : <SvgComingSoon width={75} height={75} />
                }
            </View>
            <View style={styles.contentSlider}>
                <Text style={styles.titleSlider}>
                    {item.gameName}
                </Text>
                <Text style={styles.dateSlider} >Từ ngày {moment(item.startDate).format('DD/MM')} đến ngày  {moment(item.endDate).format('DD/MM/YYYY')}</Text>
                <Text style={[styles.dateSlider, styles.addressSlider]} numberOfLines={1}>{item?.city}</Text>
            </View>
        </View>
    );
};

export default SlideItem;

const styles = StyleSheet.create({
    container: {
        width,
        backgroundColor: "rgba(202, 200, 200, 0.3)",
        flexDirection: "row",
        height: 120,
        padding: 10,
        gap: 10
    },
    logoSlider: {
        flex: 1.7,
        borderRadius: 5,
    },
    imageSlider: {
        flex: 1,
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
    },
    contentSlider: {
        flex: 6
    },
    titleSlider: {
        "fontFamily": "Quicksand-Bold",
        fontStyle: "normal",
        fontWeight: "700",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "color": COLORS.warning,
        marginBottom: 2
    },
    dateSlider: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 14,
        "alignItems": "center",
        "color": COLORS.white
    },
    addressSlider: {
        paddingTop: 5,
    },
});