import { NavigationProp } from '@react-navigation/native'
import { isEmpty } from 'lodash'
import React, { useEffect, useRef, useState } from 'react'
import { ActivityIndicator, Dimensions, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
// @ts-ignore
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { useQuery } from 'react-query'
import { newsApi } from '../../network/services'
import SlideItem from './SlideItem'
import { ROUTES } from '../../constants'
interface Props {
    navigation: NavigationProp<any, any>;
}

const viewConfigRef = { viewAreaCoveragePercentThreshold: 95 }

interface CarouselItems {
    [anyProps: string]: any
}
const useInterval = (callback: () => void, delay: number) => {
    const savedCallback = useRef<() => void>();

    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
        function tick() {
            savedCallback.current?.();
        }
        const id = setInterval(tick, delay);
        return () => clearInterval(id);
    }, [delay]);
};

const Slider: React.FC<Props> = (props) => {
    const { data } = useQuery('slider', newsApi.getDataListAllGames)

    let flatListRef = useRef<FlatList<CarouselItems> | null>();
    const [currentIndex, setCurrentIndex] = useState(0);

    useInterval(() => {
        if (currentIndex < Number(data?.length) - 1) {
            setCurrentIndex(currentIndex + 1);
        } else {
            setCurrentIndex(0);
        }
    }, 3000);

    useEffect(() => {
        flatListRef.current?.scrollToIndex({ animated: true, index: currentIndex });
    }, [currentIndex]);

    const onViewRef = useRef(({ changed }: { changed: any }) => {
        if (changed[0].isViewable) {
            setCurrentIndex(changed[0].index);
        }
    });

    const scrollToIndex = (index: number) => {
        setCurrentIndex(index);
    };

    const renderItems: React.FC<{ item: CarouselItems }> = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate(ROUTES.SCHEDULES_DETAIL, {
                        gameId: item.gameId,
                        gameName: item.gameName,
                        iconSport: item?.iconSport
                    })
                }}
                activeOpacity={1}
            >
                <SlideItem item={item} />
            </TouchableOpacity>
        );
    };

    return (
        <View style={{ marginBottom: 30 }}>
            <FlatList
                data={data}
                renderItem={renderItems}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                ref={(ref) => {
                    flatListRef.current = ref;
                }}
                viewabilityConfig={viewConfigRef}
                onViewableItemsChanged={onViewRef.current}
            />
            <View style={{ position: "absolute", bottom: "-10%", alignSelf: "center" }}>
                <View style={styles.dotView}>
                    {data.map(({ }, index: number) => (
                        <TouchableOpacity
                            key={index.toString()}
                            style={[
                                styles.circle,
                                { backgroundColor: index == currentIndex ? '#FFFFFF' : '#D9D9D9' },
                            ]}
                            onPress={() => scrollToIndex(index)}
                        />
                    ))}
                </View>
            </View>
        </View>
    )
}

export default Slider

const styles = StyleSheet.create({
    circle: {
        width: 10,
        height: 10,
        backgroundColor: 'grey',
        borderRadius: 50,
        marginHorizontal: 5,
    },
    dotView: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 20,
    },
});