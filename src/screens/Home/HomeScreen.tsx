import { NavigationProp } from '@react-navigation/native';
import { Button, Modal } from 'native-base';
import React, { useEffect, useState } from 'react';
import {
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import checkVersion from 'react-native-store-version';
import SvgAsian from '../../assets/svg/SvgAsian';
import SvgHotel from '../../assets/svg/SvgHotel';
import SvgNew from '../../assets/svg/SvgNew';
import SvgPlacesToEat from '../../assets/svg/SvgPlacesToEat';
import SvgPlaneTicket from '../../assets/svg/SvgPlaneTicket';
import SvgSchedule from '../../assets/svg/SvgSchedule';
import SvgSport from '../../assets/svg/SvgSport';
import SvgSportTour from '../../assets/svg/SvgSportTour';
import SvgUnions from '../../assets/svg/SvgUnions';
import { COLORS, ROUTES } from '../../constants';
import { DeepLinkingUtils } from '../../utils';
import Header from '../../views/components/Header';
import CardHome from './CardHome';
import Slider from './Slider';
import SvgMap from '../../assets/svg/SvgMap';
interface RouterProps {
  navigation: NavigationProp<any, any>;
}

export enum EDeepLinking {
  DLVN = 'DLVN',
  QTKD = 'QTKD',
  HSSK = 'HSSK',
}

const SPORT = [
  { id: '1', title: 'TIN TỨC', icon: <SvgNew />, navigation: ROUTES.NEWS },
  {
    id: '2',
    title: 'LỊCH THI ĐẤU',
    icon: <SvgSchedule />,
    navigation: ROUTES.SCHEDULES,
  },
  {
    id: '3',
    title: 'MÔN THỂ THAO',
    icon: <SvgSport />,
    navigation: ROUTES.SPORTS,
  },
  {
    id: '4',
    title: 'LIÊN ĐOÀN, HIỆP HỘI',
    icon: <SvgUnions />,
    navigation: ROUTES.FEDERATIONS,
  },
];

const SPORT_TOUR = [
  {
    id: '1',
    title: 'VÉ MÁY BAY',
    icon: <SvgPlaneTicket />,
    navigation: ROUTES.FLIGHT,
  },
  { id: '2', title: 'KHÁCH SẠN', icon: <SvgHotel />, navigation: ROUTES.HOTEL },
  {
    id: '3',
    title: `THẺ DU LỊCH -\n THỂ THAO`,
    icon: <SvgSportTour />,
    navigation: ROUTES.COMING_SOON,
  },
  {
    id: '4',
    title: 'ĐỊA ĐIỂM, ĂN UỐNG',
    icon: <SvgPlacesToEat />,
    navigation: ROUTES.COMING_SOON,
  },
];

const SEA_GAMES = [
  {
    id: '1',
    title: 'HUY CHƯƠNG',
    icon: <SvgAsian />,
    navigation: ROUTES.MEDAL_SCREEN,
  },
  {
    id: '2',
    title: 'LỊCH THI ĐẤU',
    icon: <SvgAsian />,
    navigation: ROUTES.SCHEDULE_SEA_GAME_SCREEN,
  },
];

const SPORT_MAP = [
  {
    id: '1',
    title: 'MÔN THỂ THAO',
    icon: <SvgSport />,
    navigation: ROUTES.SPORTS_MAP,
  },
  {
    id: '2',
    title: 'KHÁM PHÁ',
    icon: <SvgMap />,
    navigation: ROUTES.DISCOVER,
  },
];

const HomeScreen = ({ navigation }: RouterProps) => {
  const [showModal, setShowModal] = useState<boolean>(false);

  useEffect(() => {
    const init = async () => {
      try {
        const check = await checkVersion({
          version: '11.0',
          iosStoreURL:
            'https://apps.apple.com/vn/app/th%E1%BB%83-thao-vi%E1%BB%87t-nam/id6446486187',
          androidStoreURL:
            'https://play.google.com/store/apps/details?id=com.sports.ttvn',
        });

        if (check.result === 'new') {
          setShowModal(true);
        } else {
          setShowModal(false);
        }
      } catch (e) {
        setShowModal(false);
      }
    };

    init();
  }, []);

  return (
    <SafeAreaView style={styles.bgColor}>
      <Header children />
      <ScrollView>
        {/* @ts-ignore */}
        <Slider navigation={navigation} />

        {/* <CardHome
          data={SEA_GAMES}
          title="ASIAD 19"
          navigation={navigation}
          type="ASIAD"
        /> */}

        <CardHome data={SPORT_MAP} title="ĐẢO PHÚ QUÝ" navigation={navigation} />

        <CardHome data={SPORT} title="THỂ THAO" navigation={navigation} />
        <CardHome
          data={SPORT_TOUR}
          title="THỂ THAO - DU LỊCH"
          navigation={navigation}
        />

        <Modal isOpen={showModal}>
          <Modal.Content>
            <Modal.Body>
              <View>
                <Text style={styles.titleModal}>Thông báo</Text>
                <Text style={styles.contentModal}>
                  Ứng dụng đã có phiên bản mới.{'\n'}Bạn cần cập nhật để có thể
                  tiếp tục sử dụng. Nhấn nút xác nhận để cập nhật phiên bản mới.
                </Text>
              </View>
              <View style={styles.buttonModalNotification}>
                <Button
                  onPress={() => {
                    DeepLinkingUtils.openInStore({
                      appName: 'thể-thao-việt-nam',
                      appStoreId: 'id6446486187',
                      appStoreLocale: 'vn',
                      playStoreId: 'com.sports.ttvn',
                    });
                  }}
                  style={[
                    styles.buttonModal,
                    { backgroundColor: '#034685', flex: 0.5 },
                  ]}>
                  <Text style={styles.textModal}>Xác Nhận</Text>
                </Button>
              </View>
            </Modal.Body>
          </Modal.Content>
        </Modal>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  bgColor: {
    backgroundColor: COLORS.bgColor,
    flex: 1,
  },
  titleModal: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 15,
    textAlign: 'center',
    color: '#2D2F34',
  },
  contentModal: {
    fontFamily: 'Quicksand-Medium',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 15,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#000000',
    paddingTop: 14,
  },
  buttonModalNotification: {
    flex: 1,
    flexDirection: 'row',
    gap: 10,
    justifyContent: 'center',
    marginTop: 24,
  },
  buttonModal: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    width: 136,
  },
  textModal: {
    fontFamily: 'Quicksand-Bold',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 15,
    textAlign: 'center',
    color: '#FFFFFF',
  },
});
