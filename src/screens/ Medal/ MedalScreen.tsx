import { ActivityIndicator, Alert, NativeScrollEvent, NativeSyntheticEvent, RefreshControl, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import { COLORS } from '../../constants'
import { Button } from 'native-base'
import { useQuery } from 'react-query'
import { onGetMedal } from '../../network/services'
import RenderDataHtml from './RenderDataHtml'
import { decode } from 'html-entities'
import HeaderSeaGame from '../../views/components/HeaderSeaGame'
import SvgReload from '../../assets/svg/SvgReload'

const wait = (timeout: number): Promise<void> => {
    return new Promise<void>(resolve => setTimeout(resolve, timeout));
}

const MedalScreen = () => {
    const { data, isLoading, refetch } = useQuery({
        queryKey: 'medal',
        queryFn: () => onGetMedal()
    })

    const [isRefreshing, setIsRefreshing] = useState(false)

    useEffect(() => {
        const intervalId = setInterval(() => {
            refetch()
        }, 1 * 60 * 1000); // 5 phút

        return () => clearInterval(intervalId);
    }, []);

    const onRefresh = useCallback(() => {
        setIsRefreshing(true);
        wait(2000).then(() => {
            setIsRefreshing(false)
            refetch()
        })
    }, []);

    return (
        <SafeAreaView style={styles.bgColor}>
            <View style={{ flex: 1 }}>
                <ScrollView
                    refreshControl={
                        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
                    }
                >
                    <HeaderSeaGame title='TỔNG SẮP HUY CHƯƠNG' />
                    {isLoading
                        ? <ActivityIndicator style={{ paddingTop: 10 }} />
                        : <View>
                            <RenderDataHtml html={decode(data)} />
                        </View>
                    }
                    <View style={styles.reload}>
                        <SvgReload />
                        <Text style={styles.titleReload}>Tự động cập nhật sau 5 phút.</Text>
                    </View>
                </ScrollView>

            </View>
        </SafeAreaView>
    )
}

export default MedalScreen

const styles = StyleSheet.create({
    bgColor: {
        backgroundColor: COLORS.bgColor,
        flex: 1,
    },
    reload: {
        flexDirection: "row",
        alignItems: "center",
        padding: 10,
        marginBottom: 20,
        display: "none"
    },
    titleReload: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        color: COLORS.white,
    }
})