import { useRoute } from '@react-navigation/native'
import { decode } from 'html-entities'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import { COLORS, IMGS } from '../../constants'
import ApiClientMap from '../../network/ApiClientMap'
import HeaderDetail from '../../views/components/HeaderDetail'
import OpenURLButton from '../../views/container/OpenURLButton'
import RenderDataHtml from '../News/RenderDataHtml'
import { isEmpty } from 'lodash'
interface IDataDetail {
    iconPath: string,
    introduce?: any,
    name?: string
    rules?: string
    gameId?: string
}

const SportMapDetailScreen = () => {
    const [loading, setLoading] = useState<boolean>(false)
    const [dataDetail, setDataDetail] = useState<IDataDetail | undefined>(undefined)

    const route = useRoute<any>();
    const { sportId } = route.params

    useEffect(() => {
        if (sportId) {
            onGetDataDetail(sportId)
        }
    }, [sportId])

    const onGetDataDetail = async (sportId: number) => {
        setLoading(true)
        try {
            const res = await ApiClientMap.fetch(`/sport/${sportId}`)
            if (res && res.data) {
                setDataDetail(res.data)
                setLoading(false)
            } else {
                setDataDetail(undefined)
                setLoading(false)
            }
        } catch (error) {
            setLoading(false)
        }
    }

    return (
        <SafeAreaView style={styles.bgWhite}>
            <ScrollView style={{ flex: 1 }}>
                {loading
                    ? <ActivityIndicator style={{ marginTop: 10 }} />
                    : <>
                        {dataDetail?.iconPath.includes('.svg')
                            ? <HeaderDetail image={IMGS.SCHEDULES_DETAIL} title={dataDetail?.name!} pathSvg={dataDetail?.iconPath} />
                            : <HeaderDetail image={IMGS.SCHEDULES_DETAIL} title={dataDetail?.name!} urlImage={dataDetail?.iconPath} />}
                        {!isEmpty(dataDetail?.rules)
                            ? <RenderDataHtml html={decode(dataDetail?.rules!)} />
                            : ""
                        }
                    </>
                }
            </ScrollView>
        </SafeAreaView>
    )
}

export default SportMapDetailScreen

const styles = StyleSheet.create({
    bgWhite: {
        backgroundColor: COLORS.white,
        flex: 1
    },
    contentSport: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        "color": COLORS.black,
        paddingLeft: 26,
        paddingRight: 26,
        paddingTop: 15,
        paddingBottom: 10
    },
    bgBox: {
        backgroundColor: "rgba(217, 217, 217, 0.6)",
        padding: 26
    },
    listBox: {
        flex: 1
    },
    titleBox: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,

        "alignItems": "center",
        "color": COLORS.black,
        paddingBottom: 15
    },
    linkBox: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,

        "display": "flex",
        "alignItems": "center",
        "color": "#011AFF",
    },
    imgLogo: {
        width: 160,
        height: 160,
        resizeMode: "contain",
        alignSelf: "center",
        marginBottom: 10,
        marginTop: 5,
    }
})