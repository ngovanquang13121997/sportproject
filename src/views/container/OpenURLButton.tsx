import { StyleSheet, Text, View, Alert, Linking, Button } from 'react-native'
import React, { useCallback } from 'react'

type OpenURLButtonProps = {
    url: string;
    children: React.ReactNode
};

const OpenURLButton = ({ url, children }: OpenURLButtonProps) => {
    const handlePress = useCallback(async () => {
        await Linking.openURL(url);
    }, [url]);

    return <Text onPress={handlePress}>{children}</Text>
}

export default OpenURLButton