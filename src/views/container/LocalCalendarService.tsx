import RNCalendarEvents from 'react-native-calendar-events';

export const listCalendars = async () => {
    let permissions;
    let calendars: any = [];
    try {
        permissions = await RNCalendarEvents.checkPermissions();

        if (permissions !== 'authorized') {
            permissions = await RNCalendarEvents.requestPermissions();
        }

        if (permissions !== 'authorized') {
            throw 'Access calendar not authorized';
        }

        calendars = await RNCalendarEvents.findCalendars();
    } catch { }

    return calendars;
};

export const addCalendarEvent = async (event: any, calendar: any) => {
    let permissions;
    let createdEvent: any = false;
    try {
        permissions = await RNCalendarEvents.checkPermissions();
        if (permissions !== 'authorized') {
            permissions = await RNCalendarEvents.requestPermissions();
        }

        if (permissions !== 'authorized') {
            throw 'Access calendar not authorized';
        }

        const eventTmp = { ...event };
        eventTmp.calendarId = calendar.id;

        createdEvent = await RNCalendarEvents.saveEvent(event.title, eventTmp);
    } catch { }

    return createdEvent;
};