import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Alert, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import SvgBackWhite from '../../assets/svg/SvgBackWhite';
import SvgLogo from '../../assets/svg/SvgLogo';
import SvgRightMenu from '../../assets/svg/SvgRightMenu';
import { ROUTES } from '../../constants';
interface Props {
    children: React.ReactNode
}

const Header: React.FC<Props> = (props) => {
    const { children } = props
    const navigateUrl: any = useNavigation()

    return (
        <>
            <View style={styles.headerApp}>
                {/* Thông báo */}
                <TouchableOpacity
                    onPress={() => {
                        navigateUrl.navigate(ROUTES.COMING_SOON)
                    }}
                    style={styles.logoNotification}
                >
                    <SvgRightMenu />
                </TouchableOpacity>

                <View style={styles.logo}>
                    <SvgLogo />
                </View>
            </View>

            <View>
                {children}
            </View>
        </>
    )
}

export default Header

const styles = StyleSheet.create({
    headerApp: {
        padding: 10,
        position: "relative"
    },
    logo: {
        alignItems: "center",
        marginTop: 20
    },
    logoNotification: {
        position: "absolute",
        right: 0
    }
})