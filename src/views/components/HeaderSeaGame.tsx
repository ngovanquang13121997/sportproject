import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import SvgAsianMedal from '../../assets/svg/SvgAsianMedal'
import { COLORS } from '../../constants'

interface Props {
    title: string
}

const HeaderSeaGame: React.FC<Props> = ({ title }) => {
    return (
        <>
            <View style={styles.svgSeaGame}><SvgAsianMedal /></View>
            <Text style={styles.titleSeaGame}>{title}</Text>
        </>
    )
}

export default HeaderSeaGame

const styles = StyleSheet.create({
    svgSeaGame: {
        alignItems: "center",
        paddingTop: 20
    },
    titleSeaGame: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: "center"
    }
})