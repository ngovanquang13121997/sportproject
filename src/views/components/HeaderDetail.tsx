import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { SvgUri } from 'react-native-svg'
interface Props {
    image?: any,
    title: string
    pathSvg?: string
    urlImage?: string
}

const HeaderDetail: React.FC<Props> = (props) => {
    return (
        <View>
            {props.pathSvg
                ? <View>
                    <SvgUri
                        width="160"
                        height="160"
                        fill={"#2e3192"}
                        uri={props.pathSvg}
                        style={styles.imageSchedules}
                    />
                </View>
                : <Image source={props.urlImage ? { uri: props.urlImage } : props.image} style={styles.imageSchedules} />
            }

            <Text numberOfLines={2} style={styles.titleDetail}>{props.title}</Text>
        </View>
    )
}

export default HeaderDetail

const styles = StyleSheet.create({
    imageSchedules: {
        width: 160,
        height: 160,
        resizeMode: "contain",
        alignSelf: "center",
        marginBottom: 10,
        marginTop: 5,
    },
    titleDetail: {
        "fontFamily": "Quicksand-Bold",
        fontStyle: "normal",
        fontWeight: "700",
        fontSize: 15,
        display: "flex",
        alignItems: "center",
        textAlign: "center",
        color: "#011AFF",
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 5,
        paddingBottom: 5,
        textTransform: 'uppercase'
    }
})