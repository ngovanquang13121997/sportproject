import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

interface Props {
    title?: string
    numberOfLines?: number
    styleProps?: any
}

const TextComponent: React.FC<Props> = (props) => {
    const { title, numberOfLines, styleProps } = props
    return (
        <>
            <Text style={styleProps} numberOfLines={numberOfLines}>
                {title ? title : 'Đang cập nhật...'}
            </Text>
        </>
    )
}

export default TextComponent

const styles = StyleSheet.create({})