import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import SvgRightMenu from '../../assets/svg/SvgRightMenu'

const Notification = () => {
    return (
        <TouchableOpacity>
            <SvgRightMenu />
        </TouchableOpacity>
    )
}

export default Notification

const styles = StyleSheet.create({})