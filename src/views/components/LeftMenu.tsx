import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS, IMGS } from '../../constants'
import SvgLeftMenu from '../../assets/svg/SvgLeftMenu'

const LeftMenu = () => {
    return (
        <TouchableOpacity style={{ marginLeft: 0 }}>
            <SvgLeftMenu />
        </TouchableOpacity>
    )
}

export default LeftMenu

const styles = StyleSheet.create({

})