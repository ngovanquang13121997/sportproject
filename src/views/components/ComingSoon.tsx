import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import { Button, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import SvgComingSoon from '../../assets/svg/SvgComingSoon'

import { COLORS } from '../../constants'

interface Props {
    navigation: NavigationProp<any, any>
}

const ComingSoon: React.FC<Props> = (props) => {
    const { navigation } = props

    const onPressLearnMore = () => {
        navigation.goBack()
    }

    return (
        <SafeAreaView style={styles.bgColor}>
            <View style={styles.comingSoon}>
                <View style={styles.logo}>
                    <SvgComingSoon />
                </View>
                <Text style={styles.textComingSoon}>Tính năng sẽ được cập nhật trong phiên{"\n"} bản tiếp theo.</Text>
                <TouchableOpacity onPress={onPressLearnMore} style={styles.bgButton}>
                    <Text style={styles.back}>Trở lại</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>

    )
}

export default ComingSoon

const styles = StyleSheet.create({
    bgColor: {
        flex: 1,
        backgroundColor: COLORS.bgColor,

    },
    logo: {
        alignSelf: "center",
        marginBottom: 20
    },
    comingSoon: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "center",
    },
    textComingSoon: {
        "fontFamily": "Quicksand",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: "center"
    },
    bgButton: {
        backgroundColor: COLORS.white,
        borderRadius: 15,
        alignSelf: 'center',
        marginTop: 28
    },
    back: {
        alignSelf: "center",
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": "#034685",
        "paddingTop": 12,
        "paddingRight": 49,
        "paddingBottom": 12,
        "paddingLeft": 49,
    }
})