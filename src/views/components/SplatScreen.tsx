import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import SvgLogo from '../../assets/svg/SvgLogo'

const SplatScreen = () => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#034685" }}>
            <View style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}>
                <SvgLogo />
            </View>
        </SafeAreaView >
    )
}

export default SplatScreen

const styles = StyleSheet.create({})