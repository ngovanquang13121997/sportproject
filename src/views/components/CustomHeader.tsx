import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import SvgBack from '../../assets/svg/SvgBack';

interface Props {
    color?: string
    onGoBackData?: () => void
}

const CustomHeaderLeft: React.FC<Props> = (props) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={props.onGoBackData ? props.onGoBackData : () => navigation.goBack()}>
            <View>
                <SvgBack color={props.color} />
            </View>
        </TouchableOpacity >
    );
};

export default CustomHeaderLeft