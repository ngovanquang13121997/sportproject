export default {
    HOME: 'Home',
    NEWS: 'News',
    NEWS_DETAIL: 'News Detail',
    SCHEDULES: 'Schedules',
    SCHEDULES_DETAIL: 'Schedules Detail',
    Follow: 'Follow',
    FollowDetail: 'FollowDetail',
    SPORTS: 'Sports',
    SPORTS_DETAIL: 'Sports Detail',
    FEDERATIONS: 'Federations',
    FEDERATIONS_DETAIL: 'Federations Detail',
    COMING_SOON: 'Coming Soon',
    HOTEL: 'Hotel',
    FLIGHT: 'Flight',
    LOGIN: 'LOGIN',
    REGISTER: 'REGISTER',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    INTRODUCE: 'INTRODUCE',
    SUPPORT: 'SUPPORT',
    InfoUserScreen: "InfoUserScreen",
    CHANGE_PASSWORD: 'CHANGE_PASSWORD',
    CHANGE_PASSWORD_SUCCESS: 'CHANGE_PASSWORD_SUCCESS',
    ForgotPassword: 'ForgotPassword',
    FormForgotPassword: 'FormForgotPassword',
    SettingScreen: 'SettingScreen',
    MEDAL_SCREEN: 'MEDAL_SCREEN',
    SCHEDULE_SEA_GAME_SCREEN: 'SCHEDULE_SEA_GAME_SCREEN',
    SPORTS_MAP: 'SPORTS_MAP',
    SPORTS_MAP_DETAIL: 'SPORTS_MAP_DETAIL',
    DISCOVER: 'DISCOVER'
};