export default {
    gradientForm: '#A376F1',
    primary: '#7d5fff',
    white: '#FFFFFF',
    dark: '#444',
    bgColor: '#034685',
    warning: '#FFE601',
    danger: '#FF0D0E',
    gray: '#666666',
    grayLight: '#ccc',
    black: '#000000',
};