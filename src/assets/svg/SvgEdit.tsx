import * as React from "react"
import Svg, { Path, Defs, Pattern, Use, Image } from "react-native-svg"

function SvgEdit(props) {
    return (
        <Svg
            width={18}
            height={23}
            viewBox="0 0 18 23"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            {...props}
        >
            <Path fill="url(#pattern0)" d="M0 0H18V23H0z" />
            <Defs>
                <Pattern
                    id="pattern0"
                    patternContentUnits="objectBoundingBox"
                    width={1}
                    height={1}
                >
                    <Use
                        xlinkHref="#image0_8_236"
                        transform="matrix(.01 0 0 .00783 0 .109)"
                    />
                </Pattern>
                <Image
                    id="image0_8_236"
                    width={100}
                    height={100}
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAGvUlEQVR4nO2da6hUVRTHt3vPVJo9LIqS0IqiFxQlhBV1K2ftOekUQd0+FCVBL+tDhfQgiJN31pq5WhoWlX6ooPqQt5faS0gDqcQwoqLECKEHYYIaZA+17Maaa9frzD5nzjlz5t4Zz/rB/nbP2ufu/6z9WHuftZUS4tHj51Rh/uT8jOr5ylZOUyV/QkwLQqvki3iBsVjWgF8aoL3G0uCBBX8xgC8YS9eqXv+QlisUAvDobGNpoFGAkAL0o7Z4h+odMEFmhbj4vjYWye0N0Yq2+LGaueCE2HULdfT4EzXQ8qRC1HtLbawREuL5R2rAdcGNjHu0pTUG6GkN+KixtNAALjMWt4WIsr02+AspigG4Q1t6WBX6j3I+2ztgjK2UNOAXAcJsDHxWiCeGtrheFfFEFX3smR/gXUsi2cg8XpgY9J7q8Q+L20ba0lyHvb9Vse+MzLd3C2KsSiLG/xiLLzvGk1eS2jv4gccP15bWOsUA+kBNXzi+Nfv+8QbwtzrbO5W3+NDU/odMeAbgh2mFQ4ylZxrqKGAxDdtZ6abWsuekVRU3vmOS4Kdl/yBfZ+C/2tKcVEMeHh3nqGtpavYP7kUfDa8ZTBFnpVLn0DR4T50XrkzFdkbEGBwxI3qVf+Et1TttaZ49r872gMo0Xng3FS4K/pSz5YsT1z2zPNVhd7HKLF7YbIre4Y0mDfSgsbg5WBjco4uVm5NUr6Ey2zFxmKsyiddEjJHrAd/X2tKNxuIPgQN+ge6N+wpcT72tvK2epzKHF7rOeDtwcTZ94XielnKYwy0K3hb1FXIFnN7QJQJ9rzKHl1CMEeSKlYu48Rxjyj8c1W36DtfMP0IDfe2YKDymMoXXRIw4+90zKsdqSxscouxQUD4l8LlpS/Ncl+MdtrJQKjN4oSvwlYkOH7BNSx85xqBPnQtIFsPSmwHj1p0qM0BIoJBD6K0E9GZVJxmL3zbaxfuiimEAX1KZwWuDZ9Rj6UyO1NbZ38nT5uaegetaCeN3F9BGz6iDp72OX/5ToWJY/FxZ/xiVCbzQbdcVqR9Y6x0w3MB1guzSgO8H/CA2qB7/aJUJIMwz8N12bQIZoOvdC0fxjHUBM5nlbT3KydFb1/pEPING1TNGYgCfEM9ovuh7a1QOOZf8CbzFK54BoQcSDgwUtlMMi6tlzAARo3MAEaNzABGjS8TAN3h13PZ3kDFDxOg8QDyjcxAxumYAf13GDBFjMJshdPGMDkLE6Jr9jNdkzBAxBmXMEM8YY6Sb6iIxOOVRuynJfoaI0XGIZ3SHGLXPuqSbGkVEjA5CxOggRIwuEQNwmYwZo4mI0UGIGB2EiNFBiBgdhIjRWXD6u5Aj+asNVKxSg+PaGyikNXIKnSnMnxzxK6L1OYsXihhtxkD5yiiCDK0/OFU3LkntBoGSeEYDuoh3RRZk/8Lwm3yx7xwRow3U0ms3NvjVnAeK02qHCPOrKZYvT1RpSTwjkIY8HoC7htNMeP6RBnCe4yP74b81gFeJGCniSDOxseGPCv1Tgr7FMxb/zAFdEqmykmy7htPj5yIndRxKAFkO6L62Ns2nXpJuqjmFvtMdv/hFYY/wbTOui1F4HcGiiRgtwOlTHV803d3sOQ10f8Aibk7DH5fEMyLDKYkaB2pelUcQE+g5h3dtO+BkeUnEiIUBfLa+UUOzq42k5E8wQJscnlIRMRLS8OE80O44qblzUL7MsZrfzgmLJTaVgIY0qkCb4trgPFaO9ckWCRTGZajL2duQ1iImuSJdGi04SRnKN5WAvEfnOrqbJ5PYChhLBrP5GVlCjMXrGqe8dE/zJwfHKa//JGPpCm3p9tqlWYDfiWe0iC7QI6G3xcyqTsrb6jRt8RYD1M/HRbXFzwLjWuIZrWEsvejo59fuy+D8V9RGF89ICQ30SauNLp6REtqWbxrKe56SCEC7eWCvhfIB52UrBXeL6CLd4L4toFnhqDBu5ivo+N4lbfEhA9hb2zmUq62TwbfLhHrGyF+6xUUcaDSWoBZOkUZPHwP0vEOEP/jyE2n0MYCvqa4Xg0+djMW7CEyxevLwqhpwS65APdIwYw3v6hX6p4xKugtBEARBEARBELIF3yQ2o3pqSyXpdyAc95pZntpy/d1a6o/VchTWWPw9jbA6Hx2NowXvMhqLP7drr8V0SeELLFWPP1HxzTRxtlmjiDJkOBrBp+Qpc0UDPlC78zVtw8MXLkYRxNJXY90QpnPKgn2NgitSU9nSqjhdVtBBbJO1Arhr/13rtRuOK7MNYLV2YiRh0bZya5L7oPj0Su1bEkhedzcXPtGjCn1ncWP8B8D6WnxC9OqeAAAAAElFTkSuQmCC"
                />
            </Defs>
        </Svg>
    )
}

export default SvgEdit
