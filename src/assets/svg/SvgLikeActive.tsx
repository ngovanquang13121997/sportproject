import * as React from "react"
import Svg, { Path, Defs, Pattern, Use, Image } from "react-native-svg"

function SvgLikeActive(props) {
    return (
        <Svg
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M6.3 23.363c-.263 0-.487-.038-.75-.15a1.822 1.822 0 01-1.05-1.65V3.075c0-1.313 1.088-2.4 2.4-2.4h10.237c1.313 0 2.4 1.087 2.4 2.4v18.413c0 .712-.412 1.35-1.05 1.65-.637.3-1.387.187-1.95-.3l-4.124-3.6c-.263-.188-.563-.15-.825 0l-4.125 3.637c-.338.3-.75.488-1.163.488zm.563-21a.712.712 0 00-.713.712v18.488c0 .037 0 .075.075.112.075.037.113 0 .113 0l4.2-3.713a2.366 2.366 0 012.925 0l.075.076 4.087 3.6s.038.037.113 0c.075-.038.075-.076.075-.113V3.075a.712.712 0 00-.713-.713H6.862z"
                fill="#454968"
            />
            <Path
                d="M5 21.5l.5-19 .5-1L16 1l2.5 1.5v19l-1 1-6-4.5-5 4.5-1.5-1z"
                fill="#454968"
            />
        </Svg>
    )
}

export default SvgLikeActive
