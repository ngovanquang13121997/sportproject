import * as React from "react"
import Svg, { Path, Defs, Pattern, Use, Image } from "react-native-svg"

function SvgEye1(props) {
    return (
        <Svg
            width={53}
            height={30}
            viewBox="0 0 53 30"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            {...props}
        >
            <Path fill="url(#pattern0)" d="M0 0H53V30H0z" />
            <Defs>
                <Pattern
                    id="pattern0"
                    patternContentUnits="objectBoundingBox"
                    width={1}
                    height={1}
                >
                    <Use
                        xlinkHref="#image0_528_82"
                        transform="matrix(.00566 0 0 .01 .217 0)"
                    />
                </Pattern>
                <Image
                    id="image0_528_82"
                    width={100}
                    height={100}
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAI8UlEQVR4nO2ce4ydRRXAh5m7QukDjUagYgWCKBqJpNE0vq7sfmfutd0uNWaj0cTHXwaRIEqUGM1n9p5z725rDSuUWl8xoBIbNL4BxUew+odWgfqvqbg8qpTSFSmlNGbNmW+LfdzvMfN999Hl/JJJNtndeZ2Zc+acOfMpJQiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIPSBZrxKRTOrXeGfhR4zHp9tLF2pga4xQDdrS3cbS3sN0L+NpYWuJfndXve3gF92/9tovVOt27pM5OVLPa6ZRisygB0N9HsDdCR14n0L0BFtcZex1DYRjanJnUYE1I3JncZAa9RY/IoBfLwyAeQKCB83QNt5B4pwmGj6HB3RdQbw730Tgk0r+KgB+oKqxy974e2aaOrVBugbxtLhwQuCThbMM8bS15RtX6KWPLb1SmNph7F0dPATT3nlqAG8TY11LlZLjma8ylj8UkkDvV8D/pp1vlNzUXuC7c6I7azlHceFf3a2KGpPJKqQtrv/sbQ/uF3AZw3gF9XEzEq1FNC29V6nn/0n4z8a6E5t8eMjjanXK7VwRngvFs7gOrguDfh9rjugP48YwEl12hLNrF70GTxWIx3RQD9kIbL/0bO+jcdna6D3aYs/8t212uLPVAPPV6cTBvDdxuITnsfPadWcvqDvnR2lc7XFz3juYlaBm9TQ05w90/kSHmpAA31U1eOzgtqrxzUVTa9R0dRlSZleE+xP1OOztKWrDdBjHjt6O49ZDSUNPF8D/qHgjnhSA33aO5QxudMY224ai7doS3u6qhvnjbvf3WwibHgLiNWZpRuNpYMFVdgu3mVqmHCnGzZ6hVYW3q4gfrlXA/V4hbatG4q3cdIutPQpBVuWe7U5SucawO8U3ClzI2OdK9QwwEE7A/hUgY7vdfGjEHsE+HCAIE6ZNNPAq7zbtwQG6KECC22+1qC3q0FiGrhh0bPNm4xve4fGJ+MXedqjgoLBW7lur75E0+cYS98tMM5DBvBdahCwfs4/MuIz2uIHvSuvxys00C8rF8YxvQ/4C28Vxj4VtD+UuwB5ThqtSPWTmsU35zpXgA/Xmu03eVe+dseIt/8SIhRLvwo5IY0AvjE3GAp0qGZbb1F9wbYvMUAHclbgH70N9yKmF2oqvWwr4bvszql7v4LWRaqnOA8XH8xVB/V4RbhDSQt9LQGG3jExs1JbvDd7F+JfenpLaSx+K1sY9FNvg3mMeryiktOUd8F/BIdpmrNnasC7stUXflP1AhddzV4N9wR73EkQ8ob+C+P5hXR98MSs27qM7VHOLtygqg+fpztlbmsGnFpO9MCDnD53/md9nuh0nA+qA2iu1BUuq68sVc71Vxm+N5amMhp7rGxQMAmH+K5qfMCtPI5nHZ8gYdvjeXau+youeVTlyzfAfRmLNi5V/3GDfHFqXAfovy5BoCQcm/JUMXdmqkcOFCb3Hj5CmS09Dpct4+aky1zhk+xglm1DaUvXZgxic+kGkjb2+OwMVcRWJbrdr94KcDej6QvpmtINaIu/TdHdj4Yeb0+A1YzPJVGjuIFk9VW4XsBnK0kFmphZmRbC5+vkcpWv3TGSlpDgwudV0Jy+wEOtHDzBZhTqf3FDr0bxFVUMaTF8362No179PwVoXZTa+bH2pVV0XjXpdYXViqU/+Vavgf5cWCB8yVUFll6b2kajc2F4xY3OhekVT72m/wLB3b7V85G87wKJpi5LbWN961W9UVmWbuy/ysJ57pNX/7OSsnulsiL6bEr/nyunspIt/5tU/6MKZ8fl9fpkfrTHi1ZtADcWr5cOV2LU2YlO8UdchLksnMuUMYitAzj2Plj82It/La4O6f4qxmIs3pTR94+VbyG5LUt3DAOuZU8ZBCckFF/JC+z0ZQol8UF+4FMnT2T5cbRtumNIByp7UMRZ4akDAdzHYYPSA/GaPOIVvYdV0gk2xdkM3OizM54vZRdWNL3GWPxnxiL6vKqMxNmZy2jsgVL2xNmR0NA7zvPR1h1vPQz4Sat3TsWxDu5/M16VqXaBHqrEiT4eE7XXZ69YvLfMZQyn6pgggZQv2uIngieG1WOS0J0hcNyoegFftmQODPCu4Ew+2LI8axf2rPDqDb2gctmOeE/ODv666hmJsbw/P3kgzHgZS5v6L5DA1cuvv9Jcgv/vvN1lLu2KMda5OO+9BetztX7zeSHVG8Bb+ycQvKVEZn/mwjSW/lUuTOIBp/cUyFZ8hNOFgtKAIOeeuoLibF5IGtBY5wq+h8/ZdU+5h0T9ZDHFMs/DPqwBP+xdOWxZnq+bywiD7g5KlLPtj+S+i3QvrlqjahBw2qRLn8xXDXfwzaN/Kilt64ma8omHMRs6LzGA3ytQ99PsU6lBwgnGhe4cOGGZnwd4Yhp4Va6KKFK4/QADvrjoipz+DtaA3qqGgSS9suiRFe/wNvjj7gna9UHOI9Cc8zN8j7bRzOpiuyIR9kiTLldDRZJeuaugUOY5RO2tx+NYu0QCS7Mu64T1dRcdnpyA8Cany3098CTR+3NFvX4NeF9o2mzvYb3vc2wF3Ocu/kM9/MmdxqUgHXvSxj+HhtCTl1PXZsWjupRtwZma/X+qkJ6j1G3HuDQcWy5IGYR7KeWCp/u9HqlG7Ql1WrF+83n8hNhT3x/Rln6sLb2/8mDcqU/kPqABf8K3eD595P4N3ZtCH4zF9wQa40PuzXpE141EU28o/eGAJl3OdS2+TT8Ucjjg7Hy1JEgy27d0NcLFJ+TAYuxohztxcdwrojHnEXMGzFj7Uvf4NDH6mzS0Pun+lvPKct6z5JTDxuJMqdzloSVJaJgtJRjbr4LPuY/P9PzRzTDAAUrArwapDttzQTzNr7heGIJI+4AZP5keuCBoL39iQ421XzroaRk8caxrUettiTrz+LRF+d3wBKslF1Ipmy+1ZJl0j3euNECoLf2uUnvDXjzgfcZiy32lVD6CGcC6rctqEdU5p4nDIe6uBOhvOd8iOch/oy393P2Ppatr0HpH72/vBOWyXfj7VVyWylffBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQ1JDzPyX8e8dO+DCXAAAAAElFTkSuQmCC"
                />
            </Defs>
        </Svg>
    )
}

export default SvgEye1
