import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgRight(props) {
    return (
        <Svg
            width={4}
            height={18}
            viewBox="0 0 4 18"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M4 2a2 2 0 10-4 0 2 2 0 004 0zM2 7a2 2 0 110 4 2 2 0 010-4zm0 7a2 2 0 110 4 2 2 0 010-4z"
                fill="#332E2E"
            />
        </Svg>
    )
}

export default SvgRight
