import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgFavourite(props) {
    return (
        <Svg
            width={17}
            height={17}
            viewBox="0 0 17 17"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                clipRule="evenodd"
                d="M15.217 15.171l-7-3.888-7 3.888V2.727c0-.86.896-1.556 2-1.556h10c1.105 0 2 .697 2 1.556V15.17z"
                stroke="#AFB2B5"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    )
}

export default SvgFavourite
