import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgRightDate(props) {
    return (
        <Svg
            width={12}
            height={20}
            viewBox="0 0 12 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M1.763 19.488a.92.92 0 01-.6-.226.838.838 0 010-1.2L9.038 10 1.163 1.975a.838.838 0 010-1.2.838.838 0 011.2 0L10.837 9.4a.838.838 0 010 1.2l-8.474 8.625a.885.885 0 01-.6.262z"
                fill="#fff"
            />
        </Svg>
    )
}

export default SvgRightDate
