import * as React from "react"
import Svg, { Defs, Image, Path, Pattern, Use } from "react-native-svg"

function SvgReload(props) {
    return (
        <Svg
            width={22}
            height={17}
            viewBox="0 0 22 17"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            {...props}
        >
            <Path fill="url(#pattern0)" d="M0 0H22V17H0z" />
            <Defs>
                <Pattern
                    id="pattern0"
                    patternContentUnits="objectBoundingBox"
                    width={1}
                    height={1}
                >
                    <Use
                        xlinkHref="#image0_1030_674"
                        transform="matrix(.00773 0 0 .01 .114 0)"
                    />
                </Pattern>
                <Image
                    id="image0_1030_674"
                    width={100}
                    height={100}
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFqUlEQVR4nO2dW4hWVRTHt45jdLFMxV4qwzKksjcvKXhFCyd6qJeMjGDKYMg0dai8RIUIJj1UREgIiWWS0AWfygbSl0AYQnrotSwDoUJTJ3Maf7H59gdfztmX75tzzt77nP17FGfPWuc/a1/W2hchEolEIpFIJBKJUQBzR/9rwgvAG8AV4PkkQRhiNJGirPdtU23h/2K0ivKCb9tqB7AzQ4wUKR4F6QVGLKJs8GZgHQGeVR/exMu+7awVwDoHUV7xbWetADZgZ6tvO2sFsNFBlG2+7awVwCYHUbb7trNWAFscRNnh287KAlwLzATmAA8AC4CDqfsq5+PfCDwMvAUcBU45fPjUfRUgQi9wDBgmf7bkanBVAe4CPgCGKBa5hlnn299gAe4APgb+pRzOy3HHt9/BAXSrBd55ykNG3zLfvgcHMBs4WaIQTTFW+fY9OIAnO4iKX4D9skIIrFTjzc0qyuQk4LiDGCt9+x4cwOttiHAK2AXcY2hvInDE0s5FYEW5ngYOMB5431GIH4AngC5LmzI6vnAQY3l5nkYC8LaDEH+qQX68Q3tdwCdJjOK6qc+BaY7tTQAOW9q7ACzpxN46DOAmhlVUjGtDjEOWNuWEYXHx3kUGMAv4y/DhLgGPtdFeF3DAITKWFutZhKjZz0nLNLStLgXYZxFDir+oOK8iRpZPDR9Opkge7aDNh4C/DZGRxgxDbkpON3VsHIPQPaqra+WsrIt02mblUYlC02xq3Bjb72kRRYoxPz/rK4ZKaeiytr+7Tm0dRTkDzLP+5zqj6hk6nsn5d12fZ3uOv3MK8LSIAZXkGzKkQ6wr8JBRYgwCf8i6vggdGQGG6FgjIgaYDJxo8adXhA7wrSFra0wURhIZrRwTEXRXug0Ju0TckTGY4dNl4CYRKmqrjg5tPSN0gFsMRyDaXtyWhto3ldldicgBvtP49o4IFeAbjdEfisgB9mh8OypCxbCjMPoDmcAajW+nRYjIBZrhAE30GwuA+eiZJAJNJuqYKSIHuNXg3+0iNNQudB1TROQAkwz+3StCA1hoMHiiiBwaZWMd4aX91RmNKgtyTWyC3FfxLmtabF3WjCqu0q+q8UQ1qMs8lo5HROTQqOVnIVMq14kQAX6r6gF+9GfifxahAgxojP5SRA7wkca3r0SoAO9qjD4jIgc4rfFtjwgVuQMRPXeKSAHuN/i1WgReyMkqUMnizmQRKcBujRiXg8xjtZJximkw5nUIjRX6rxpBBkRkmxy+B6aKiAGeMnRX4W8FknNytUUm6sho2Wn/o0aMi8F3V03kX44PMch50xzQZ4iOvXn+rsoBzFPbSntyam+6Ol6XxXAVajxFV/POqo91aayiyI3gllO9+/OzvmLQSPs3xSAPUYB+y1G52/L1oiLQKIqd03y4fzpJasqfsdw+1F+MN5EDLLKcXZTsa7PNZYbTWc1pfHdxXlVbjE/loq7NyBiydFWzi/UsQoDFDnekHHIVQw3g/Q7XQj1evHeRASxRBztNHG5DjOkOd6RI0uWZVyPPnTuI8ZlLH6/uXOkzrDNaec/WXu0AlltO9KIuoOl2SBSuNaRDRokR+4mv3AFWOIhxxLTVSNUzdhuytlm8Wq6nEUDjkjLbZZjH1QYLeWvEVOBu4EFVAz9gqPPrkLO3NIBniLGqhJtJs9YZs7z89UWwzrhQohCyS3ytCjssc4fGleFl3VA6rO5wTLmpMT7UkkdE7E0p9HxfOugkGgZU8SyOSl9IANvpHBlhPwFfq7OBq4EbfPsUPcA2h49/UI07C9QO/BlRXHkRK5iLRU3S6zkBjilbSzWq7gCbHURJbxcG+PjXS6UaVXeAFy2CpJejPb1deMUiSnpjvWRRnnMQpa9Uo+oOsN4iyk7fNtYO9LmvN33bVvfnvEeSGOGdSxlJkREQwFzfNiQSiUQikUgkRID8B6uqUSeoaeG6AAAAAElFTkSuQmCC"
                />
            </Defs>
        </Svg>
    )
}

export default SvgReload
