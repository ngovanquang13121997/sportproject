import * as React from "react"
import Svg, { G, Rect, Path, Defs } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function SvgLeftMenu(props) {
    return (
        <Svg
            width={100}
            height={100}
            viewBox="0 0 100 100"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <G filter="url(#filter0_d_5_12)">
                <Rect x={10} y={10} width={40} height={40} rx={20} fill="#fff" />
            </G>
            <Path
                d="M19 30h22M19 22.615h22M19 37.385h22"
                stroke="#404246"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Defs></Defs>
        </Svg>
    )
}

export default SvgLeftMenu
