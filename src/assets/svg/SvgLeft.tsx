import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgLeft(props) {
    return (
        <Svg
            width={12}
            height={20}
            viewBox="0 0 12 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M10.238 19.488c-.226 0-.45-.075-.6-.263L1.163 10.6a.838.838 0 010-1.2L9.638.775a.838.838 0 011.2 0 .838.838 0 010 1.2L2.963 10l7.912 8.025a.838.838 0 010 1.2c-.225.15-.412.262-.637.262z"
                fill="#fff"
            />
        </Svg>
    )
}

export default SvgLeft
