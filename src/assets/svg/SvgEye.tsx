import * as React from "react"
import Svg, { Path, Defs, Pattern, Use, Image } from "react-native-svg"

function SvgEye(props) {
    return (
        <Svg
            width={53}
            height={30}
            viewBox="0 0 53 30"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            {...props}
        >
            <Path fill="url(#pattern0)" d="M0 0H53V30H0z" />
            <Defs>
                <Pattern
                    id="pattern0"
                    patternContentUnits="objectBoundingBox"
                    width={1}
                    height={1}
                >
                    <Use
                        xlinkHref="#image0_64_128"
                        transform="matrix(.00566 0 0 .01 .217 0)"
                    />
                </Pattern>
                <Image
                    id="image0_64_128"
                    width={100}
                    height={100}
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJFUlEQVR4nO1daYwcRxWurZoxzgEOBhIIhxwwEj8QBCzAEsdgb7+aTWIgIl4OifsIiH8IzCGIGpj3ZtbkQAZC4nApgEJkQALCoQikAIGEH5wyDjJXEgcnIjHrhDhOsokZ9KrHeL3ZnXk9XX1N6pP6x652u46v3qt6R71WKiAgICAgICAgICAgICAgICAgV0DnjHwbCBBDt+l1xuKCBnxfmLZKkEEPGUt9Y/G/gZTKkHH0CaRUiIxASinQUeeNBvDh5clYRIqld+fWCYhPbQKe2QB6qWl3ItOm1xrAWQNdyz83p3svUNH209WGnU010ZjuPZM38OFkeCRlhp5kou7ZGvACDfRdY+kmY/GwrH3qG6AjBvBmbfFaA/QZXkw8BjVJ4Enmyc6LlCbgmQawpy3+Tt5O2gf/zgSZdueVqhU3VN2RKynRp55tLP0zHyKWlaJ/G6ArG9B5haoztMV3OJUgJSWNnRIVTMrg0ZZ+ry2erzZefIKqIyZKUuxiqcE7tMUPq1a8WtUNuUsK4G2lkJIQc5u2+Bal+lOqTphYSbFHVRn+stnGF6o6IVdJsd31pUpK8jxkgOZqZdvkKim2u95Y/JIG/KixeB4fj51NYeO1ajZe5Z7p7hP4d+7oHGFbA73fWNqpAX9hLB7yIi1Av6qVLZO3nTI2ZnY8xljaZCySBvptJmIA/2OAtqo6QNvO6w3Qg/IBFkjKIjSBnuuMQ8A7xyOG+40fq/SGr23ng+NZ1+WQ4jAbr0qkmv4xJjFfZ+lTlUIcawP42Wz6uURSGBt2Nt3BZIwTnQa6TrXik1UlkAzkWz42TFM2KYxXb3+ssXjJyuGFFUixdD3/b6l9V7O7jLF0lR8yKkQK7zG293xtcXc6ScEbVDS3pqQu96eMxcv9kkHHSKlCjL4VrzaWdqTZF7XFX6st8YmF93Ug1vJJBrxIt/G9uR2J41hrwLfxKc9Z1R4NuCT4Rfel2FO+zf1RRWFgcEmJeEDb7tuLMx5psCnjAQN0WSOilo8xN2a6LzJAt6dYhDtUEXBh0pEh3KNk0D3LxRmKI4UGaoR+7sUXFc09w1jcm0J9nZ+5TYH3dV4oGfMNiy9e6VVFk2Kcnw0/n1mVnf3pJ2ugP0m1w7A5yAa48CQDuEc4gQecr2kEeAUVSop1q/ZnzueVNdYvnQugffz3yjcM4BXCiTvUgO5LpO8tgxRjcW92UuaeZizeKlRd31M+YSydK5ywBWO7M2nfX4qkAF2XWX1Zeo6xdLDY/aSNTzGW7hI1GuG7xm2mFEkB+pzKCGO7W4RxoHu9uO35TC2bJLo08+CA5oRtedvoXVJd5n7jJ4VScm3Whl4lDtpkNsT6U9rib+SE+CHFbfJZwUappR+J5qrdfXOGUxXtK0oUpeSbHEjxkpNl47UG6JaR/QW6fSzPsFgMM+wbi6EBvzMeIWP4vtq9dZxiumiSLvMxBgOdzaJ9EOgT6d68GZ8qiUFrwGt8DIRXjLF0//iEZJQUwDt9pZQawK8I+nrIJYOneOkXBJNwl9pMp3kZhIt1ZyEjOykSQ1aEc3qPF/m8AK+QSwfgA0X6aTTQh0ZNtMn5SMx3XjyO5w2Cfi4o23m6F7e6BvwDB6d8DSCJOQxt86a8fV+LPdLZwSdGulHQz0sEuhzvFgx6k7/OOxX5teELgH6Qf4qR3+wRd5loZF/x7uEnri3xiRJvbiPqvMxn5w3QN0ZI5I8LyZD0CI7DCPp4cOQRWKSyLN3oc0XxsXN4e7i7Fsl4xxmKEiN3lMo6FoQZeW2NQ6WFbepAR9inVhdSBnlew/sE9KBoU2cYS18UDHK/asWn+BiAATxLIJUfWX7gFSNlM50mC+Lh5fKXRttPFyUnA33ZXy7UiDwowHm+fVt1UgzQ1YK+3MuRxxxcJ24iwMdAeOM2o6Xk+uXSNqtCisz+cIeUC9K/nY/AgHeMlhK8ma1TL6k2VjAYJqWKkgKdM2QmA+5nx+1YbfB1Ltkk4Q8z5yElWZB/kU0oHeR7Ikv9QaWR4tJpJYYg9bnIQYaW+lMa6CeihlJ7MLOEimlxYYA9HItg49EA/jld9r0fUjjyKGzvm1nbUmqm8yzhBn/EtPGcAiOUfT9PNlLY8BS29S/Vip+YdX6OTtJ7ZCsW5znwn6mxVnxKCtVVKikuaVCcJY/nKX9g1YXXCBu+lVNkPNRU2V9tUpw6FybM0dXKOyA+VXwTFnBP5uSwaSal4pKyYt7XcWT8zZcB/Qg0ItwoiZX8n5S0xs9SRHNrjKVd9SUFD3sLfK2E5EqBeHB72TeWtU3TxtekSXJe+jhHn0sxKjoZr/tWVQTkx7wky4JT+TM3OrvLsPHINo+wZtf97qqdO/klnulik/GQVGFILsekyBLBwz5DpMoF0rozGnAbJ+i5eArQla5IGeA2dydkhThDQaRcVfx16Va8mt0YqdQHp9tUoPSRzpOUdm9dedeko7k1YpfBMX2+O/eNToDSfV+5AS48iTPJU0oKh2B3qpn4cWV2XdckHJwerfhkzpFNRUoyyP1u5XHhmJKgJ1ZS+ArxiGSFIRJzC7tnVBm618ZrtcWfplhENSIlcSd8fPxqoniAc7SK2GP4lpex9NVUZWfrSYqzU7a6EkZjkULJ5g/4R2Nxu4lo2kvtw40XnzCI3V96XKL12E/NSGFfFN8byT5wYpV2H5fZ4+QLbekDPLHumjNfg2CXNqs6fjiSaLvrm7a3YVCAeRvbJ4P6WBmTuSeBlNldhqt7pqufRTV76nT6GoBX82CFTy4ptk6S4tCfcl9V8KK/qZjHhRsm8Ui81OXCmYqSjBZbFhG0z123SGqB5edmqRRm41XOewt4Q+kE2CWlxpec6jTQOyfTol8BjTa9nGsXGqB7SlBL83xjbFRNkkePpDyyYNi57OcS3Wgdn4S/ciEal3WZwm3z6CRlMdq9dUnZWbwoSTXl734IS0Mlk7LA/6MtfZ+/SeJq7S7Jok+LlKQs1Kro8lhoxQ2eVK6J6D53ZAkGFd+2Dn7e1Jyh5yXx/HyCRCKHJODD7os+ARVw3TMZQG8qqCsBQ0kJZFSIlEBGNZBUxObPzwY1VR2EDzQHBAQEBAQEBAQEBAQEBAQEqJzxP1Mf9SheTn1EAAAAAElFTkSuQmCC"
                />
            </Defs>
        </Svg>
    )
}

export default SvgEye
