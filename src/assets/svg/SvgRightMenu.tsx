import * as React from "react"
import Svg, { G, Rect, Path, Defs } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function SvgRightMenu(props) {
    return (
        <Svg
            width={97}
            height={100}
            viewBox="0 0 97 100"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <G filter="url(#filter0_d_5_19)">
                <Rect x={48} y={10} width={40} height={40} rx={20} fill="#fff" />
            </G>
            <Path
                d="M73.333 26.4c0-2.982-2.388-5.4-5.333-5.4-2.945 0-5.333 2.418-5.333 5.4 0 6.3-2.667 8.1-2.667 8.1h16s-2.667-1.8-2.667-8.1M69.538 38.1a1.774 1.774 0 01-1.538.897c-.634 0-1.22-.342-1.538-.897"
                stroke="#404246"
                strokeWidth={1.8}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Defs></Defs>
        </Svg>
    )
}

export default SvgRightMenu
