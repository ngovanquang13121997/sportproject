import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgBackWhite(props) {
    return (
        <Svg
            width={23}
            height={22}
            viewBox="0 0 23 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M22 11H1M11.5 21L1 11 11.5 1"
                stroke="#fff"
                strokeWidth={1.6}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    )
}

export default SvgBackWhite
