import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgHomeActive(props) {
    return (
        <Svg
            width={17}
            height={17}
            viewBox="0 0 17 17"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                clipRule="evenodd"
                d="M1.435 6.071l7-4.9 7 4.9v7.7c0 .774-.697 1.4-1.556 1.4H2.99c-.859 0-1.555-.626-1.555-1.4v-7.7z"
                stroke="#034685"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M6.101 15.171v-7h4.667v7"
                stroke="#034685"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    )
}

export default SvgHomeActive
