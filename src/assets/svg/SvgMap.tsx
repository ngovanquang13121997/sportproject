import * as React from "react"
import Svg, { Path, Defs, Pattern, Use, Image } from "react-native-svg"

// @ts-ignore
function SvgMap(props) {
    return (
        <Svg
            width={71}
            height={62}
            viewBox="0 0 71 62"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            {...props}
        >
            <Path fill="url(#pattern0)" d="M0 0H71V62H0z" />
            <Defs>
                <Pattern
                    id="pattern0"
                    patternContentUnits="objectBoundingBox"
                    width={1}
                    height={1}
                >
                    <Use
                        xlinkHref="#image0_1453_272"
                        transform="matrix(.00873 0 0 .01 .063 0)"
                    />
                </Pattern>
                <Image
                    id="image0_1453_272"
                    width={100}
                    height={100}
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAIP0lEQVR4nO2dWYwURRiACwUVUPFgFS88CR7IpQhqNDEmGo8YD4JGjQEFJEJEeYCgeCJe8VYUTbwSjQf4gJoY44s+qPHiCrp4EAILKq4gh1luPlPZf+PwT83s9Ex3V/fUfE+b3Z2qf+rv6qr6rzImgwC9gL7AaUA/oDewt2+5gkAGfSLwNrAQ2ERpfgfmArcDJ/qWvW6QJ34G8BPVsxv4CDjZ9/fJLUBP4FFgM/HRBlzn+7vlDuAcYHmFT/4aYAnwJfAN8AuwocxndgIzgaFAF9/fNfPYJxjYWuYJnwdMAAYD+5Vppz8wDlhURjktsibtk+63zAnAVcCuEgM3GTigija7ALfIzCjFr8CgZL5VTgEGygzQr6Rn7XoSQ/szKY9dqy6N59vkHHtuAL5TA7QDuDHGProBw+R195ZD+ZZ/gQEmdIBJjsEZm3CfhwFzHP3+DPQwgc+OFWpQ5qXY/wSHUqabUAGuVoNhXyVHpSzDbCXD2mB3XmIGKeQNDzIcKutHIVeY0AD2AlrVQJznSZbnlRzPmNAATleDYJ/Sbp5kuVjJstiEBjBSDcJnHmU5UB0edwW3jgB3KYXM9izPCiVPXxMSjve21+0mxYfTESYkgFfTPAx2BvCpkuciExLAu2oAbvYsz2dKngtNSADvqAEY41meL5Q8F5iQcNiSJnqWZ6GS50wTEsDjagAe8CzPOiXPkSYkgKlqAOZ4lKWHksWeQ7qawA+Gn3iU5Qwly3ITGsAQNQgrPcoyRsnyoQkNMVdYN20Hu6vxm8cB8IJSyMMmRIDf1ECc60mOZiVHmP51x1lkqgcZjlIyWCPjQSZEgClqMOZ7kGG0kuEHEyqOwWhNO6KQYq9laj79TGH9DRIEpxno2Wu50zrPTGgAVzoG4v5yIaIJytGqZHnWhIbdWqpBeNmjLDcoWb4yoeEIv7kjQ4fUpSY0gEfUIDzqUZZLlCxfm9AARqlB+NSjLHcrWV40oQEcqwbB5oU0eZLleyXLTSZEHCaL14E+acRn0Z470tsR32sj748xIQKMx80CDw6yDt42oWLPHDZKkGLWpdD3645+bap1fxMywPGOIDXLcQn3u8gRynpZkn3mBqnIoE3xiaUwA/ur0FHri2lkT6lBsvmEqfjYgUtVX0uS6iu3AJerQdoQR8KnC0mvLuRJ5z+GjCzwrUkHzwGHA9tUP2fH3U9dADymBspWdNg35j6earyuou24tidldAROcFSKmBBX+3UJ8JwjEfTsmILhvnFUcQgrMafKJEwd1mlnzfga2jwGWE0x18QrfZ1iczMc9UmerqG9wQ5lhGsiqQabCZugQlYHXbGhGuxinqBCFtYQFDFC8iM/sJ5FYL28UrfLz0vlnDMdGG4/Y+oB4M4EFbIg4uePFg+nK0qmM1okfiDVChVpBNHFqZDvI2wwXnQcJKthq8QRHGLqJH+kFoUMiqoQ4FqH5SAObJujTN4AHlRfZFYNbfVTbTWX+d+uMitKsUFqbo2VGlxNUo+rm/w8TP5mIyI3lmnnhVwlBTl2WdNqaKuPaqulxP91l5KyLpol9LV7hH67S+7JshJtfhilPa9IIeRCbosxZW2zjpCUmeFSRpvs+Kp+mqXtKSUq2c3P/EyxphLHwfDyGttsVe2NU39/yTFYq+PMxpXXmS1tq3neZBU7AMBfDptTTU8R7RWyC63IvdQCrlmQRCaubVPa1ow0GSz3Z6f1FoewV8fQ/gFyJrDu2vPV1rbVMTMSS4uWxd8WfdaV7A5Oqs+oirge+BE3T8fY11DgFfU7vaNqS6NogPXhO9aU55Lut7NAg1sdT8oeyog7gYeC9uQEvtVX0LcjhHVL6gULpJLc7E726BvirNsbIeC7Oc0dj1w6sCau81bUbafdw39FeWwFhdfSsPvQbijUtqnRSffrkMO+JQpZmZhBUhxD1k/+TyeKsJbSN4FTEhHEgVht9axM/ZAmD6u+nGZY3J30E/OC9o9rVsl29IhYBagAMY8X8pbJTu2waXE13Ed2Lds7eS19Ymvj+rwvinZ/RiYq2jmCzufG0ehNnSzU1k/+BHCSyQC0O5KSe01Ek8U6suKJpLT2feD9MopYFNUolwYUB1L09iiLvRygkNZa1godJN3BYrmkJZNXC1HsdPIWDmSDAZUsW6tppL8c93FYUidn/T5Bsq2QbdVMMbtf1nxrcwdNDiDbr6x1UXPzdElVpLJPptaJiIv6WcYTjjNR5Yu6ra/rUMbcrK4VOd32zotyqlzreE3lZmaUORh6i2QE3lOyTKr0g/bev0KsV+9Uk0Mo3vtv9GQ66em40bSya2Idnq49fAt5ArdxMfXq2o48+VWVfvBEhwmkb51VJFrmwfxub7YuZEa1pVQ/NzmH9nqL2kE1JcX+71F9b644stERlXGvqc8yUW1p2LXEUddWtbvacb9GHvlDZ/aWCHJYk0KQg42kKaQlUpBDmSCEvPFQBaWikA1MU0LK+EH1tTvyBTLAn9QHOwrDgMpUt0YCMAbE/JrSM8PyUTWNueKl8sp6PdAS7mljbTVtEh3Ss8bd1IwSoaQdO1avNwxlEtqDo22srYs1EpBQcSqcWDYmOLa2DaVUiswUfYNcIZvEBz5eTvtNUoN4H7HaDhfFvec4gReuGR/LzGjMlAj3m7j8PbXS0rGAi6G2oZRKsdtQKWSgD4/VsFkiMffY2jaUUn3E+qwSTjkqDHUqeQJvKKU2g6TN8ZgmPqAl4nncJqFPf8vvbFr0pIqttg2lZBOxF+5ybAAaBXJ80VBKBmkoJYM0lJJByuy+hviWLVgoXujv8y1T8PC/UhrKMBnBFtAp/MV/1UH7nJpxdKkAAAAASUVORK5CYII="
                />
            </Defs>
        </Svg>
    )
}

export default SvgMap
