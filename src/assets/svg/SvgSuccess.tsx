import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"

function SvgSuccess(props) {
    return (
        <Svg
            width={134}
            height={134}
            viewBox="0 0 134 134"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Circle cx={67} cy={67} r={67} fill="#fff" />
            <Path
                d="M31 75.154l10.02-11.487 17.176 18.666L95.412 45 104 55.051 58.196 101 31 75.154z"
                fill="#034685"
            />
        </Svg>
    )
}

export default SvgSuccess
