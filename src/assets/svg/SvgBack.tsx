import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgBack(props) {
    return (
        <Svg
            width={20}
            height={20}
            viewBox="0 0 20 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M19 10H1M10 19l-9-9 9-9"
                stroke={props.color || "#303535"}
                strokeWidth={1.6}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    )
}

export default SvgBack
