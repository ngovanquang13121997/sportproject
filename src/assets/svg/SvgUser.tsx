import * as React from "react"
import Svg, { Path, Ellipse } from "react-native-svg"

function SvgUser(props) {
    return (
        <Svg
            width={17}
            height={17}
            viewBox="0 0 17 17"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M15.652 15.171v-1.555c0-1.718-1.567-3.111-3.5-3.111h-7c-1.933 0-3.5 1.393-3.5 3.11v1.556"
                stroke="#AFB2B5"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Ellipse
                cx={8.65218}
                cy={4.2825}
                rx={3.5}
                ry={3.11111}
                stroke="#AFB2B5"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    )
}

export default SvgUser
