import * as React from "react"
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg"

function SvgMessage(props) {
    return (
        <Svg
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <G clipPath="url(#clip0_36_49)">
                <Path
                    d="M11.963.413C5.438.413.375 5.213.375 11.663c0 3.412 1.387 6.337 3.638 8.325.15.15.262.412.3.637l.037 2.063c.038.637.675 1.125 1.313.825L7.986 22.5c.15-.037.413-.112.638-.037 1.05.262 2.175.487 3.375.487 6.563-.037 11.625-4.837 11.625-11.25 0-6.412-5.1-11.287-11.662-11.287zm6.975 8.662l-3.413 5.363c-.525.825-1.688 1.125-2.475.487l-2.7-2.062a.812.812 0 00-.825 0L5.85 15.6c-.487.375-1.125-.225-.787-.75l3.412-5.362C9 8.663 10.163 8.363 10.95 9l2.7 2.063c.262.15.563.15.825 0l3.638-2.813c.562-.225 1.125.338.825.825z"
                    fill="#011AFF"
                />
            </G>
            <Defs>
                <ClipPath id="clip0_36_49">
                    <Path fill="#fff" d="M0 0H24V24H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    )
}

export default SvgMessage
