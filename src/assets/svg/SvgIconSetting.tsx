import * as React from "react"
import Svg, { Rect } from "react-native-svg"

function SvgIconSetting(props) {
    return (
        <Svg
            width={7}
            height={9}
            viewBox="0 0 7 9"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Rect
                width={2.09228}
                height={6.62556}
                rx={1.04614}
                transform="scale(-1 1) rotate(-50 1.874 9.255)"
                fill="#8C8D8E"
            />
            <Rect
                width={2.09228}
                height={6.62556}
                rx={1.04614}
                transform="scale(-1 1) rotate(50 -.672 -1.442)"
                fill="#8C8D8E"
            />
        </Svg>
    )
}

export default SvgIconSetting
