import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"

function SvgAvatarDefault(props) {
    return (
        <Svg
            width={53}
            height={53}
            viewBox="0 0 53 53"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Circle cx={26.5} cy={26.5} r={26.5} fill="#C4C4C4" />
            <Path
                d="M26.819 26.819c4.32 0 7.716-3.395 7.716-7.716 0-4.32-3.395-7.716-7.716-7.716s-7.716 3.395-7.716 7.716 3.395 7.716 7.716 7.716zm14.351 13.58c-2.006-7.87-10.03-12.809-17.9-10.802-5.247 1.388-9.414 5.4-10.803 10.802-.154.771.309 1.697 1.235 1.852h25.925c.926 0 1.543-.618 1.543-1.543v-.31z"
                fill="#E2E2E2"
            />
        </Svg>
    )
}

export default SvgAvatarDefault
