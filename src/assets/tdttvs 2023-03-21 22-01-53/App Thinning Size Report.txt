
App Thinning Size Report for All Variants of tdttvs

Variant: AppProject-0BCAB68F-68FE-4A2E-81A8-37BF176B4F13.ipa
Supported variant descriptors: [device: iPhone13,4, os-version: 14.0], [device: iPhone12,5, os-version: 13.0], [device: iPhone10,2, os-version: 13.0], [device: iPhone10,3, os-version: 14.0], [device: iPhone12,5, os-version: 14.0], [device: iPhone10,5, os-version: 13.0], [device: iPhone9,2, os-version: 13.0], [device: iPhone11,4, os-version: 14.0], [device: iPhone11,4, os-version: 13.0], [device: iPhone11,2, os-version: 13.0], [device: iPhone13,1, os-version: 14.0], [device: iPhone12,3, os-version: 14.0], [device: iPhone9,4, os-version: 14.0], [device: iPhone10,6, os-version: 13.0], [device: iPhone12,3, os-version: 13.0], [device: iPhone11,6, os-version: 13.0], [device: iPhone13,2, os-version: 14.0], [device: iPhone10,2, os-version: 14.0], [device: iPhone10,3, os-version: 13.0], [device: iPhone11,2, os-version: 14.0], [device: iPhone10,6, os-version: 14.0], [device: iPhone13,3, os-version: 14.0], [device: iPhone9,2, os-version: 14.0], [device: iPhone11,6, os-version: 14.0], [device: iPhone10,5, os-version: 14.0], and [device: iPhone9,4, os-version: 13.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-19217B4E-EA0D-41E4-BFF4-A52A8FE5FC09.ipa
Supported variant descriptors: [device: iPad6,7, os-version: 13.0], [device: iPad6,7, os-version: 14.0], [device: iPad7,6, os-version: 14.0], [device: iPad5,1, os-version: 14.0], [device: iPad6,8, os-version: 13.0], [device: iPad6,11, os-version: 13.0], [device: iPad7,5, os-version: 14.0], [device: iPad11,6, os-version: 14.0], [device: iPad7,11, os-version: 13.0], [device: iPad6,8, os-version: 14.0], [device: iPad6,11, os-version: 14.0], [device: iPad5,4, os-version: 13.0], [device: iPad5,4, os-version: 14.0], [device: iPad5,3, os-version: 13.0], [device: iPad5,3, os-version: 14.0], [device: iPad5,1, os-version: 13.0], [device: iPad7,5, os-version: 13.0], [device: iPad7,11, os-version: 14.0], [device: iPad7,6, os-version: 13.0], [device: iPad5,2, os-version: 13.0], [device: iPad6,12, os-version: 14.0], [device: iPad11,7, os-version: 14.0], [device: iPad5,2, os-version: 14.0], [device: iPad7,12, os-version: 14.0], [device: iPad7,12, os-version: 13.0], and [device: iPad6,12, os-version: 13.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-26BFD48E-7BE9-4A59-9DA8-DD0A54DFD8A3.ipa
Supported variant descriptors: [device: iPhone8,2, os-version: 14.0] and [device: iPhone8,2, os-version: 13.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-3679FD9D-21D9-4F31-8234-640E4C671587.ipa
Supported variant descriptors: [device: iPhone9,1, os-version: 12.2], [device: iPhone10,1, os-version: 12.2], [device: iPhone9,3, os-version: 12.2], and [device: iPhone10,4, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-3A6CDF52-72C3-494F-9A7B-62F1AB2C9C74.ipa
Supported variant descriptors: [device: iPhone11,8, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-4B228ACB-B0E1-4538-B6D3-D5CFB4AF45ED.ipa
Supported variant descriptors: [device: iPhone8,2, os-version: 12.2] and [device: iPhone7,1, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-5CB5FB38-5E4B-42FF-84C7-A572C3450FBB.ipa
Supported variant descriptors: [device: iPhone10,4, os-version: 15.0], [device: iPhone14,6, os-version: 15.0], [device: iPhone10,1, os-version: 15.0], [device: iPhone12,8, os-version: 16.0], [device: iPhone9,3, os-version: 15.0], [device: iPhone10,1, os-version: 16.0], [device: iPhone10,4, os-version: 16.0], [device: iPhone14,6, os-version: 16.0], [device: iPhone9,1, os-version: 15.0], and [device: iPhone12,8, os-version: 15.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-5DDC2063-B52A-490D-8A30-3743EC41A849.ipa
Supported variant descriptors: [device: iPad13,2, os-version: 15.0], [device: iPad8,9, os-version: 15.0], [device: iPad7,3, os-version: 16.0], [device: iPad7,1, os-version: 16.0], [device: iPad7,2, os-version: 16.0], [device: iPad13,11, os-version: 15.0], [device: iPad8,6, os-version: 16.0], [device: iPad14,6-A, os-version: 16.0], [device: iPad8,4, os-version: 15.0], [device: iPad6,3, os-version: 16.0], [device: iPad11,1, os-version: 16.0], [device: iPad6,3, os-version: 15.0], [device: iPad8,8, os-version: 15.0], [device: iPad14,1, os-version: 16.0], [device: iPad14,3-A, os-version: 16.0], [device: iPad14,5-B, os-version: 16.0], [device: iPad8,3, os-version: 15.0], [device: iPad8,12, os-version: 16.0], [device: iPad8,9, os-version: 16.0], [device: iPad11,2, os-version: 15.0], [device: iPad14,2, os-version: 16.0], [device: iPad8,10, os-version: 16.0], [device: iPad8,3, os-version: 16.0], [device: iPad13,1, os-version: 16.0], [device: iPad8,6, os-version: 15.0], [device: iPad13,8, os-version: 16.0], [device: iPad8,1, os-version: 15.0], [device: iPad13,2, os-version: 16.0], [device: iPad13,1, os-version: 15.0], [device: iPad8,11, os-version: 15.0], [device: iPad8,12, os-version: 15.0], [device: iPad13,4, os-version: 16.0], [device: iPad14,1, os-version: 15.0], [device: iPad11,3, os-version: 15.0], [device: MacFamily20,1, os-version: 15.0], [device: iPad11,4, os-version: 15.0], [device: iPad13,17, os-version: 15.0], [device: iPad13,16, os-version: 16.0], [device: iPad14,6-B, os-version: 16.0], [device: iPad13,16, os-version: 15.0], [device: iPad13,6, os-version: 16.0], [device: iPad7,4, os-version: 16.0], [device: iPad14,2, os-version: 15.0], [device: iPad13,4, os-version: 15.0], [device: iPad8,7, os-version: 16.0], [device: iPad14,4-B, os-version: 16.0], [device: iPad13,17, os-version: 16.0], [device: iPad11,4, os-version: 16.0], [device: iPad7,1, os-version: 15.0], [device: iPad14,3-B, os-version: 16.0], [device: MacFamily20,1, os-version: 16.0], [device: iPad8,8, os-version: 16.0], [device: iPad7,2, os-version: 15.0], [device: iPad13,9, os-version: 15.0], [device: iPad8,2, os-version: 15.0], [device: iPad13,11, os-version: 16.0], [device: iPad8,1, os-version: 16.0], [device: iPad8,2, os-version: 16.0], [device: iPad8,11, os-version: 16.0], [device: iPad14,5-A, os-version: 16.0], [device: iPad11,2, os-version: 16.0], [device: iPad13,8, os-version: 15.0], [device: iPad7,4, os-version: 15.0], [device: iPad13,10, os-version: 16.0], [device: iPad11,1, os-version: 15.0], [device: iPad8,5, os-version: 15.0], [device: iPad13,10, os-version: 15.0], [device: iPad11,3, os-version: 16.0], [device: iPad13,6, os-version: 15.0], [device: iPad14,4-A, os-version: 16.0], [device: iPad8,10, os-version: 15.0], [device: iPad13,5, os-version: 16.0], [device: iPad7,3, os-version: 15.0], [device: iPad6,4, os-version: 16.0], [device: iPad13,5, os-version: 15.0], [device: iPad13,7, os-version: 16.0], [device: iPad8,5, os-version: 16.0], [device: iPad6,4, os-version: 15.0], [device: iPad13,7, os-version: 15.0], [device: iPad13,9, os-version: 16.0], [device: iPad8,4, os-version: 16.0], and [device: iPad8,7, os-version: 15.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-77206161-D16B-481E-9E86-CCF9285CDF12.ipa
Supported variant descriptors: [device: iPhone12,1, os-version: 15.0], [device: iPhone11,8, os-version: 16.0], [device: iPhone11,8, os-version: 15.0], and [device: iPhone12,1, os-version: 16.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-A51D7306-5DA1-4036-88AE-446B0409C893.ipa
Supported variant descriptors: [device: iPhone10,2, os-version: 12.2], [device: iPhone11,2, os-version: 12.2], [device: iPhone11,4, os-version: 12.2], [device: iPhone9,2, os-version: 12.2], [device: iPhone9,4, os-version: 12.2], [device: iPhone10,5, os-version: 12.2], [device: iPhone10,3, os-version: 12.2], [device: iPhone11,6, os-version: 12.2], and [device: iPhone10,6, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-A6394F51-AD4B-499C-A17D-CC4D0063DBAA.ipa
Supported variant descriptors: [device: iPad13,4, os-version: 14.0], [device: iPad8,10, os-version: 13.0], [device: iPad8,9, os-version: 14.0], [device: iPad7,4, os-version: 14.0], [device: iPad7,1, os-version: 14.0], [device: iPad8,3, os-version: 14.0], [device: iPad11,4, os-version: 13.0], [device: iPad13,5, os-version: 14.0], [device: iPad8,6, os-version: 14.0], [device: iPad13,11, os-version: 14.0], [device: iPad7,2, os-version: 14.0], [device: iPad13,10, os-version: 14.0], [device: iPad8,12, os-version: 13.0], [device: iPad7,1, os-version: 13.0], [device: iPad7,4, os-version: 13.0], [device: iPad8,4, os-version: 13.0], [device: iPad8,11, os-version: 14.0], [device: iPad8,5, os-version: 13.0], [device: iPad8,7, os-version: 13.0], [device: iPad8,3, os-version: 13.0], [device: iPad6,4, os-version: 13.0], [device: iPad8,11, os-version: 13.0], [device: MacFamily20,1, os-version: 14.0], [device: iPad7,3, os-version: 14.0], [device: iPad8,7, os-version: 14.0], [device: iPad8,10, os-version: 14.0], [device: iPad6,3, os-version: 13.0], [device: iPad11,3, os-version: 14.0], [device: iPad8,2, os-version: 13.0], [device: iPad11,4, os-version: 14.0], [device: iPad8,5, os-version: 14.0], [device: iPad13,8, os-version: 14.0], [device: iPad8,2, os-version: 14.0], [device: iPad13,6, os-version: 14.0], [device: iPad6,3, os-version: 14.0], [device: iPad8,6, os-version: 13.0], [device: iPad11,2, os-version: 14.0], [device: iPad8,1, os-version: 14.0], [device: iPad8,12, os-version: 14.0], [device: iPad13,1, os-version: 14.0], [device: iPad8,1, os-version: 13.0], [device: iPad11,3, os-version: 13.0], [device: iPad13,9, os-version: 14.0], [device: iPad7,3, os-version: 13.0], [device: iPad8,9, os-version: 13.0], [device: iPad11,1, os-version: 13.0], [device: iPad7,2, os-version: 13.0], [device: iPad13,2, os-version: 14.0], [device: iPad11,1, os-version: 14.0], [device: iPad8,4, os-version: 14.0], [device: iPad11,2, os-version: 13.0], [device: iPad13,7, os-version: 14.0], [device: iPad8,8, os-version: 13.0], [device: iPad6,4, os-version: 14.0], and [device: iPad8,8, os-version: 14.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-AF94336D-8227-4A12-B606-069B292C119B.ipa
Supported variant descriptors: [device: iPhone8,2, os-version: 15.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-B8AB7392-309F-4CA1-AE05-CAFE481DB592.ipa
Supported variant descriptors: [device: iPhone13,3, os-version: 15.0], [device: iPhone11,6, os-version: 16.0], [device: iPhone10,2, os-version: 15.0], [device: iPhone11,2, os-version: 15.0], [device: iPhone14,3, os-version: 15.0], [device: iPhone13,2, os-version: 16.0], [device: iPhone13,4, os-version: 16.0], [device: iPhone14,5, os-version: 15.0], [device: iPhone13,1, os-version: 15.0], [device: iPhone11,4, os-version: 16.0], [device: iPhone12,3, os-version: 15.0], [device: iPhone10,3, os-version: 15.0], [device: iPhone11,4, os-version: 15.0], [device: iPhone11,2, os-version: 16.0], [device: iPhone14,2, os-version: 15.0], [device: iPhone13,2, os-version: 15.0], [device: iPhone15,2, os-version: 16.0], [device: iPhone10,6, os-version: 16.0], [device: iPhone14,3, os-version: 16.0], [device: iPhone12,5, os-version: 15.0], [device: iPhone14,8, os-version: 16.0], [device: iPhone10,5, os-version: 15.0], [device: iPhone13,3, os-version: 16.0], [device: iPhone10,3, os-version: 16.0], [device: iPhone14,4, os-version: 16.0], [device: iPhone15,3, os-version: 16.0], [device: iPhone13,4, os-version: 15.0], [device: iPhone9,2, os-version: 15.0], [device: iPhone11,6, os-version: 15.0], [device: iPhone9,4, os-version: 15.0], [device: iPhone12,5, os-version: 16.0], [device: iPhone10,5, os-version: 16.0], [device: iPhone12,3, os-version: 16.0], [device: iPhone13,1, os-version: 16.0], [device: iPhone10,2, os-version: 16.0], [device: iPhone14,7, os-version: 16.0], [device: iPhone14,5, os-version: 16.0], [device: iPhone14,2, os-version: 16.0], [device: iPhone14,4, os-version: 15.0], and [device: iPhone10,6, os-version: 15.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-C272787C-DCC8-489B-8964-F2AD74708914.ipa
Supported variant descriptors: [device: iPod9,1, os-version: 14.0], [device: iPhone8,4, os-version: 13.0], [device: iPhone8,1, os-version: 13.0], [device: iPod9,1, os-version: 13.0], [device: iPhone8,4, os-version: 14.0], and [device: iPhone8,1, os-version: 14.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-CC238782-8053-4BA3-B1B3-D9B013E5B5EE.ipa
Supported variant descriptors: [device: iPhone7,2, os-version: 12.2], [device: iPod9,1, os-version: 12.2], [device: iPhone6,1, os-version: 12.2], [device: iPhone8,4, os-version: 12.2], [device: iPhone8,1, os-version: 12.2], [device: iPod7,1, os-version: 12.2], and [device: iPhone6,2, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-D5D6AC44-6DD0-42A4-9D36-1598176C9E13.ipa
Supported variant descriptors: [device: iPhone9,1, os-version: 13.0], [device: iPhone10,1, os-version: 14.0], [device: iPhone10,4, os-version: 13.0], [device: iPhone10,1, os-version: 13.0], [device: iPhone12,8, os-version: 13.0], [device: iPhone9,1, os-version: 14.0], [device: iPhone9,3, os-version: 13.0], [device: iPhone9,3, os-version: 14.0], [device: iPhone12,8, os-version: 14.0], and [device: iPhone10,4, os-version: 14.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-DA0F5704-8792-4DE8-BA8F-50AB82562848.ipa
Supported variant descriptors: [device: iPhone11,8, os-version: 13.0], [device: iPhone12,1, os-version: 14.0], [device: iPhone12,1, os-version: 13.0], and [device: iPhone11,8, os-version: 14.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-DDC65E65-DF23-4D7A-B1F6-055991BEA7A0.ipa
Supported variant descriptors: [device: iPod9,1, os-version: 15.0], [device: iPhone8,4, os-version: 15.0], and [device: iPhone8,1, os-version: 15.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-E089A771-B882-4BBB-939E-F66515193E88.ipa
Supported variant descriptors: [device: iPad7,1, os-version: 12.2], [device: iPad8,8, os-version: 12.2], [device: iPad11,1, os-version: 12.2], [device: iPad7,3, os-version: 12.2], [device: iPad7,2, os-version: 12.2], [device: iPad8,5, os-version: 12.2], [device: iPad7,4, os-version: 12.2], [device: iPad8,2, os-version: 12.2], [device: iPad8,4, os-version: 12.2], [device: iPad11,2, os-version: 12.2], [device: iPad11,4, os-version: 12.2], [device: iPad8,6, os-version: 12.2], [device: iPad8,3, os-version: 12.2], [device: iPad6,3, os-version: 12.2], [device: iPad8,7, os-version: 12.2], [device: iPad6,4, os-version: 12.2], [device: iPad8,1, os-version: 12.2], and [device: iPad11,3, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-E157A64F-5803-4C1E-ADDB-9BA093EF4FCE.ipa
Supported variant descriptors: [device: iPad7,6, os-version: 12.2], [device: iPad4,7, os-version: 12.2], [device: iPad5,4, os-version: 12.2], [device: iPad4,9, os-version: 12.2], [device: iPad6,12, os-version: 12.2], [device: iPad5,1, os-version: 12.2], [device: iPad6,11, os-version: 12.2], [device: iPad4,2, os-version: 12.2], [device: iPad6,7, os-version: 12.2], [device: iPad4,3, os-version: 12.2], [device: iPad4,1, os-version: 12.2], [device: iPad4,8, os-version: 12.2], [device: iPad7,5, os-version: 12.2], [device: iPad4,4, os-version: 12.2], [device: iPad4,5, os-version: 12.2], [device: iPad5,2, os-version: 12.2], [device: iPad6,8, os-version: 12.2], [device: iPad4,6, os-version: 12.2], and [device: iPad5,3, os-version: 12.2]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject-E8312107-B6F2-42AC-9CC8-205D5E408481.ipa
Supported variant descriptors: [device: iPad6,11, os-version: 16.0], [device: iPad6,8, os-version: 15.0], [device: iPad6,8, os-version: 16.0], [device: iPad11,7, os-version: 16.0], [device: iPad12,2, os-version: 16.0], [device: iPad7,11, os-version: 16.0], [device: iPad5,4, os-version: 15.0], [device: iPad13,18, os-version: 16.0], [device: iPad11,6, os-version: 15.0], [device: iPad12,1, os-version: 16.0], [device: iPad6,12, os-version: 16.0], [device: iPad11,7, os-version: 15.0], [device: iPad6,11, os-version: 15.0], [device: iPad6,12, os-version: 15.0], [device: iPad7,5, os-version: 16.0], [device: iPad7,11, os-version: 15.0], [device: iPad6,7, os-version: 15.0], [device: iPad5,1, os-version: 15.0], [device: iPad13,19, os-version: 16.0], [device: iPad5,2, os-version: 15.0], [device: iPad6,7, os-version: 16.0], [device: iPad7,12, os-version: 15.0], [device: iPad12,1, os-version: 15.0], [device: iPad11,6, os-version: 16.0], [device: iPad12,2, os-version: 15.0], [device: iPad5,3, os-version: 15.0], [device: iPad7,6, os-version: 15.0], [device: iPad7,6, os-version: 16.0], [device: iPad7,12, os-version: 16.0], and [device: iPad7,5, os-version: 15.0]
App + On Demand Resources size: 6,5 MB compressed, 13,5 MB uncompressed
App size: 6,5 MB compressed, 13,5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: AppProject.ipa
Supported variant descriptors: Universal
App + On Demand Resources size: 6,6 MB compressed, 13,7 MB uncompressed
App size: 6,6 MB compressed, 13,7 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed
