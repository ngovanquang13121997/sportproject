import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS, ROUTES } from '../constants';
import FollowScreen from '../screens/Follow';
import FollowDetail from '../screens/Follow/FollowDetail';
import CustomHeaderLeft from '../views/components/CustomHeader';

const Stack = createNativeStackNavigator();

const TabFollow = () => {
    const optionHeader: any = (title: string) => {
        return {
            title,
            headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
            headerStyle: {
                backgroundColor: '#034685',
            },
            headerTitleAlign: 'center',
            headerTitleStyle: styles.titleHeader
        }
    }

    return (
        <Stack.Navigator
            initialRouteName={ROUTES.Follow}
            screenOptions={{
                headerLeft: () => <CustomHeaderLeft />,
            }}
        >
            <Stack.Screen name={ROUTES.Follow} component={FollowScreen}
                options={optionHeader("ĐANG THEO DÕI")}
            />

            <Stack.Screen name={ROUTES.FollowDetail} component={FollowDetail}
                options={{
                    headerTitle: "",
                    headerShown: true,
                    headerBackTitleVisible: false,
                    headerTransparent: true
                }}
            />
        </Stack.Navigator>
    )
}

export default TabFollow

const styles = StyleSheet.create({
    titleHeader: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: 'center',
        textAlignVertical: "center",
        textTransform: "uppercase"
    },
})