import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS, ROUTES } from '../constants';
import MedalScreen from '../screens/ Medal/ MedalScreen';
import FederationDetailScreen from '../screens/Federations/FederationDetailScreen';
import FederationScreen from '../screens/Federations/FederationScreen';
import FlightScreen from '../screens/Flight';
import HomeScreen from '../screens/Home/HomeScreen';
import HotelScreen from '../screens/Hotel';
import DetailScreen from '../screens/News/DetailScreen';
import NewScreen from '../screens/News/NewScreen';
import ScheduleSeaGameScreen from '../screens/ScheduleSeaGame/ScheduleSeaGameScreen';
import ScheduleScreen from '../screens/Schedules/ScheduleScreen';
import SchuduleDetailScreen from '../screens/Schedules/SchuduleDetailScreen';
import SportMapDetailScreen from '../screens/SportMap/SportMapDetailScreen';
import SportMapScreen from '../screens/SportMap/SportMapScreen';
import SportDetailScreen from '../screens/Sports/SportDetailScreen';
import SportScreen from '../screens/Sports/SportScreen';
import CustomHeaderLeft from '../views/components/CustomHeader';
import DiscoverScreen from '../screens/DiscoverScreen';

const Stack = createNativeStackNavigator();

const TabHome = () => {
    const optionHeader: any = (title: string) => {
        return {
            title,
            headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
            headerStyle: {
                backgroundColor: '#034685',
            },
            headerTitleAlign: 'center',
            headerTitleStyle: styles.titleHeader
        }
    }

    return (
        <Stack.Navigator
            initialRouteName={ROUTES.HOME}
            screenOptions={{
                headerLeft: () => <CustomHeaderLeft />,
            }}
        >
            {/* TRANG CHỦ */}
            <Stack.Screen name={ROUTES.HOME} component={HomeScreen}
                options={{
                    headerShown: false
                }}
            />

            {/* MÔN THỂ THAO MAP */}
            <Stack.Screen name={ROUTES.SPORTS_MAP} component={SportMapScreen}
                options={optionHeader("MÔN THỂ THAO")}
            />
            <Stack.Screen name={ROUTES.SPORTS_MAP_DETAIL} component={SportMapDetailScreen}
                options={{
                    headerTitle: "",
                    headerShown: true,
                    headerBackTitleVisible: false,
                    headerTransparent: true
                }}
            />

            {/* TIN TỨC */}
            <Stack.Screen name={ROUTES.NEWS} component={NewScreen}
                options={optionHeader("TIN TỨC")}
            />
            <Stack.Screen name={ROUTES.NEWS_DETAIL} component={DetailScreen}
                options={{
                    headerTitle: "",
                    headerShown: true,
                    headerBackTitleVisible: false,
                    headerTransparent: true
                }}
            />

            {/* LỊCH THI ĐẤU */}
            <Stack.Screen name={ROUTES.SCHEDULES} component={ScheduleScreen}
                options={optionHeader("LỊCH THI ĐẤU")}
            />
            <Stack.Screen name={ROUTES.SCHEDULES_DETAIL} component={SchuduleDetailScreen}
                options={{
                    headerTitle: "",
                    headerShown: true,
                    headerBackTitleVisible: false,
                    headerTransparent: true
                }}
            />

            {/* MÔN THỂ THAO */}
            <Stack.Screen name={ROUTES.SPORTS} component={SportScreen}
                options={optionHeader("MÔN THỂ THAO")}
            />
            <Stack.Screen name={ROUTES.SPORTS_DETAIL} component={SportDetailScreen}
                options={{
                    headerTitle: "",
                    headerShown: true,
                    headerBackTitleVisible: false,
                    headerTransparent: true
                }}
            />

            {/* LIÊN ĐOÀN,HIỆP HÔI */}
            <Stack.Screen name={ROUTES.FEDERATIONS} component={FederationScreen}
                options={optionHeader("LIÊN ĐOÀN, HIỆP HỘI")}
            />
            <Stack.Screen name={ROUTES.FEDERATIONS_DETAIL} component={FederationDetailScreen}
                options={{
                    headerTitle: "",
                    headerShown: true,
                    headerBackTitleVisible: false,
                    headerTransparent: true
                }}
            />

            <Stack.Screen name={ROUTES.HOTEL} component={HotelScreen}
                options={optionHeader("ĐẶT PHÒNG KHÁCH SẠN")}
            />

            <Stack.Screen name={ROUTES.FLIGHT} component={FlightScreen}
                options={optionHeader("ĐẶT VÉ MÁY BAY")}
            />

            {/* HUY CHƯƠNG */}
            <Stack.Screen name={ROUTES.MEDAL_SCREEN} component={MedalScreen}
                options={optionHeader("HUY CHƯƠNG")}
            />

            {/* LỊCH THI ĐẤU */}
            <Stack.Screen name={ROUTES.SCHEDULE_SEA_GAME_SCREEN} component={ScheduleSeaGameScreen}
                options={optionHeader("LỊCH THI ĐẤU")}
            />
            {/* END TRANG CHỦ */}
        </Stack.Navigator>
    )
}

export default TabHome

const styles = StyleSheet.create({
    titleHeader: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: 'center',
        textAlignVertical: "center",
        textTransform: "uppercase"
    },
})