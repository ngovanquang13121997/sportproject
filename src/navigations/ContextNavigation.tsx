import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useCallback, useContext, useEffect, useRef, useState } from 'react';
// @ts-ignore

function useOnEventCallback<T extends (...args: any[]) => any>(callback: T, options?: { event?: string }): T {
    const ref = useRef<T>((() => { }) as T);

    if (typeof callback === 'function') {
        ref.current = callback;
    }

    return useCallback<T>(
        ((...args) => {
            return ref.current(...args);
        }) as T,
        [],
    );
}


const NavigationDataContext = createContext({})

interface ContextValues {
    isLogin?: boolean,
    setIsLogin?: any,
    updateContextData?: any,
}

export const NavigationDataProvider = ({ children }: { children: any }) => {
    const [isLogin, setIsLogin] = useState<boolean>(false)

    useEffect(() => {
        onGetAsyncStorageData()
    }, [])

    const onGetAsyncStorageData = async () => {
        const TokenCode: any = await AsyncStorage.getItem('@userInfoData');
        setIsLogin(Boolean(TokenCode))
    }

    const updateContextData = useOnEventCallback((changedData: any) => {
        setIsLogin(changedData)
    })

    return (
        <NavigationDataContext.Provider value={{ isLogin, updateContextData }}>
            {children}
        </NavigationDataContext.Provider>
    )
}

export const useDataContextNavigation = () => {
    const values: ContextValues = useContext(NavigationDataContext)
    return values
}