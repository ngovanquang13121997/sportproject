import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Button, Modal, NativeBaseProvider } from 'native-base';
import React, { useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { useQuery } from 'react-query';
import SvgFavourite from '../assets/svg/SvgFavourite';
import SvgFavouriteActive from '../assets/svg/SvgFavouriteActive';
import SvgHome from '../assets/svg/SvgHome';
import SvgHomeActive from '../assets/svg/SvgHomeActive';
import SvgLoading from '../assets/svg/SvgLoading';
import SvgSetting from '../assets/svg/SvgSetting';
import SvgSettingActive from '../assets/svg/SvgSettingActive';
import SvgUser from '../assets/svg/SvgUser';
import SvgUserActive from '../assets/svg/SvgUserActive';
import { COLORS, IMGS, ROUTES } from '../constants';
import { newsApi } from '../network/services';
import ForgotPassword from '../screens/Auth/ ForgotPassword';
import FormForgotPassword from '../screens/Auth/ ForgotPassword/FormForgotPassword';
import LoginScreen from '../screens/Auth/Login';
import RegisterScreen from '../screens/Auth/Register';
import RegisterSuccess from '../screens/Auth/Register/RegisterSuccess';
import ChangePassword from '../screens/Settings/ChangePassword';
import ChangePasswordSuccess from '../screens/Settings/ChangePasswordSuccess';
import ComingSoon from '../views/components/ComingSoon';
import CustomHeaderLeft from '../views/components/CustomHeader';
import { NavigationDataProvider, useDataContextNavigation } from './ContextNavigation';
import TabFollow from './TabFollow';
import TabHome from './TabHome';
import TabInfo from './TabInfo';
import TabSetting from './TabSetting';
import DiscoverScreen from '../screens/DiscoverScreen';

const Tab = createBottomTabNavigator()
const Stack = createNativeStackNavigator()

const NavigationLayout = () => {
    const { isLogin } = useDataContextNavigation()
    const [showModal, setShowModal] = useState<boolean>(false)
    const [navData, setNavData] = useState<any>()

    const { isLoading, isLoadingError } = useQuery('slider', newsApi.getDataListAllGames)

    const ViewLoadingScreen = () => {
        return (
            <View style={styles.splashLayout}>
                <View style={styles.logoScreen}>
                    <SvgLoading />
                </View>
                <View style={styles.loadingScreen}>
                    <View style={{ alignSelf: "center" }}>
                        <Image source={IMGS.IMG_LOADING} />
                    </View>
                    {isLoadingError
                        ? <Text style={styles.textLoadingError}>Có lỗi xảy ra. Vui lòng thử lại!</Text>
                        : ""}
                </View>
            </View>
        )
    }

    switch (isLoading) {
        case true:
            return ViewLoadingScreen()
        case false:
            if (isLoadingError) {
                return ViewLoadingScreen()
            } else {
                return (
                    <>
                        <NavigationContainer independent={true}>
                            <Stack.Navigator screenOptions={{ headerShown: false }}>
                                <Stack.Screen name="Home">
                                    {() => <Tab.Navigator
                                        backBehavior={'history'}
                                        initialRouteName={ROUTES.HOME}
                                        screenOptions={({ route }) => ({
                                            tabBarShowLabel: false,
                                            tabBarIcon: ({ focused, color, size }) => {
                                                let iconName;

                                                if (route.name === 'HomeComponent') {
                                                    iconName = focused ? <SvgHomeActive /> : <SvgHome />;
                                                } else if (route.name === 'Favourite') {
                                                    iconName = focused ? <SvgFavouriteActive /> : <SvgFavourite />
                                                } else if (route.name === 'Settings') {
                                                    iconName = focused ? <SvgSettingActive /> : <SvgSetting />
                                                } else if (route.name === 'User') {
                                                    iconName = focused ? <SvgUserActive /> : <SvgUser />
                                                }

                                                return <View style={styles.textIcon}><Text>{iconName}</Text></View>
                                            },

                                            tabBarStyle: {
                                                borderTopLeftRadius: 10,
                                                borderTopRightRadius: 10,
                                                "shadowOffset": {
                                                    "width": 0,
                                                    "height": 2
                                                },
                                                "shadowRadius": 48,
                                                "shadowColor": "rgba(0, 0, 0, 0.08)",
                                                "shadowOpacity": 1,
                                            }
                                        })}
                                    >
                                        <Tab.Screen name="HomeComponent" component={TabHome}
                                            options={{
                                                headerShown: false,
                                            }}
                                            listeners={({ navigation }) => ({
                                                tabPress: (e) => {
                                                    e.preventDefault();
                                                    navigation.navigate("HomeComponent", {
                                                        screen: ROUTES.HOME
                                                    })
                                                }
                                            })}
                                        />
                                        <Tab.Screen name="Favourite" component={TabFollow}
                                            options={{
                                                headerShown: false,
                                            }}
                                            listeners={({ navigation }) => ({
                                                tabPress: (e) => {
                                                    e.preventDefault();
                                                    navigation.navigate("Favourite", {
                                                        screen: ROUTES.Follow
                                                    })
                                                }
                                            })}
                                        />

                                        <Tab.Screen name="Settings" component={TabSetting}
                                            options={{
                                                headerShown: false,
                                            }}
                                            listeners={({ navigation }) => ({
                                                tabPress: (e) => {
                                                    e.preventDefault();
                                                    navigation.navigate("Settings", {
                                                        screen: ROUTES.SettingScreen
                                                    })
                                                }
                                            })}
                                        />

                                        <Tab.Screen name="User"
                                            options={{
                                                headerShown: false,
                                            }}
                                            listeners={({ navigation }) => ({
                                                tabPress: (e) => {
                                                    setNavData(navigation)
                                                    e.preventDefault();
                                                    if (isLogin) {
                                                        navigation.navigate('User', {
                                                            screen: ROUTES.InfoUserScreen
                                                        })
                                                    } else {
                                                        setShowModal(true)
                                                    }

                                                },
                                            })}
                                            component={isLogin ? TabInfo : LoginScreen}
                                        />
                                    </Tab.Navigator>}
                                </Stack.Screen>

                                {/* AUTH  */}
                                <Stack.Screen name={ROUTES.LOGIN} component={LoginScreen}
                                    options={{
                                        headerShown: false,
                                    }}
                                />

                                <Stack.Screen name={ROUTES.REGISTER} component={RegisterScreen}
                                    options={{
                                        headerShown: false,
                                    }}
                                />

                                <Stack.Screen name={ROUTES.REGISTER_SUCCESS} component={RegisterSuccess}
                                    options={{
                                        headerShown: false,
                                    }}
                                />

                                <Stack.Screen name={ROUTES.CHANGE_PASSWORD} component={ChangePassword}
                                    options={{
                                        headerShown: true,
                                        title: "ĐỔI MẬT KHẨU",
                                        headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
                                        headerStyle: {
                                            backgroundColor: '#034685',
                                        },
                                        headerTitleAlign: 'center',
                                        headerTitleStyle: styles.titleHeader

                                    }}
                                />

                                <Stack.Screen name={ROUTES.CHANGE_PASSWORD_SUCCESS} component={ChangePasswordSuccess}
                                    options={{
                                        headerShown: false,
                                    }}
                                />

                                {/* Quên mật khẩu */}
                                <Stack.Screen name={ROUTES.ForgotPassword} component={ForgotPassword}
                                    options={{
                                        headerShown: false,
                                    }}
                                />
                                <Stack.Screen name={ROUTES.FormForgotPassword} component={FormForgotPassword}
                                    options={{
                                        headerShown: true,
                                        title: "Quên mật khẩu",
                                        headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
                                        headerStyle: {
                                            backgroundColor: '#034685',
                                        },
                                        headerTitleAlign: 'center',
                                        headerTitleStyle: styles.titleHeader

                                    }}
                                />
                                {/* END AUTH */}

                                {/* COMING_SOON */}
                                <Stack.Screen name={ROUTES.COMING_SOON} component={ComingSoon}
                                    options={{
                                        headerShown: false,
                                    }}
                                />

                                <Stack.Screen name={ROUTES.DISCOVER} component={DiscoverScreen}
                                    options={{
                                        headerShown: true,
                                        title: "KHÁM PHÁ",
                                        headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
                                        headerStyle: {
                                            backgroundColor: '#034685',
                                        },
                                        headerTitleAlign: 'center',
                                        headerTitleStyle: styles.titleHeader

                                    }}
                                />
                            </Stack.Navigator>
                        </NavigationContainer>
                        <Modal
                            isOpen={showModal}
                            onClose={() => {
                                setShowModal(false)
                            }}
                        >
                            <Modal.Content>
                                <Modal.Body>
                                    <View>
                                        <Text style={styles.titleModal}>Thông báo</Text>
                                        <Text style={styles.contentModal}>
                                            Bạn cần phải đăng nhập ứng dụng để sử dụng chức năng này
                                        </Text>
                                    </View>
                                    <View style={styles.buttonModalNotification}>
                                        <Button onPress={() => {
                                            setShowModal(false)
                                        }} style={[styles.buttonModal, { backgroundColor: "#ED1C24", flex: 0.5 }]}>
                                            <Text style={styles.textModal}>Bỏ qua</Text>
                                        </Button>

                                        <Button onPress={() => {
                                            setShowModal(false)
                                            navData.navigate(ROUTES.LOGIN)
                                        }} style={[styles.buttonModal, { backgroundColor: "#034685", flex: 0.5 }]}>
                                            <Text style={styles.textModal}>Đăng nhập</Text>
                                        </Button>
                                    </View>
                                </Modal.Body>
                            </Modal.Content>
                        </Modal>
                    </>
                )
            }
    }
}

const NavigationLayoutWithContextProvider = () => {
    return (
        <NativeBaseProvider>
            <NavigationDataProvider>
                <NavigationLayout />
            </NavigationDataProvider>
        </NativeBaseProvider>
    )
}

export default NavigationLayoutWithContextProvider

const styles = StyleSheet.create({
    titleHeader: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: 'center',
        textAlignVertical: "center",
    },
    titleModal: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#2D2F34"
    },
    contentModal: {
        "fontFamily": "Quicksand-Medium",
        "fontStyle": "normal",
        "fontWeight": "500",
        "fontSize": 15,
        "display": "flex",
        "alignItems": "center",
        "textAlign": "center",
        "color": "#000000",
        paddingTop: 14,
    },
    buttonModalNotification: {
        flex: 1,
        flexDirection: "row",
        gap: 10,
        justifyContent: "center",
        marginTop: 24,
    },
    buttonModal: {
        "borderTopLeftRadius": 15,
        "borderTopRightRadius": 15,
        "borderBottomRightRadius": 15,
        "borderBottomLeftRadius": 15,
        width: 136,
    },
    textModal: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "textAlign": "center",
        "color": "#FFFFFF"
    },
    tileFooter: {
        "fontFamily": "Quicksand-SemiBold",
        "fontStyle": "normal",
        "fontWeight": "600",
        "fontSize": 15,
        "color": "#034685"
    },
    textIcon: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    splashLayout: {
        flex: 1,
        position: "relative"
    },
    logoScreen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#034685"
    },
    loadingScreen: {
        position: "absolute",
        alignSelf: 'center',
        bottom: "15%",

    },
    textLoadingError: {
        "fontFamily": "Quicksand-Regular",
        "fontStyle": "normal",
        "fontWeight": "400",
        "fontSize": 15,
        "color": COLORS.white,
        paddingTop: 5
    }
})