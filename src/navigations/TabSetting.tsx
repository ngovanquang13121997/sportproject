import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS, ROUTES } from '../constants';
import IntroduceScreen from '../screens/Settings/ IntroduceScreen';
import SettingScreen from '../screens/Settings/SettingScreen';
import SupportScreen from '../screens/Settings/SupportScreen';
import CustomHeaderLeft from '../views/components/CustomHeader';

const Stack = createNativeStackNavigator();

const TabSetting = () => {
    const optionHeader: any = (title: string) => {
        return {
            title,
            headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
            headerStyle: {
                backgroundColor: '#034685',
            },
            headerTitleAlign: 'center',
            headerTitleStyle: styles.titleHeader
        }
    }

    return (
        <Stack.Navigator
            initialRouteName={ROUTES.SettingScreen}
            screenOptions={{
                headerLeft: () => <CustomHeaderLeft />,
            }}
        >
            <Stack.Screen name={ROUTES.SettingScreen} component={SettingScreen}
                options={optionHeader("CÀI ĐẶT")}
            />

            <Stack.Screen name={ROUTES.INTRODUCE} component={IntroduceScreen}
                options={optionHeader("GIỚI THIỆU")}
            />

            <Stack.Screen name={ROUTES.SUPPORT} component={SupportScreen}
                options={optionHeader("HỖ TRỢ")}
            />
        </Stack.Navigator>
    )
}

export default TabSetting

const styles = StyleSheet.create({
    titleHeader: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: 'center',
        textAlignVertical: "center",
        textTransform: 'uppercase',
    },
})