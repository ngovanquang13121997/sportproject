import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS, ROUTES } from '../constants';
import InfoUserScreen from '../screens/InfoUser/InfoUserScreen';
import CustomHeaderLeft from '../views/components/CustomHeader';

const Stack = createNativeStackNavigator();

const TabInfo = () => {
    const optionHeader: any = (title: string) => {
        return {
            title,
            headerLeft: () => <CustomHeaderLeft color='#ffffff' />,
            headerStyle: {
                backgroundColor: '#034685',
            },
            headerTitleAlign: 'center',
            headerTitleStyle: styles.titleHeader
        }
    }

    return (
        <Stack.Navigator
            initialRouteName={ROUTES.InfoUserScreen}
            screenOptions={{
                headerLeft: () => <CustomHeaderLeft />,
            }}
        >
            <Stack.Screen name={ROUTES.SettingScreen} component={InfoUserScreen}
                options={optionHeader("THÔNG TIN CÁ NHÂN")}
            />
        </Stack.Navigator>
    )
}

export default TabInfo

const styles = StyleSheet.create({
    titleHeader: {
        "fontFamily": "Quicksand-Bold",
        "fontStyle": "normal",
        "fontWeight": "700",
        "fontSize": 15,
        "color": COLORS.white,
        textAlign: 'center',
        textAlignVertical: "center",
        textTransform: 'uppercase',
    },
})