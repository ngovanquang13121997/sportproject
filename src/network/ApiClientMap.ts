import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

const REQ_TIMEOUT = 1800 * 1000;

export interface BaseResponse {
    [anyProps: string]: any
}

const instance = axios.create({
    baseURL: "http://14.232.245.27:8888/api/v1",
    timeout: REQ_TIMEOUT,
});

instance.interceptors.request.use((_config) => requestHandler(_config));

const requestHandler = (request: AxiosRequestConfig | any) => {
    return request;
};

instance.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
);

const errorHandler = (error: any) => {
    return Promise.reject({ ...error });
};

const successHandler = (response: AxiosResponse) => {
    return response.data;
};

async function fetch<ReqType, ResType extends BaseResponse>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
}

async function post<ReqType, ResType extends BaseResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, { ...data }, { headers });
}

async function put<ReqType, ResType extends BaseResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.put(url, { ...data }, { headers });
}

async function remove<ReqType, ResType extends BaseResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { data, headers });
}

function getHeader(customHeaders: any = {}): any {
    const sessionStr: string | null = null;
    if (sessionStr) {
        const session = JSON.parse(sessionStr);
        const token = session.token;
        if (token) {
            return {
                ...customHeaders,
                Authorization: `Bearer ${token}`,
            };
        }
    }
    let header = {
        accept: "application/json",
    };

    return { ...customHeaders, ...header };
}
const ApiClientMap = {
    fetch,
    post,
    put,
    delete: remove,
};

export default ApiClientMap;
