import AsyncStorage from "@react-native-async-storage/async-storage"
import ApiHelper from "./ApiClient"
// @ts-ignore
import CryptoJS from 'react-native-crypto-js'
import axios from "axios"
import { isEmpty } from "lodash"
import { URL_AUTH_ASC } from "./UrlNetWork"
import ApiClientMap from "./ApiClientMap"

const LIMIT = 10

export const newsApi = {
    getDataListNews: async (pageParam: number) => {
        const res = await ApiHelper.fetch('/article', {
            start: pageParam,
            limit: LIMIT
        })
        return res?.data
    },

    getDataListGames: async (pageParam: number, limit?: number) => {
        const res = await ApiHelper.fetch('/games', {
            start: pageParam,
            limit: limit || LIMIT
        })
        if (limit && limit > 5) {
            const arrGameIds: string | null = await AsyncStorage.getItem('@arrGameIds');
            if (arrGameIds) {
                const myArray: string[] = JSON.parse(arrGameIds);
                const matchedObjects = res?.data.filter((obj: any) => myArray.includes(String(obj.gameId)));
                return matchedObjects
            }
            return []
        }
        return res?.data
    },

    getDataListAllGames: async () => {
        const res = await ApiHelper.fetch('/games', {
            start: 0,
            limit: 1000
        })

        const data = res?.data

        if (data) {
            const currentMonth = new Date().getMonth() + 1;
            const filteredGames = data.filter((game: any) => {
                const gameMonth = new Date(game.startDate).getMonth() + 1;
                return gameMonth === currentMonth;
            });
            if (!isEmpty(filteredGames)) {
                return filteredGames.slice(0, 5)
            } else {
                return res?.data.slice(0, 5)
            }
        }

        return res?.data
    },

    getDataListSports: async () => {
        const res = await ApiHelper.fetch('/sports', {
            start: 0,
            limit: 100
        })
        return res?.data
    },

    getDataListSportMap: async () => {
        const res = await ApiClientMap.fetch('/sportall')
        return res?.data
    },

    getDataListFederations: async (pageParam: number) => {
        const res = await ApiHelper.fetch('/federations', {
            start: pageParam,
            limit: LIMIT
        })
        return res?.data
    }
}

export const getTokenUser = async () => {
    const plainText = "TTV:tichhop:tichhop"
    const ciphertext = await CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(plainText));

    if (ciphertext) {
        const res = await axios.get(`${URL_AUTH_ASC}/api/Token/Login`, {
            headers: {
                'content-type': 'application/json',
                'Authorization': `Basic ${ciphertext}`
            }
        })

        await AsyncStorage.setItem('@TokenCode', JSON.stringify(res?.data?.Data?.TokenCode))
        return res?.data?.Data?.TokenCode
    }
}

export const onGetMedal = async () => {
    const res = await ApiHelper.fetch('/medal')
    return res?.data
}

export const onGetSchedules = async () => {
    const res = await ApiHelper.fetch('/schedules')
    return res?.data
}