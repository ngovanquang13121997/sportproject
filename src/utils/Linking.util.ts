import { Linking, Platform } from "react-native";

export type AppstoreLinkingOption = {
    appName: string,
    appStoreId: string,
    appStoreLocale: string,
    playStoreId: string,
}
export const openUrl = async (
    url: string,
    options: AppstoreLinkingOption
) => {
    try {
        await Linking.openURL(url)
    } catch (error: any) {
        if (error.code === 'EUNSPECIFIED') {
            await openInStore(options)
        }
    }
};

export const openInStore = async (options: AppstoreLinkingOption) => {
    const { appName, appStoreId, appStoreLocale = 'vn', playStoreId } = options
    if (Platform.OS === 'ios') {
        await Linking.openURL(`https://itunes.apple.com/${appStoreLocale}/app/${appName}/id${appStoreId}`);
    } else {
        await Linking.openURL(
            `https://play.google.com/store/apps/details?id=${playStoreId}`
        );
    }
};

export const DeepLinkingUtils = {
    openUrl,
    openInStore
}