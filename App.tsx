import { NavigationContainer } from '@react-navigation/native';
import * as React from 'react';
import Toast, { BaseToast, ErrorToast } from 'react-native-toast-message';
import { QueryClient, QueryClientProvider } from 'react-query';
import NavigationLayout from './src/navigations/NavigationLayout';
import RNBootSplash from "react-native-bootsplash";
const queryClient = new QueryClient();

function App() {
  const toastConfig = {
    success: (props: any) => (
      <BaseToast
        {...props}
        text1NumberOfLines={0}
        style={{
          paddingTop: 5,
          paddingBottom: 5,
          borderLeftColor: '#00FF00'
        }}
        text1Style={{
          "fontFamily": "Quicksand-Bold",
          "fontStyle": "normal",
          "fontWeight": "700",
          "fontSize": 14,
          "color": "#034685",
        }}
      />
    ),

    error: (props: any) => (
      <ErrorToast
        {...props}
        text1NumberOfLines={0}
        style={{
          paddingTop: 5,
          paddingBottom: 5,
          borderLeftColor: 'red'
        }}
        text1Style={{
          "fontFamily": "Quicksand-Bold",
          "fontStyle": "normal",
          "fontWeight": "700",
          fontSize: 15,
          "color": "#034685",
        }}
      />
    ),
  };

  return (
    <NavigationContainer onReady={() => RNBootSplash.hide({ fade: true, duration: 100 })}>
      <QueryClientProvider client={queryClient}>
        <NavigationLayout />
        <Toast config={toastConfig} visibilityTime={2000} />
      </QueryClientProvider>
    </NavigationContainer>
  );
}

export default App;